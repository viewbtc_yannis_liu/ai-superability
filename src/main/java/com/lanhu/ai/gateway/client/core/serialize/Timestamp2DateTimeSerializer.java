package com.lanhu.ai.gateway.client.core.serialize;

import cn.hutool.core.date.DateUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.lanhu.ai.gateway.client.core.ProjectConstant;

import java.io.IOException;

/********************************
 * @title Timestamp2DateTimeSerializer
 * @package com.lanhu.gateway.client.jackson.serializer
 * @description description
 *
 * @author Mr.ｓｕ＇ｑｉａｎｇ
 * @date 2023/6/3 21:56
 * @version 0.0.1
 *********************************/
public class Timestamp2DateTimeSerializer extends JsonSerializer<Long> {
    @Override
    public void serialize(Long timestamp, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (timestamp != null) {
            //如果单位是秒
            if (String.valueOf(timestamp).length() == ProjectConstant.TEN) {
                timestamp = timestamp * 1000L;
            }
            gen.writeString(DateUtil.formatDateTime(DateUtil.date(timestamp)));
        } else {
            gen.writeString("");
        }
    }
}
