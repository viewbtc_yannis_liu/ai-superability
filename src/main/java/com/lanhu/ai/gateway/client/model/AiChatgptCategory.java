package com.lanhu.ai.gateway.client.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(value = "ai_chatgpt_category")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ai_chatgpt_category")
public class AiChatgptCategory {
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "")
    private Integer id;

    /**
     * 分类名称
     */
    @TableField(value = "category_name")
    @ApiModelProperty(value = "分类名称")
    private String categoryName;

    /**
     * chatgpt上下文
     */
    @TableField(value = "chatgpt_context")
    @ApiModelProperty(value = "chatgpt上下文")
    private String chatgptContext;

    /**
     * chatgpt温度
     */
    @TableField(value = "chatgpt_temperature")
    @ApiModelProperty(value = "chatgpt温度")
    private Double chatgptTemperature;

    /**
     * chatgpt模型
     */
    @TableField(value = "chatgpt_model")
    @ApiModelProperty(value = "chatgpt模型")
    private String chatgptModel;

    /**
     * chatgpt tokens
     */
    @TableField(value = "chatgpt_max_tokens")
    @ApiModelProperty(value = "chatgpt tokens")
    private Integer chatgptMaxTokens;

    /**
     * chatgpt top p
     */
    @TableField(value = "chatgpt_top_p")
    @ApiModelProperty(value = "chatgpt top p")
    private Double chatgptTopP;

    @TableField(value = "chatgpt_frequency_penalty")
    @ApiModelProperty(value = "")
    private Double chatgptFrequencyPenalty;

    @TableField(value = "chatgpt_presence_penalty")
    @ApiModelProperty(value = "")
    private Double chatgptPresencePenalty;

    @TableField(value = "chatgpt_stop")
    @ApiModelProperty(value = "")
    private String chatgptStop;

    /**
     * 返回数据数量
     */
    @TableField(value = "chatgpt_n")
    @ApiModelProperty(value = "返回数据数量")
    private Integer chatgptN;

    /**
     * 图片尺寸
     */
    @TableField(value = "chatgpt_image_size")
    @ApiModelProperty(value = "图片尺寸")
    private String chatgptImageSize;

    /**
     * 图片返回格式
     */
    @TableField(value = "chatgpt_image_response_format")
    @ApiModelProperty(value = "图片返回格式")
    private String chatgptImageResponseFormat;

    /**
     * 上传图片大小限制
     */
    @TableField(value = "upload_image_max")
    @ApiModelProperty(value = "上传图片大小限制")
    private Integer uploadImageMax;

    /**
     * 上传图片格式限制
     */
    @TableField(value = "upload_image_format")
    @ApiModelProperty(value = "上传图片格式限制")
    private String uploadImageFormat;

    /**
     * 是否有效(0:有效 1删除)
     */
    @TableField(value = "is_effect")
    @ApiModelProperty(value = "是否有效(0:有效 1删除)")
    private Integer isEffect;

    /**
     * 费用
     */
    @TableField(value = "cost")
    @ApiModelProperty(value = "费用")
    private BigDecimal cost;

    /**
     * 开头
     */
    @TableField(value = "`start`")
    @ApiModelProperty(value = "开头")
    private String start;
}