package com.lanhu.ai.gateway.client.enums;

/**
 * 是否有效
 *
 * @author liuyi
 * @version 1.0
 * @date 2021/11/29 10:48 下午
 */

public enum AiActionRequestStatusEnum {
    /**
     * 是
     */
    WAIT_HANDLER(0, "等待处理"),

    /**
     * 否
     */
    HANDLERING(1, "处理中"),

    SUCCESS(2, "成功"),

    FAIL(3, "失败"),
    ;

    private final int code;

    private final String msg;

    AiActionRequestStatusEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static AiActionRequestStatusEnum convert(int code) {
        for (AiActionRequestStatusEnum whetherEnum : AiActionRequestStatusEnum.values()) {
            if (whetherEnum.getCode() == code) {
                return whetherEnum;
            }
        }
        return AiActionRequestStatusEnum.HANDLERING;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
