package com.lanhu.ai.gateway.client.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lanhu.ai.gateway.client.core.BaseController;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.model.AiActionRequest;
import com.lanhu.ai.gateway.client.model.AiImageTemplate;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.service.AiActionRequestService;
import com.lanhu.ai.gateway.client.service.AiImageTemplateService;
import com.lanhu.ai.gateway.client.utils.ServletUtils;
import com.lanhu.ai.gateway.client.vo.req.AiActionRequestDetailForm;
import com.lanhu.ai.gateway.client.vo.req.AiActionRequestListPageForm;
import com.lanhu.ai.gateway.client.vo.req.AiImageTemplateListPageForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/1/11 5:21 下午
 */

@Slf4j
@Api(value = "ai操作请求接口", tags = "ai操作请求接口")
@RestController
public class AiActionRequestController extends BaseController {

    @Autowired
    private AiActionRequestService aiActionRequestService;




    @ApiOperation(value = "AI操作请求结果分页查询", notes = "AI操作请求结果分页查询")
    @PostMapping("/ai/action/v1/listPage")
    public PcsResult<IPage<AiActionRequest>> listPage(@Validated @RequestBody AiActionRequestListPageForm request) {

        ImuMemberUser imuMemberUser = this.currentUser(ServletUtils.getRequest());


        return aiActionRequestService.listPage(imuMemberUser,request);
    }



    @ApiOperation(value = "AI操作请求结果详情", notes = "AI操作请求结果详情")
    @PostMapping("/ai/action/v1/detail")
    public PcsResult<AiActionRequest> detail(@Validated @RequestBody AiActionRequestDetailForm request) {

        ImuMemberUser imuMemberUser = this.currentUser(ServletUtils.getRequest());


        return aiActionRequestService.detail(imuMemberUser,request);
    }



}
