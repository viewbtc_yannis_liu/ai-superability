package com.lanhu.ai.gateway.client.utils;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.http.HttpUtil;
import com.lanhu.ai.gateway.client.config.FileUploadProperties;
import com.lanhu.ai.gateway.client.core.ProjectConstant;
import com.lanhu.ai.gateway.client.enums.UploadTypeEnum;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/6/27 4:43 下午
 */


public class FileUtil {



    private static  final Logger logger = LoggerFactory.getLogger(FileUtil.class);






    /**
    * @description: 通过web流创建本地文件
    * @param: [url]
    * @return: java.io.File
    * @author: liuyi
    * @date: 4:44 下午 2023/6/27
    */
    public static File createFileByWeb(String url,String suffix,String dir,boolean isRGBATransformer){


        try {



            String newFileName = String.format("%s.%s", IdUtil.simpleUUID(), suffix);

            // 日期转换
            String dateStr = DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN);

            // 文件路径
            String parent = dir + File.separator + dateStr;

            File root = new File(parent);

            File file = new File(parent, newFileName);

            // 如果目录不存在,则新建
            if (!root.exists()) {
                root.mkdirs();
            }

            // 创建文件
            file.createNewFile();

            HttpUtil.downloadFile(url,file);


            if(isRGBATransformer){
                file = RGBATransformer(file);
            }


            return file;

        }catch (Exception e){
            logger.error("file create error" ,e);

            return null;
        }


    }

    
    
    /**
    * @description: rgba格式转换
    * @param: [file]
    * @return: java.io.File
    * @author: liuyi
    * @date: 8:54 上午 2023/6/28 
    */
    public static File RGBATransformer(File file){
        try {


            BufferedImage image = ImageIO.read(file);

            ColorSpace colorSpace = image.getColorModel().getColorSpace();
            ColorModel colorModel = image.getColorModel();


            BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);

            Graphics2D g = newImage.createGraphics();
            g.drawImage(image, 0, 0, null);
            g.dispose();

            ImageIO.write(newImage, ProjectConstant.CHATGPT_IMAGE_TYPE, file);





            return file;

        }catch (Exception e){
            logger.error("RGBATransformer error",e);
            return null;
        }
    }




    public static void main(String[] args) {
        String url = "https://oaidalleapiprodscus.blob.core.windows.net/private/org-T0p7ctDSi3SMuwhuyZUtloah/user-hFM6RlluDJoO3IWWmCxmuAgF/img-y5NnKHt5wCxbsftokmUIB1gz.png?st=2023-06-27T07%3A00%3A03Z&se=2023-06-27T09%3A00%3A03Z&sp=r&sv=2021-08-06&sr=b&rscd=inline&rsct=image/png&skoid=6aaadede-4fb3-4698-a8f6-684d7786b067&sktid=a48cca56-e6da-484e-a814-9c849652bcb3&skt=2023-06-27T00%3A07%3A11Z&ske=2023-06-28T00%3A07%3A11Z&sks=b&skv=2021-08-06&sig=1fIarj8v31%2BeeDohbK00b9nZ4hEkgvhBXazGf0SrFL4%3D";
        String suffix = "png";
        String dir = "/Users/liuyi/logs";


        //createFileByWeb(url,suffix,dir);
    }
}
