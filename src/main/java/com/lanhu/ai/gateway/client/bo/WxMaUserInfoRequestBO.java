package com.lanhu.ai.gateway.client.bo;

import lombok.Data;


@Data
public class WxMaUserInfoRequestBO {
    /**
     * 用户openid
     */
    private String openid;

    /**
     * 用户unionid
     */
    private String unionid;

    /**
     * 会话sessionKey
     */
    private String sessionKey;

    /**
     * 使用 sha1( rawData + sessionKey ) 得到字符串，用于校验用户信息，
     */
    private String signature;

    /**
     * 不包括敏感信息的原始数据字符串，用于计算签名
     */
    private String rawData;

    /**
     * 包括敏感数据在内的完整用户信息的加密数据
     */
    private String encryptedData;

    /**
     * 加密算法的初始向量
     */
    private String iv;
}
