package com.lanhu.ai.gateway.client.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


@ApiModel("微信用户登录入参")
@Data
public class MemberUserWxLoginForm {
    /**
     * openid
     */
    @ApiModelProperty(value = "openid", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String openid;
}
