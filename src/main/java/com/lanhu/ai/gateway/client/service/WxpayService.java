package com.lanhu.ai.gateway.client.service;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lanhu.ai.gateway.client.annotation.LhTransaction;
import com.lanhu.ai.gateway.client.configurer.wx.WxMaConfigurer;
import com.lanhu.ai.gateway.client.core.BusinessException;
import com.lanhu.ai.gateway.client.core.PcsResultCode;
import com.lanhu.ai.gateway.client.enums.AccountChangeTypeEnum;
import com.lanhu.ai.gateway.client.enums.AccountStatusEnum;
import com.lanhu.ai.gateway.client.enums.EffectEnum;
import com.lanhu.ai.gateway.client.enums.OrderStatus;
import com.lanhu.ai.gateway.client.mapper.*;
import com.lanhu.ai.gateway.client.model.*;
import com.lanhu.ai.gateway.client.vo.req.WxOrderPayForm;
import com.lanhu.ai.gateway.client.vo.resp.MemberPriceListVo;
import com.lanhu.ai.gateway.client.vo.resp.MemberPriceVo;
import com.lanhu.ai.gateway.client.vo.resp.UnifiedOrderResultVo;
import com.lanhu.ai.gateway.client.wxpaysdk.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;


@Slf4j
@Service
public class WxpayService {

    @Resource
    private ImuMemberUserThirdpartMapper imuMemberUserThirdpartMapper;

    @Resource
    private ImuMemberPriceMapper imuMemberPriceMapper;

    @Resource
    private ImuMemberOrderMapper imuMemberOrderMapper;

    @Resource
    private ImuMemberZhifuMapper imuMemberZhifuMapper;

    @Autowired
    private ImuMemberAccountMapper imuMemberAccountMapper;

    @Autowired
    private ImuMemberAccountChangeRecordMapper imuMemberAccountChangeRecordMapper;


    /**
     * 获取价格列表
     * @param imuMemberUser
     * @return
     * @throws Exception
     */
    public MemberPriceListVo priceList(ImuMemberUser imuMemberUser) throws Exception {
        MemberPriceListVo vo = new MemberPriceListVo();
        List<MemberPriceVo> list = new ArrayList<>();
        List<ImuMemberPrice> prices = imuMemberPriceMapper.selectList();
        if(CollectionUtil.isNotEmpty(prices)){
            prices.forEach(x->{
                MemberPriceVo it = new MemberPriceVo();
                BeanUtil.copyProperties(x,it);
                it.setPrice(x.getPrice().divide(new BigDecimal(100))); //转化为元
                BigDecimal unitPrice = x.getUnitPrice() == null ? new BigDecimal(0) : x.getUnitPrice().divide(new BigDecimal(100)) ;
                it.setUnitPrice(unitPrice);  //转化为元
                it.setPoint(x.getPoint().intValue());
                list.add(it);
            });
            vo.setList(list);
        }
        return vo;
    }


    /**
     * 下单
     * @param imuMemberUser
     * @param form
     * @param ip
     * @return
     * @throws Exception
     */
    @LhTransaction
    public UnifiedOrderResultVo wxpay(ImuMemberUser imuMemberUser , WxOrderPayForm form , String ip) throws Exception {
        LambdaQueryWrapper<ImuMemberUserThirdpart> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(ImuMemberUserThirdpart::getUserId, imuMemberUser.getId());
        ImuMemberUserThirdpart imuMemberUserThirdpart = imuMemberUserThirdpartMapper.selectOne(wrapper);
        String openId = ObjectUtil.isNotNull(imuMemberUserThirdpart) ? imuMemberUserThirdpart.getOpenId() : null ;
        if (StringUtils.isBlank(openId)) {
            throw new BusinessException(PcsResultCode.MEMBER_USER_NOT_EXIST);
        }
        //获取商品信息
        ImuMemberPrice memberPrice = imuMemberPriceMapper.selectById(form.getId());
        if (ObjectUtil.isNull(memberPrice)) {
            throw new BusinessException(PcsResultCode.ORDER_PARAM_ERROR);
        }
        String goodName = memberPrice.getName();
        BigDecimal price = memberPrice.getPrice();
        String outTradeNo = UUID.randomUUID().toString().replaceAll("-","");
        String appId = WxMaConfigurer.getMaConfig().getAppid();
        Long timeStamp = WXPayUtil.getCurrentTimestamp();
        String nonceStr = WXPayUtil.generateNonceStr();

        //1、拼接请求头
        Map<String,String> paraMap = new HashMap<String, String>();
        //商品描述
        paraMap.put("body", goodName);
        paraMap.put("openid",openId);
        //随机字符串
        paraMap.put("out_trade_no", outTradeNo );
        paraMap.put("spbill_create_ip",ip);
        // 1表示1分  100 表示 1元
        paraMap.put("total_fee", String.valueOf(price));
        //支付类型
        paraMap.put("trade_type","JSAPI");

        UnifiedOrderResultVo vo = this.createWxUnifiedOrder(paraMap ,  appId ,  timeStamp , nonceStr );

        //创建订单和支付单
        ImuMemberOrder order = new ImuMemberOrder();
        order.setUserId(imuMemberUser.getId());
        order.setOpenId(openId);
        order.setOutTradeNo(outTradeNo);
        order.setIp(ip);
        order.setAppId(appId);
        order.setTimeStamp(timeStamp);
        order.setNonceStr(nonceStr);
        order.setSignType(WXPayConstants.HMACSHA256);
        order.setPaySign(vo.getPaySign());
        order.setPrepayId(vo.getPrepayID());
        order.setGoodId(form.getId());
        order.setGoodPrice(price);
        order.setGoodPoint(memberPrice.getPoint());
        order.setGoodName(goodName);
        order.setGoodDesc(memberPrice.getDesc());
        order.setStatus(OrderStatus.UNPAY.getCode());
        order.setCreateTime(timeStamp);
        order.setUpdateTime(timeStamp);
        order.setIsDelete(EffectEnum.YES_EFFECT.getCode());
        imuMemberOrderMapper.insert(order);
        log.info("WxpayService wxpay orderId:{}" , order.getId() );

        ImuMemberZhifu zhifu = new ImuMemberZhifu();
        zhifu.setUserId(imuMemberUser.getId());
        zhifu.setOpenId(openId);
        zhifu.setOrderId(order.getId());
        zhifu.setOutTradeNo(outTradeNo);
        zhifu.setPrice(price);
        zhifu.setPoint(memberPrice.getPoint());
        zhifu.setStatus(OrderStatus.UNPAY.getCode());
        zhifu.setCreateTime(timeStamp);
        zhifu.setUpdateTime(timeStamp);
        zhifu.setIsDelete(EffectEnum.YES_EFFECT.getCode());
        imuMemberZhifuMapper.insert(zhifu);

        //6、返回这些参数
        log.info("WxpayService wxpay vo:{}" , vo);
        return vo;
    }


    /**
     * 创建微信预付单
     * @param paraMap
     * @param appId
     * @param timeStamp
     * @param nonceStr
     * @return
     * @throws Exception
     */
    private UnifiedOrderResultVo createWxUnifiedOrder(Map<String,String> paraMap , String appId , Long timeStamp , String nonceStr ) throws Exception {
        // 2、创建wxpay对象，用于支付请求
        //是否使用沙箱环境
        boolean useSandbox = false;
        WXPayConfig wxPayConfig = new MyWxPayConfig();
        WXPay wxPay = new WXPay(wxPayConfig,WxMaConfigurer.getMaConfig().getSUCCESS_NOTIFY(),false,useSandbox);
        // 3、发送请求
        Map<String, String> map = wxPay.unifiedOrder(wxPay.fillRequestData(paraMap), 15000, 15000);
        // 4、统一下单接口，返回预支付id
        String prepayID = map.get("prepay_id");
        log.info("WxpayService wxpay 预支付id:{}" , prepayID);

        // 整合返回信息
        Map<String,String> payMap = new HashMap<String ,String>();
        payMap.put("appId", appId);
        payMap.put("timeStamp", String.valueOf(timeStamp) );
        payMap.put("nonceStr", nonceStr);
        if(useSandbox){
            payMap.put("signType", WXPayConstants.MD5);
        }else{
            payMap.put("signType",WXPayConstants.HMACSHA256);
        }
        String packageStr = "prepay_id="+prepayID ;
        payMap.put("package",packageStr);

        log.info("WxpayService wxpay appId:{} , timeStamp:{} , nonceStr:{} , prepay_id:{}" , appId , timeStamp , nonceStr , prepayID );

        // 5、将上述包装进行key=value形式加密
        String paySign = null;
        if(useSandbox){
            paySign = WXPayUtil.generateSignature(payMap,WxMaConfigurer.getMaConfig().getMchKey(),WXPayConstants.SignType.MD5);
        }else{
            paySign = WXPayUtil.generateSignature(payMap,WxMaConfigurer.getMaConfig().getMchKey(),WXPayConstants.SignType.HMACSHA256);
        }
        payMap.put("paySign",paySign);

        UnifiedOrderResultVo vo = JSONObject.parseObject(JSONObject.toJSONString(payMap), UnifiedOrderResultVo.class);
        vo.setPackageStr(packageStr);
        vo.setPrepayID(prepayID);
        return vo;
    }


    /**
     * 微信支付结果回调通知
     * @param data
     * @param returnCode
     * @throws Exception
     */
    @LhTransaction
    public void payCallBack(String data , String returnCode ) throws Exception {
        //获取微信带来的订单信息
        log.info("WxpayService payCallBack data:{} , returnCode:{}" , data , returnCode );
        Map<String, String> orderMap = WXPayUtil.xmlToMap(data);
        String outTradeNo = orderMap.get("out_trade_no");
        log.info("WxpayService payCallBack outTradeNo:{}", outTradeNo);

        ImuMemberOrder order = imuMemberOrderMapper.selectByOutTradeNo(outTradeNo);
        long time = DateUtil.currentSeconds();
        if (ObjectUtil.isNotNull(order)) {
            order.setStatus(OrderStatus.PAYED.getCode());
            order.setPayTime(time);
            imuMemberOrderMapper.insertOrUpdateSelective(order);
        }

        ImuMemberZhifu zhifu = imuMemberZhifuMapper.selectByOutTradeNo(outTradeNo);
        if (ObjectUtil.isNotNull(zhifu)) {
            zhifu.setStatus(OrderStatus.PAYED.getCode());
            zhifu.setPayTime(time);
            imuMemberZhifuMapper.insertOrUpdateSelective(zhifu);
            BigDecimal point = zhifu.getPoint();
            Long userId = zhifu.getUserId();

            //会员账号充值
            ImuMemberAccount imuMemberAccount = imuMemberAccountMapper.selectByUserId(userId);
            int count = 0;
            if (ObjectUtil.isNotNull(imuMemberAccount)) {
                count = imuMemberAccountMapper.rechargeBalance(userId , point );
            }else{
                imuMemberAccount = ImuMemberAccount.builder()
                .userId(userId)
                .point(point)
                .availablePoint(point)
                .freezePoint(new BigDecimal(0))
                .status(AccountStatusEnum.NORMAL.getCode())
                .createTime(time)
                .updateTime(time)
                .isDelete(EffectEnum.YES_EFFECT.getCode())
                .build();
                count = imuMemberAccountMapper.insert(imuMemberAccount);
            }

            //会员账号变化记录
            if(count >0){
                ImuMemberAccountChangeRecord imuMemberAccountChangeRecord =ImuMemberAccountChangeRecord.builder()
                .id(IdUtil.getSnowflakeNextId())
                .userId(userId)
                .accountId(imuMemberAccount.getUserId())
                .createTime(time)
                .updateTime(time)
                .afterPoint(imuMemberAccount.getAvailablePoint().add(point))
                .beforePoint(imuMemberAccount.getAvailablePoint())
                .desc("充值")
                .changePoint(point)
                .isDelete(EffectEnum.YES_EFFECT.getCode())
                .type(AccountChangeTypeEnum.RECHARGE.getCode())
                .build();
                imuMemberAccountChangeRecordMapper.insert(imuMemberAccountChangeRecord);
            }
        }
    }




}
