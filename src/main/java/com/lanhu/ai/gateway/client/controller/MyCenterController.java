package com.lanhu.ai.gateway.client.controller;
import com.lanhu.ai.gateway.client.annotation.IgnoreSecurity;
import com.lanhu.ai.gateway.client.core.BaseController;
import com.lanhu.ai.gateway.client.core.PageForm;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.core.Result;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.service.MyCenterService;
import com.lanhu.ai.gateway.client.vo.req.CommunityDetailForm;
import com.lanhu.ai.gateway.client.vo.req.CommunityChangeStatusForm;
import com.lanhu.ai.gateway.client.vo.resp.CommunityInfoVo;
import com.lanhu.ai.gateway.client.vo.resp.CommunityListPageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/06/29 22:12
 */

@Slf4j
@Api(tags = "我的相关")
@RestController
public class MyCenterController extends BaseController {

    @Autowired
    private MyCenterService myCenterService;



    @ApiOperation(value = "我的作品", notes = "我的作品")
    @PostMapping("/my/v1/community/listPage")
    public PcsResult<CommunityListPageVo> communityListPage(HttpServletRequest request, @Validated @RequestBody PageForm form) {
        ImuMemberUser imuMemberUser = this.currentUserIgnoreEx(request);
        CommunityListPageVo vo = myCenterService.myCommunityListPage(imuMemberUser , form);
        return Result.ok(vo);
    }


    @ApiOperation(value = "我的作品-详情", notes = "我的作品-详情")
    @PostMapping("/my/v1/community/detail")
    public PcsResult<CommunityInfoVo> communityListPage(HttpServletRequest request, @Validated @RequestBody CommunityDetailForm form) {
        ImuMemberUser imuMemberUser = this.currentUserIgnoreEx(request);
        CommunityInfoVo vo = myCenterService.communityDetail(imuMemberUser , form);
        return Result.ok(vo);
    }



    @ApiOperation(value = "我的作品-删除", notes = "我的作品-删除")
    @PostMapping("/my/v1/community/del")
    public PcsResult communityDel(HttpServletRequest request, @Validated @RequestBody CommunityDetailForm form) {
        ImuMemberUser imuMemberUser = this.currentUserIgnoreEx(request);
        myCenterService.communityDel(imuMemberUser , form);
        return Result.ok();
    }



    @ApiOperation(value = "我的作品-发布或撤销发布", notes = "我的作品-发布或撤销发布")
    @PostMapping("/my/v1/community/changeStatus")
    public PcsResult changeStatus(HttpServletRequest request, @Validated @RequestBody CommunityChangeStatusForm form) {
        ImuMemberUser imuMemberUser = this.currentUserIgnoreEx(request);
        myCenterService.changeStatus(imuMemberUser , form);
        return Result.ok();
    }



    private ImuMemberUser getTestUser(){
        ImuMemberUser imuMemberUser = new ImuMemberUser();
        imuMemberUser.setId(1l);
        return imuMemberUser;
    }
}
