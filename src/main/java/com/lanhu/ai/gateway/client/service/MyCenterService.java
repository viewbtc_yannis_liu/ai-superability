package com.lanhu.ai.gateway.client.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanhu.ai.gateway.client.core.PageForm;
import com.lanhu.ai.gateway.client.enums.CommunityOperateTypeEnum;
import com.lanhu.ai.gateway.client.enums.CommunityStatusEnum;
import com.lanhu.ai.gateway.client.enums.EffectEnum;
import com.lanhu.ai.gateway.client.mapper.ImuMemberCommunityMapper;
import com.lanhu.ai.gateway.client.mapper.ImuMemberCommunityOperateMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberCommunity;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.vo.req.CommunityDetailForm;
import com.lanhu.ai.gateway.client.vo.req.CommunityChangeStatusForm;
import com.lanhu.ai.gateway.client.vo.resp.CommunityInfoVo;
import com.lanhu.ai.gateway.client.vo.resp.CommunityListPageVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/06/29 22:15
 */

@Slf4j
@Service
public class MyCenterService {

    @Resource
    private ImuMemberCommunityMapper imuMemberCommunityMapper;

    @Resource
    private ImuMemberCommunityOperateMapper imuMemberCommunityOperateMapper;


    /**
     * 我的作品
     * @param imuMemberUser
     * @param form
     * @return
     */
    public CommunityListPageVo myCommunityListPage(ImuMemberUser imuMemberUser , PageForm form){
        Long userId = imuMemberUser.getId();
        Page<ImuMemberCommunity> page = new Page<>(form.getPageIndex(), form.getPageSize());
        IPage<ImuMemberCommunity> pageList = imuMemberCommunityMapper.selectMyListPage(page ,userId );
        List<ImuMemberCommunity> records = pageList.getRecords();
        List<CommunityInfoVo> resultList = this.assemblMyCommunityList(imuMemberUser , records);

        CommunityListPageVo listPageVO = new CommunityListPageVo();
        listPageVO.setList(resultList);
        listPageVO.setTotal(pageList.getTotal());
        listPageVO.setTotalPages(pageList.getPages());
        listPageVO.setCurrentPage(pageList.getCurrent());
        return listPageVO;
    }

    /**
     * 我的作品-详情
     * @param imuMemberUser
     * @param form
     * @return
     */
    public CommunityInfoVo communityDetail(ImuMemberUser imuMemberUser , CommunityDetailForm form){
        Long userId = imuMemberUser.getId();
        ImuMemberCommunity community = imuMemberCommunityMapper.selectMyCommunityDetail(userId , form.getId());
        CommunityInfoVo vo = new CommunityInfoVo();
        if (ObjectUtil.isNotNull(community)) {
            BeanUtil.copyProperties(community, vo);
            int count = imuMemberCommunityOperateMapper.selectCountByUserIdAndCommunityId(userId , community.getId() , CommunityOperateTypeEnum.LIKE.getCode() );
            vo.setIsLike(count > 0 ? 1 : 0 );
        }
        //这里不需要预览
        return vo;
    }


    /**
     * 我的作品-删除
     * @param imuMemberUser
     * @param form
     */
    public void communityDel(ImuMemberUser imuMemberUser , CommunityDetailForm form){
        Long userId = imuMemberUser.getId();
        ImuMemberCommunity community = imuMemberCommunityMapper.selectMyCommunityDetail(userId , form.getId());
        if (ObjectUtil.isNotNull(community)) {
            community.setIsDelete(EffectEnum.NO_EFFECT.getCode());
            community.setUpdateTime(DateUtil.currentSeconds());
            imuMemberCommunityMapper.updateById(community);
        }
    }


    /**
     * 我的作品-发布或撤销发布
     * @param imuMemberUser
     * @param form
     */
    public void changeStatus(ImuMemberUser imuMemberUser , CommunityChangeStatusForm form){
        Long userId = imuMemberUser.getId();
        ImuMemberCommunity community = imuMemberCommunityMapper.selectMyCommunityDetail(userId , form.getId());
        if (ObjectUtil.isNotNull(community)) {
            community.setStatus(form.getStatus());
            community.setUpdateTime(DateUtil.currentSeconds());
            imuMemberCommunityMapper.updateById(community);
        }
    }



    /**
     * 组装我的作品
     * @param imuMemberUser
     * @param list
     * @return
     */
    private List<CommunityInfoVo> assemblMyCommunityList(ImuMemberUser imuMemberUser , List<ImuMemberCommunity> list) {
        List<CommunityInfoVo> resultList = CollUtil.newArrayList();
        Long userId = imuMemberUser.getId();
        if (CollUtil.isNotEmpty(list)) {
            for (ImuMemberCommunity community : list) {
                CommunityInfoVo vo = new CommunityInfoVo();
                BeanUtil.copyProperties(community, vo);
                int count = imuMemberCommunityOperateMapper.selectCountByUserIdAndCommunityId(userId , community.getId() , CommunityOperateTypeEnum.LIKE.getCode() );
                vo.setIsLike(count > 0 ? 1 : 0 );
                resultList.add(vo);
            }
        }
        return resultList;
    }


}
