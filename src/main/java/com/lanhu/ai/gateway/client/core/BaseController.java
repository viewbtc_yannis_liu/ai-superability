package com.lanhu.ai.gateway.client.core;

import cn.hutool.core.util.ObjectUtil;
import com.lanhu.ai.gateway.client.core.jwt.Jwt;
import com.lanhu.ai.gateway.client.core.jwt.JwtResult;
import com.lanhu.ai.gateway.client.exception.TokenAuthException;
import com.lanhu.ai.gateway.client.service.MemberUserService;
import com.lanhu.ai.gateway.client.utils.ThreadLocalUtil;
import com.lanhu.ai.gateway.client.enums.TokenStateEnum;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


@Slf4j
@RequestMapping("/miniAiClient")
public class BaseController {
    @Resource
    private MemberUserService memberUserService;

    /**
     * 获取当前用户(忽略异常)
     *
     * @param request request
     * @return 用户
     */
    protected ImuMemberUser currentUserIgnoreEx(HttpServletRequest request) {
        try {
            return this.currentUser(request);
        } catch (Exception e) {
            log.debug("============>>>>>>> 当前用户未登录！");
        }
        return null;
    }

    /**
     * 获取当前用户
     *
     * @param request request
     * @return 用户
     */
    protected ImuMemberUser currentUser(HttpServletRequest request) {
        ImuMemberUser memberUser = (ImuMemberUser) ThreadLocalUtil.get(ProjectConstant.THREAD_LOCAL_MEMBER_FLAG);
        return ObjectUtil.defaultIfNull(memberUser, this.resolveUser(request));
    }

    /**
     * 解析token获取用户信息
     *
     * @param request request
     * @return 用户
     */
    protected ImuMemberUser resolveUser(HttpServletRequest request) {
        JwtResult jwt = checkToken(request);
        ImuMemberUser memberUser = memberUserService.findMemberUserById(jwt.getUserId());
        if (ObjectUtil.isNull(memberUser)) {
            throw new BusinessException(PcsResultCode.MEMBER_USER_NOT_EXIST);
        }
        return memberUser;
    }

    protected void logout(HttpServletRequest request) {
        ThreadLocalUtil.remove(ProjectConstant.THREAD_LOCAL_MEMBER_FLAG);
    }


    /**
     * 检验token
     *
     * @param request request
     * @return JwtResult
     */
    protected JwtResult checkToken(HttpServletRequest request) {
        String token = request.getHeader(ProjectConstant.HEADER_TOKEN);
        // 核对token
        JwtResult jwt = Jwt.checkToken(token);
        // 如果token过期或无效
        if (!TokenStateEnum.VALID.getState().equals(jwt.getState())) {
            throw new TokenAuthException(PcsResultCode.GATEWAY_TOKEN_FAILURE);
        }

        return jwt;
    }
}
