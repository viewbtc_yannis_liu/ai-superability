package com.lanhu.ai.gateway.client.vo.resp;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lanhu.ai.gateway.client.core.serialize.PhoneJsonSerializer;
import com.lanhu.ai.gateway.client.core.serialize.Timestamp2DateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;


@ApiModel("用户信息出参")
@Data
public class MemberUserInfoVo {
    /**
     * id
     */
    @ApiModelProperty(value = "用户ID")
    private Long id;

    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称")
    private String nick;

    /**
     * 头像地址
     */
    @ApiModelProperty(value = "头像地址")
    private String headPortrait;

    /**
     * 手机号前缀
     */
    @ApiModelProperty(value = "手机号前缀")
    private String prefix;

    /**
     * 手机号
     */
    @JsonSerialize(using = PhoneJsonSerializer.class)
    @ApiModelProperty(value = "手机号")
    private String mobile;


    @ApiModelProperty(value = "我的积分")
    private BigDecimal point;


    /**
     * C端token信息
     */
    //@ApiModelProperty(value = "C端token信息")
    //private String cToken;

    /**
     * B端加密签名
     */
    //@ApiModelProperty(value = "B端加密签名")
    //private String bEncryptSign;

    /**
     * 会员卡信息
     */
    //@ApiModelProperty(value = "会员卡信息")
    //private MemberInfoVo memberInfoVo;
}
