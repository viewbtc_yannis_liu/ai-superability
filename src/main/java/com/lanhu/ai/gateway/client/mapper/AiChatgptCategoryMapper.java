package com.lanhu.ai.gateway.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanhu.ai.gateway.client.model.AiChatgptCategory;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AiChatgptCategoryMapper extends BaseMapper<AiChatgptCategory> {
    int updateBatch(List<AiChatgptCategory> list);

    int updateBatchSelective(List<AiChatgptCategory> list);

    int batchInsert(@Param("list") List<AiChatgptCategory> list);

    int insertOrUpdate(AiChatgptCategory record);

    int insertOrUpdateSelective(AiChatgptCategory record);
}