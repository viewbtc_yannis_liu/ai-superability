package com.lanhu.ai.gateway.client.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/6/26 4:24 下午
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleMessage {

    @ApiModelProperty(value="消息拼装，自己提问: user 系统回复: assistant 最新提问: user")
    private String role;

    @ApiModelProperty(value="内容")
    private String content;
}
