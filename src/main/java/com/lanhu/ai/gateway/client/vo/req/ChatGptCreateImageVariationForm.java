package com.lanhu.ai.gateway.client.vo.req;

import com.lanhu.ai.gateway.client.enums.ScaleEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/6/26 8:39 下午
 */
@ApiModel(value = "chatgpt 图片变体 生成入参")
@Data
public class ChatGptCreateImageVariationForm {



    @ApiModelProperty(value = "分类ID", required = true)
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    private Integer categoryId;


    @ApiModelProperty(value = "图片地址", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String imageUrl;


    @ApiModelProperty(value = "生成图片数量")
    private Integer n;

    @ApiModelProperty(value = "生成图片的大小(256x256 , 512x512 ,1024x1024)")
    private String size;


    @ApiModelProperty(value = "缩放比例(1:1,16:9,9:16,3:2,4:3)")
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private  String scale = ScaleEnum.one_one.getCode();

}
