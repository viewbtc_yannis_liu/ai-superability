package com.lanhu.ai.gateway.client.helper;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.auth.DefaultCredentialProvider;
import com.aliyun.oss.model.PutObjectResult;
import com.lanhu.ai.gateway.client.config.AliYunOssProperties;
import com.lanhu.ai.gateway.client.config.FileUploadProperties;
import com.lanhu.ai.gateway.client.core.ProjectConstant;
import com.lanhu.ai.gateway.client.enums.UploadTypeEnum;
import com.lanhu.ai.gateway.client.utils.FileNameGeneratorUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Base64;
import java.util.Date;
import java.util.List;

/********************************
 * @title AliYunOssHelper
 * @package com.lanhu.imenu.gateway.client.helper
 * @description description
 *
 * @date 2022/11/17 2:19 下午
 * @version 0.0.1
 *********************************/
@Slf4j
@Component
@EnableConfigurationProperties(AliYunOssProperties.class)
public class AliYunOssHelper {
    private static final Logger logger = LoggerFactory.getLogger(AliYunOssHelper.class);

    @Resource
    private AliYunOssProperties aliYunOssProperties;

    @Resource
    private FileUploadProperties fileUploadProperties;

    /**
     * @param folderName  上传文件目录
     * @param suffix      上传文件后缀
     * @param inputStream 上传文件流
     * @return 地址
     */
    public String uploadFile2Oss(String folderName, String suffix, InputStream inputStream) {
        OSSClient ossClient = null;
        PutObjectResult ret;

        try {
            String fileName = String.format("%s.%s", FileNameGeneratorUtil.generateFileName(), suffix);
            String dateStr = DateUtil.format(new Date(), DatePattern.NORM_DATE_FORMAT);
            String pathName = folderName + "/" + dateStr + "/" + fileName;
            ossClient = new OSSClient(aliYunOssProperties.getInternalEndPoint(), new DefaultCredentialProvider(aliYunOssProperties.getAccessKeyId(), aliYunOssProperties.getAccessKeySecret()), null);
            ret = ossClient.putObject(aliYunOssProperties.getBucketName(), pathName, inputStream);
            if (ret.getETag() != null) {
                return String.format("%s/%s/%s/%s", aliYunOssProperties.getDomain(), folderName, dateStr, fileName);
            }
        } catch (Exception e) {
            logger.error("====uploadFile2OSS error", e);
        } finally {
            try {
                if (ossClient != null) {
                    ossClient.shutdown();
                }
            } catch (Exception e) {
                logger.error("uploadFile2OSS close inputStream error", e);
            }
        }

        return null;
    }
    
    
    /**
    * @description: 把base64图片上传oss
    * @param: [base64Content]
    * @return: java.lang.String
    * @author: liuyi
    * @date: 2:57 下午 2023/6/27 
    */
    public String uploadBaseImage2Oss(String  base64Content){

        log.info("====uploadBaseImage2Oss start:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT));


        BufferedImage bufferedImage;

        byte[] b = null;

        OSSClient ossClient = null;

        PutObjectResult ret;


        try{

            FileUploadProperties.Config config = null;
            List<FileUploadProperties.Config> configList = fileUploadProperties.getConfigs();
            for (FileUploadProperties.Config tempConfig : configList) {
                if (StringUtils.equals(tempConfig.getFileType(), UploadTypeEnum.chatgpt_image.getCode())) {
                    config = tempConfig;
                    break;
                }
            }


            Base64.Decoder decoder = Base64.getDecoder();
            // 解密
            b = decoder.decode(base64Content);


            String fileName = String.format("%s.%s", FileNameGeneratorUtil.generateFileName(), ProjectConstant.CHATGPT_IMAGE_TYPE);

            String dateStr = DateUtil.format(new Date(), DatePattern.NORM_DATE_FORMAT);

            String pathName = config.getUploadDir() + "/" + dateStr + "/" + fileName;


            ossClient = new OSSClient(aliYunOssProperties.getInternalEndPoint(), new DefaultCredentialProvider(aliYunOssProperties.getAccessKeyId(), aliYunOssProperties.getAccessKeySecret()), null);
            ret = ossClient.putObject(aliYunOssProperties.getBucketName(), pathName, new ByteArrayInputStream(b));


            log.info("====uploadBaseImage2Oss end:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT));



            if (ret.getETag() != null) {
                return String.format("%s/%s/%s/%s", aliYunOssProperties.getDomain(), config.getUploadDir(), dateStr, fileName);
            }



        }catch (Exception e){
            logger.error("====uploadBaseImage2Oss error", e);
            return ProjectConstant.EMPTY;
        }finally {
            b = null;
            bufferedImage = null;
            try {
                if (ossClient != null) {
                    ossClient.shutdown();
                }
            } catch (Exception e) {
                logger.error("uploadBaseImage2Oss close inputStream error", e);
            }
        }



        return ProjectConstant.EMPTY;
    }



    /**
     * @param suffix      上传文件后缀
     * @param inputStream 上传文件流
     * @return 地址
     */
    public String uploadImage2Oss( String suffix, InputStream inputStream) {
        OSSClient ossClient = null;
        PutObjectResult ret;

        try {
            FileUploadProperties.Config config = null;
            List<FileUploadProperties.Config> configList = fileUploadProperties.getConfigs();
            for (FileUploadProperties.Config tempConfig : configList) {
                if (StringUtils.equals(tempConfig.getFileType(), UploadTypeEnum.chatgpt_image.getCode())) {
                    config = tempConfig;
                    break;
                }
            }


            String fileName = String.format("%s.%s", FileNameGeneratorUtil.generateFileName(), suffix);
            String dateStr = DateUtil.format(new Date(), DatePattern.NORM_DATE_FORMAT);
            String pathName = config.getUploadDir() + "/" + dateStr + "/" + fileName;
            ossClient = new OSSClient(aliYunOssProperties.getInternalEndPoint(), new DefaultCredentialProvider(aliYunOssProperties.getAccessKeyId(), aliYunOssProperties.getAccessKeySecret()), null);
            ret = ossClient.putObject(aliYunOssProperties.getBucketName(), pathName, inputStream);
            if (ret.getETag() != null) {
                return String.format("%s/%s/%s/%s", aliYunOssProperties.getDomain(),  config.getUploadDir(), dateStr, fileName);
            }
        } catch (Exception e) {
            logger.error("====uploadImage2Oss error", e);
        } finally {
            try {
                if (ossClient != null) {
                    ossClient.shutdown();
                }
            } catch (Exception e) {
                logger.error("uploadImage2Oss close inputStream error", e);
            }
        }

        return null;
    }
}
