package com.lanhu.ai.gateway.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberZhifu;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ImuMemberZhifuMapper extends BaseMapper<ImuMemberZhifu> {
    int updateBatch(List<ImuMemberZhifu> list);

    int updateBatchSelective(List<ImuMemberZhifu> list);

    int batchInsert(@Param("list") List<ImuMemberZhifu> list);

    int insertOrUpdate(ImuMemberZhifu record);

    int insertOrUpdateSelective(ImuMemberZhifu record);

    /**
     * 根据订单号查询
     * @param outTradeNo
     * @return
     */
    ImuMemberZhifu selectByOutTradeNo(@Param("outTradeNo") String outTradeNo);

}