package com.lanhu.ai.gateway.client.enums;

/**
 * @author liuyi
 * @version 1.0
 * @date 2022/7/20 6:58 下午
 */
public enum UploadTypeEnum {

    /**
     * 其它
     */

    //  /v1/chat/completions
    chatgpt_image("chatgpt_image", "chatgpt_image"),

    ai_image("ai_image", "ai_image"),

    local_store_image("local_store_image", "local_store_image"),



    ;
    private final String code;

    private final String msg;

    UploadTypeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
