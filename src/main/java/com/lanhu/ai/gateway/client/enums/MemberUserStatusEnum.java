package com.lanhu.ai.gateway.client.enums;

import cn.hutool.core.util.ObjectUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum MemberUserStatusEnum {
    /**
     * 正常
     */
    NORMAL(new Byte("0"), "正常"),

    /**
     * 禁用
     */
    DISABLE(new Byte("1"), "禁用"),
    ;

    private final Byte status;

    private final String msg;

    public static MemberUserStatusEnum convert(Byte status) {
        for (MemberUserStatusEnum statusEnum : MemberUserStatusEnum.values()) {
            if (ObjectUtil.equals(statusEnum.getStatus(), status)) {
                return statusEnum;
            }
        }
        return MemberUserStatusEnum.DISABLE;
    }
}
