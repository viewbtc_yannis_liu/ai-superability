package com.lanhu.ai.gateway.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.lanhu"})
public class AiMiniProApplication {

    public static void main(String[] args) {
        SpringApplication.run(AiMiniProApplication.class, args);
    }

}
