package com.lanhu.ai.gateway.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberAccountChangeRecord;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ImuMemberAccountChangeRecordMapper extends BaseMapper<ImuMemberAccountChangeRecord> {
    int updateBatch(List<ImuMemberAccountChangeRecord> list);

    int updateBatchSelective(List<ImuMemberAccountChangeRecord> list);

    int batchInsert(@Param("list") List<ImuMemberAccountChangeRecord> list);

    int insertOrUpdate(ImuMemberAccountChangeRecord record);

    int insertOrUpdateSelective(ImuMemberAccountChangeRecord record);
}