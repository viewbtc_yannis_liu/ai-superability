package com.lanhu.ai.gateway.client.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/06/26 17:50
 */
@ApiModel(value = "微信订单支付入参")
@Data
public class WxOrderPayForm {

    @ApiModelProperty(value = "价格ID (商品ID)", required = true)
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    private Long id;

    /*@ApiModelProperty(value = "价格名称", required = true)
    @NotBlank(message = "NAME_NOT_EMPTY")
    private String name;

    @ApiModelProperty(value = "价格 (单位元)" , required = true)
    @NotNull(message = "PRICE_NOT_EMPTY")
    private Double price ;

    @ApiModelProperty(value = "内容")
    private String content;*/

}
