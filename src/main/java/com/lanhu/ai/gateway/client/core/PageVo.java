package com.lanhu.ai.gateway.client.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "分页出数")
@Data
public class PageVo {
    /**
     * 总条数
     */
    @ApiModelProperty(value = "总条数")
    protected Long total;

    /**
     * 总页数
     */
    @ApiModelProperty(value = "总页数")
    protected Long pages;

    /**
     * 当前页码
     */
    @ApiModelProperty(value = "当前页码")
    protected Long current;
}
