package com.lanhu.ai.gateway.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanhu.ai.gateway.client.model.AiImageTemplate;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AiImageTemplateMapper extends BaseMapper<AiImageTemplate> {
    int updateBatch(List<AiImageTemplate> list);

    int updateBatchSelective(List<AiImageTemplate> list);

    int batchInsert(@Param("list") List<AiImageTemplate> list);

    int insertOrUpdate(AiImageTemplate record);

    int insertOrUpdateSelective(AiImageTemplate record);
}