package com.lanhu.ai.gateway.client.vo.resp;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/06/29 9:39
 */

@ApiModel(value = "社区分页记录列表出参")
@Data
public class CommunityListPageVo {


    @ApiModelProperty(value = "列表", required = true)
    private List<CommunityInfoVo> list;


    @ApiModelProperty(value = "总条数")
    protected Long total;

    @ApiModelProperty(value = "总页数", required = true)
    @JsonProperty(value = "pageCount")
    private Long totalPages;


    @ApiModelProperty(value = "当前页数", required = true)
    @JsonProperty(value = "curPage")
    private Long currentPage;

}
