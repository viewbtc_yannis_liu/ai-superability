package com.lanhu.ai.gateway.client.core;
import com.lanhu.ai.gateway.client.utils.MessageUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


@Data
@ApiModel(value = "响应信息")
public class PcsResult<T> {
    /**
     * 接口处理结果code
     */
    @ApiModelProperty(value = "编码", name = "code", dataType = "java.lang.Integer", example = "0", required = true)
    private int code;

    /**
     * 接口返回信息
     */
    @ApiModelProperty(value = "提示", name = "msg", dataType = "java.lang.String", example = "success", required = true)
    private String message;

    /**
     * 接口返回对象
     */
    @ApiModelProperty(value = "内容", name = "data")
    private T data;

    public PcsResult() {
        this(PcsResultCode.SUCCESS);
    }


    public PcsResult(T o) {
        this(PcsResultCode.SUCCESS);
        this.data = o;
    }

    /**
     * 异常码与对应的异常信息
     *
     * @param code EnumPcsService
     */
    public PcsResult(PcsResultCode code) {
        this(code.getCode(), code.getDesc());
    }

    public PcsResult(int code, String message) {
        this.code = code;
        //this.message = MessageUtil.message(message);
        this.message = message;
    }

    /**
     * 异常码与自定义的异常信息
     *
     * @param code    EnumPcsService
     * @param message 异常信息
     */
    public PcsResult(PcsResultCode code, String message) {
        this(code);
        this.message = MessageUtil.message(message);
    }

    public void setErrorCode(PcsResultCode errorCode) {
        this.code = errorCode.getCode();
        this.message = MessageUtil.message(errorCode.getDesc());
    }

    /**
     * 返回结果是否正确
     *
     * @return 判定结果
     */
    public boolean success() {
        return this.getCode() == PcsResultCode.SUCCESS.getCode();
    }
}
