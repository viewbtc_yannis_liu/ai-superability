package com.lanhu.ai.gateway.client.handler.wx.ma;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaMessage;
import cn.binarywang.wx.miniapp.message.WxMaMessageHandler;
import cn.binarywang.wx.miniapp.message.WxMaXmlOutMessage;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.common.session.WxSessionManager;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
public class WxMaEventHandler implements WxMaMessageHandler {
    @Override
    public WxMaXmlOutMessage handle(WxMaMessage wxMessage, Map<String, Object> context, WxMaService wxService, WxSessionManager wxSessionManager) throws WxErrorException {
        return null;
    }
}
