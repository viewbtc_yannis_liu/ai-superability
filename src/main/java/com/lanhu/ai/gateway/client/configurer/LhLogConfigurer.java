package com.lanhu.ai.gateway.client.configurer;

import com.lanhu.ai.gateway.client.aspect.LhLogAspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ConditionalOnClass(LhLogAspect.class)
@ConditionalOnMissingBean(LhLogAspect.class)
public class LhLogConfigurer {
    @Bean
    public LhLogAspect lhLogAspect() {
        return new LhLogAspect();
    }
}
