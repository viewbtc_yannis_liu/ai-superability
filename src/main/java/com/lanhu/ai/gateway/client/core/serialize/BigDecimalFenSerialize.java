package com.lanhu.ai.gateway.client.core.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.lanhu.ai.gateway.client.core.ProjectConstant;

import java.io.IOException;
import java.math.BigDecimal;

public class BigDecimalFenSerialize extends JsonSerializer<BigDecimal> {
    @Override
    public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        if (value != null) {
            gen.writeNumber(value.multiply(ProjectConstant.ONE_HUNDRED).intValue());
        } else {
            gen.writeString("");
        }
    }
}
