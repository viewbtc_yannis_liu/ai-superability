package com.lanhu.ai.gateway.client.core.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 * create by yiyongyao Date:2018/3/9 下午3:50 Description: 将雪花生成器的id转为字符id ,
 * 传递到页面不会丢失精度
 */
public class LongJsonSerializer extends JsonSerializer<Long> {

    @Override
    public void serialize(Long aLong, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (aLong != null) {
            jsonGenerator.writeString(String.valueOf(aLong));
        }
    }

}
