package com.lanhu.ai.gateway.client.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lanhu.ai.gateway.client.core.BaseController;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.model.AiImageTemplate;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.service.AiImageTemplateService;
import com.lanhu.ai.gateway.client.utils.ServletUtils;
import com.lanhu.ai.gateway.client.vo.req.AiImageTemplateListPageForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/1/11 5:21 下午
 */

@Slf4j
@Api(value = "ai模版接口", tags = "ai模版接口")
@RestController
public class AiImageTemplateController extends BaseController {

    @Autowired
    private AiImageTemplateService aiImageTemplateService;




    @ApiOperation(value = "模版分页查询", notes = "模版分页查询")
    @PostMapping("/ai/template/v1/listPage")
    public PcsResult<IPage<AiImageTemplate>> listPage(@Validated @RequestBody AiImageTemplateListPageForm request) {

        ImuMemberUser imuMemberUser = this.currentUser(ServletUtils.getRequest());


        return aiImageTemplateService.listPage(imuMemberUser,request);
    }





}
