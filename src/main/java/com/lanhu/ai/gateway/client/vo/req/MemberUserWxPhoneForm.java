package com.lanhu.ai.gateway.client.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@ApiModel("微信用户授权解析手机号-老版-参数入参")
@Data
public class MemberUserWxPhoneForm {

    @ApiModelProperty(value = "会话sessionKey", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String sessionKey;


    @ApiModelProperty(value = "encryptedPhoneData" , required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String encryptedPhoneData;

    @ApiModelProperty(value = "phoneIv" , required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String phoneIv;

}
