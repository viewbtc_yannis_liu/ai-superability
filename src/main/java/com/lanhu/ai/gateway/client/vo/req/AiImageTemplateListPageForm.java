package com.lanhu.ai.gateway.client.vo.req;

import com.lanhu.ai.gateway.client.core.PageForm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/1/16 10:42 上午
 */

@ApiModel(value = "ai模版分页查询入参")
@Data
public class AiImageTemplateListPageForm extends PageForm {


    @ApiModelProperty(value = "模版名称")
    private String templateName;

    @ApiModelProperty(value = "模版风格")
    private String templateStyle;


    @ApiModelProperty(value = "模版比例")
    private String templateScale;


}
