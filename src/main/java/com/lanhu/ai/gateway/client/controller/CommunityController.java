package com.lanhu.ai.gateway.client.controller;
import com.lanhu.ai.gateway.client.annotation.IgnoreSecurity;
import com.lanhu.ai.gateway.client.core.BaseController;
import com.lanhu.ai.gateway.client.core.PageForm;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.core.Result;
import com.lanhu.ai.gateway.client.enums.CommunityOperateTypeEnum;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.service.CommunityService;
import com.lanhu.ai.gateway.client.vo.req.CommunityDetailForm;
import com.lanhu.ai.gateway.client.vo.resp.CommunityInfoVo;
import com.lanhu.ai.gateway.client.vo.resp.CommunityListPageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/06/29 9:26
 */

@RestController
@Api(tags = "社区相关")
@Slf4j
public class CommunityController extends BaseController {

    @Autowired
    private CommunityService communityService;



    @IgnoreSecurity
    @ApiOperation(value = "社区分页记录", notes = "社区分页记录")
    @PostMapping("/community/v1/listPage")
    public PcsResult<CommunityListPageVo> communityListPage(HttpServletRequest request,@Validated @RequestBody PageForm form) {
        ImuMemberUser imuMemberUser = this.currentUserIgnoreEx(request);
        CommunityListPageVo vo = communityService.communityListPage(imuMemberUser , form);
        return Result.ok(vo);
    }


    @IgnoreSecurity
    @ApiOperation(value = "社区详情", notes = "社区详情")
    @PostMapping("/community/v1/detail")
    public PcsResult<CommunityInfoVo> communityDetail(HttpServletRequest request,@Validated @RequestBody CommunityDetailForm form) {
        ImuMemberUser imuMemberUser = this.currentUserIgnoreEx(request);
        CommunityInfoVo vo = communityService.communityDetail(imuMemberUser , form);
        return Result.ok(vo);
    }



    @ApiOperation(value = "社区点赞", notes = "社区点赞")
    @PostMapping("/community/v1/like")
    public PcsResult communityLike(HttpServletRequest request,@Validated @RequestBody CommunityDetailForm form) {
        ImuMemberUser imuMemberUser = this.currentUserIgnoreEx(request);
        communityService.communityOperate(imuMemberUser , form.getId() , CommunityOperateTypeEnum.LIKE.getCode() );
        return Result.ok();
    }


    @ApiOperation(value = "社区取消点赞", notes = "社区取消点赞")
    @PostMapping("/community/v1/unlike")
    public PcsResult communityUnLike(HttpServletRequest request,@Validated @RequestBody CommunityDetailForm form) {
        ImuMemberUser imuMemberUser = this.currentUserIgnoreEx(request);
        communityService.communityUnOperate(imuMemberUser , form.getId() , CommunityOperateTypeEnum.LIKE.getCode() );
        return Result.ok();
    }



    private ImuMemberUser getTestUser(){
        ImuMemberUser imuMemberUser = new ImuMemberUser();
        imuMemberUser.setId(2l);
        return imuMemberUser;
    }

}
