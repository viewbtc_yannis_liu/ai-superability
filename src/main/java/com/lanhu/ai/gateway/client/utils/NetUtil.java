package com.lanhu.ai.gateway.client.utils;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

@Slf4j
public class NetUtil {
    /**
     * unknown
     */
    private static final String UNKNOWN = "unknown";

    /**
     * 获取本地地址
     *
     * @return 本地地址
     */
    public static ArrayList<String> getLocalIpAddress() {
        ArrayList<String> ipList = new ArrayList<>();
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface ni = interfaces.nextElement();
                Enumeration<InetAddress> ipAddrEnum = ni.getInetAddresses();
                while (ipAddrEnum.hasMoreElements()) {
                    InetAddress addr = ipAddrEnum.nextElement();
                    if (addr.isLoopbackAddress()) {
                        continue;
                    }
                    String ip = addr.getHostAddress();
                    if (ip.contains(":")) {
                        //skip the IPv6 address
                        continue;
                    }
                    log.debug("Interface: " + ni.getName() + ", IP: " + ip);
                    ipList.add(ip);
                }
            }
            Collections.sort(ipList);
        } catch (Exception e) {
            log.error("Failed to get local ip list. " + e.getMessage());
            throw new RuntimeException("Failed to get local ip list");
        }
        return ipList;
    }

    /**
     * 获取请求远程地址
     *
     * @param request request
     * @return 远程地址
     */
    public static String getRemoteIpAddress(HttpServletRequest request) {
        if (request == null) {
            return UNKNOWN;
        }

        String ip = request.getHeader("x-forwarded-for");
        if (StrUtil.isBlank(ip) || StrUtil.equalsIgnoreCase(UNKNOWN, ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (StrUtil.isBlank(ip) || StrUtil.equalsIgnoreCase(UNKNOWN, ip)) {
            ip = request.getHeader("X-Forwarded-For");
        }
        if (StrUtil.isBlank(ip) || StrUtil.equalsIgnoreCase(UNKNOWN, ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (StrUtil.isBlank(ip) || StrUtil.equalsIgnoreCase(UNKNOWN, ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if (StrUtil.isBlank(ip) || StrUtil.equalsIgnoreCase(UNKNOWN, ip)) {
            ip = request.getRemoteAddr();
        }

        return StrUtil.equals("0:0:0:0:0:0:0:1", ip) ? "127.0.0.1" : ip;
    }
}
