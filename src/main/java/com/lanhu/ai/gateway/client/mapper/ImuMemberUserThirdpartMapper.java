package com.lanhu.ai.gateway.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberUserThirdpart;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImuMemberUserThirdpartMapper extends BaseMapper<ImuMemberUserThirdpart> {
}