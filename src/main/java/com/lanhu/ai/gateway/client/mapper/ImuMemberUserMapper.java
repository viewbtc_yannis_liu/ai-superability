package com.lanhu.ai.gateway.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ImuMemberUserMapper extends BaseMapper<ImuMemberUser> {
}