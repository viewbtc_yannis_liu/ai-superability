package com.lanhu.ai.gateway.client.utils;

import java.util.Random;

public class FileNameGeneratorUtil {
    /**
     * hash迭代次数
     */
    private static final int HASH_ITERATIONS = 1024;

    /**
     * 盐值长度
     */
    private static final int SALT_SIZE = 8;

    /**
     * 字符串
     */
    private static final String STR = "ABCDEFGHJKLMNPRSTUVWXYZ0123456789";

    /**
     * 文件名称生成
     */
    public static String generateFileName() {
        String plain = EncodesUtil.unescapeHtml(randomStr() + System.currentTimeMillis());
        byte[] salt = DigestsUtil.generateSalt(SALT_SIZE);
        byte[] hashPassword = DigestsUtil.sha1(plain.getBytes(), salt, HASH_ITERATIONS);
        return EncodesUtil.encodeHex(hashPassword);
    }

    /**
     * 随机生成length长度的字符串
     *
     * @return 字符串
     */
    private static String randomStr() {
        Random r = new Random();
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < 5; i++) {
            buffer.append(r.nextInt(STR.length()));
        }
        return buffer.toString();
    }
}
