package com.lanhu.ai.gateway.client.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel(value = "图片上传入参")
@Data
public class ImageFileUploadForm {

    @ApiModelProperty(value = "文件类型(拍照图片: ai_image chatgpt图片：chatgpt_image)", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String fileType;

}
