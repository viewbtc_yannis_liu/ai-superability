package com.lanhu.ai.gateway.client.core;

public class BusinessException extends RuntimeException{
    /**
     * The error code
     */
    private Integer errorCode = PcsResultCode.SUCCESS.getCode();

    /**
     * The error
     */
    private PcsResultCode error = PcsResultCode.SUCCESS;

    public BusinessException(){}

    /**
     * Instantiates a new pcs run time exception.
     *
     * @param code the code
     */
    public BusinessException(PcsResultCode code) {
        super(code.getDesc());
        this.errorCode = code.getCode();
        this.error = code;
    }

    /**
     * Instantiates a new pcs run time exception.
     *
     * @param code the code
     * @param message the message
     */
    public BusinessException(PcsResultCode code, String message) {
        super(message);
        this.errorCode = code.getCode();
        this.error = code;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public PcsResultCode getError() {
        return error;
    }

    public void setError(PcsResultCode error) {
        this.error = error;
    }
}
