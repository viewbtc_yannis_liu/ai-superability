package com.lanhu.ai.gateway.client.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(value = "ai_image_template")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ai_image_template")
public class AiImageTemplate {
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "")
    private Long id;

    /**
     * 模版名称
     */
    @TableField(value = "template_name")
    @ApiModelProperty(value = "模版名称")
    private String templateName;

    /**
     * 模版图片
     */
    @TableField(value = "template_image")
    @ApiModelProperty(value = "模版图片")
    private String templateImage;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 模版风格
     */
    @TableField(value = "template_style")
    @ApiModelProperty(value = "模版风格")
    private String templateStyle;

    /**
     * 模版比例
     */
    @TableField(value = "template_scale")
    @ApiModelProperty(value = "模版比例")
    private String templateScale;

    /**
     * 创建人ID
     */
    @TableField(value = "create_by")
    @ApiModelProperty(value = "创建人ID")
    private Long createBy;

    /**
     * 修改人
     */
    @TableField(value = "update_by")
    @ApiModelProperty(value = "修改人")
    private Long updateBy;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 模版费用(0:免费 其他:所需要积分)
     */
    @TableField(value = "template_cost")
    @ApiModelProperty(value = "模版费用(0:免费 其他:所需要积分)")
    private BigDecimal templateCost;

    /**
     * 是否有效(0:有效 1删除)
     */
    @TableField(value = "is_effect")
    @ApiModelProperty(value = "是否有效(0:有效 1删除)")
    private Integer isEffect;

    /**
     * 越大排前面
     */
    @TableField(value = "order_by")
    @ApiModelProperty(value = "越大排前面")
    private Integer orderBy;

    /**
     * 描述
     */
    @TableField(value = "prompt")
    @ApiModelProperty(value = "描述")
    private String prompt;
}