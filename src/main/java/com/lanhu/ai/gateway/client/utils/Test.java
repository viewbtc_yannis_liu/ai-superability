package com.lanhu.ai.gateway.client.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/03/24 13:07
 */
public class Test {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(99);
        list.add(2);
        list.add(1);
        list.add(16);
        list.add(19);
        list.add(21);
        list.add(88);
        list.add(77);
        list.add(65);

        Integer max = Collections.max(list);
        Integer min = Collections.min(list);

        System.out.println("max:" + max);
        System.out.println("min:" + min);
    }
}
