package com.lanhu.ai.gateway.client.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "上传图片出参")
@Data
public class ImageFileUploadVo {
    /**
     * 图片地址
     */
    @ApiModelProperty(value = "图片地址", required = true)
    private String img;
}
