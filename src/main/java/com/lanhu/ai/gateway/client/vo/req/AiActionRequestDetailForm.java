package com.lanhu.ai.gateway.client.vo.req;

import com.lanhu.ai.gateway.client.core.PageForm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/1/16 10:42 上午
 */

@ApiModel(value = "ai操作请求结果详情入参")
@Data
public class AiActionRequestDetailForm  {


    @ApiModelProperty(value = "请求ID", required = true)
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    private Integer requestId;




}
