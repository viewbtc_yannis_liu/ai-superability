package com.lanhu.ai.gateway.client.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/06/26 17:50
 */
@ApiModel(value = "社区改变状态入参")
@Data
public class CommunityChangeStatusForm {

    @ApiModelProperty(value = "社区ID", required = true)
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    private Long id;


    @ApiModelProperty(value = "状态 0撤销 1发布", required = true)
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    private Integer status;
}
