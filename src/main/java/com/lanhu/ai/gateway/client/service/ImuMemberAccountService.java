package com.lanhu.ai.gateway.client.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.lanhu.ai.gateway.client.annotation.LhTransaction;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.core.Result;
import com.lanhu.ai.gateway.client.mapper.ImuMemberAccountChangeRecordMapper;
import com.lanhu.ai.gateway.client.mapper.ImuMemberAccountMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberAccount;
import com.lanhu.ai.gateway.client.model.ImuMemberAccountChangeRecord;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/6/28 1:22 下午
 */
@Service
@Slf4j
public class ImuMemberAccountService {

    @Autowired
    private ImuMemberAccountMapper imuMemberAccountMapper;

    @Autowired
    private ImuMemberAccountChangeRecordMapper imuMemberAccountChangeRecordMapper;


    /**
    * @description: chatgpt服务扣款
    * @param: [imuMemberUser, reduceAccount]
    * @return: com.lanhu.ai.gateway.client.core.PcsResult
    * @author: liuyi
    * @date: 1:25 下午 2023/6/28
    */
    @LhTransaction
    public PcsResult chatgptServiceReduce(ImuMemberUser imuMemberUser, BigDecimal reduceAmount,String desc){

        ImuMemberAccount imuMemberAccount = imuMemberAccountMapper.selectByUserId(imuMemberUser.getId());



        if(imuMemberAccount != null){

            int count = imuMemberAccountMapper.reduceBalance(imuMemberUser.getId(),reduceAmount);

            if(count >0){
                ImuMemberAccountChangeRecord imuMemberAccountChangeRecord =ImuMemberAccountChangeRecord.builder()
                        .id(IdUtil.getSnowflakeNextId())
                        .userId(imuMemberUser.getId())
                        .accountId(imuMemberAccount.getUserId())
                        .createTime(DateUtil.currentSeconds())
                        .updateTime(DateUtil.currentSeconds())
                        .afterPoint(imuMemberAccount.getAvailablePoint().subtract(reduceAmount))
                        .beforePoint(imuMemberAccount.getAvailablePoint())
                        .desc(desc)
                        .changePoint(reduceAmount)
                        .isDelete(0)
                        .type(1)
                        .build();

                imuMemberAccountChangeRecordMapper.insert(imuMemberAccountChangeRecord);
            }


        }


        return Result.ok();


    }
}
