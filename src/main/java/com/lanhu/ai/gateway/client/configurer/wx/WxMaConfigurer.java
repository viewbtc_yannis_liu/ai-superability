package com.lanhu.ai.gateway.client.configurer.wx;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import cn.binarywang.wx.miniapp.message.WxMaMessageRouter;
import com.google.common.collect.Maps;
import com.lanhu.ai.gateway.client.handler.wx.ma.*;
import com.lanhu.ai.gateway.client.properties.wx.WxMaProperties;
import me.chanjar.weixin.common.api.WxConsts;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Configuration
@EnableConfigurationProperties(WxMaProperties.class)
public class WxMaConfigurer {
    @Resource
    private WxMaLogHandler logHandler;

    @Resource
    private WxMaPicHandler picHandler;

    @Resource
    private WxMaQrCodeHandler qrCodeHandler;

    @Resource
    private WxMaTemplateMsgHandler templateMsgHandler;

    @Resource
    private WxMaTextHandler textHandler;

    @Resource
    private WxMaEventHandler eventHandler;

    @Resource
    private WxMaProperties properties;

    /**
     * 消息事件路由
     */
    private static final Map<String, WxMaMessageRouter> ROUTERS = Maps.newHashMap();

    /**
     * 小程序service
     */
    private static Map<String, WxMaService> maServices = Maps.newHashMap();

    public static WxMaMessageRouter getRouter(String appId) {
        return ROUTERS.get(appId);
    }

    private static WxMaProperties.Config maConfig ;

    public static WxMaService getMaService(String appId) {
        WxMaService wxService = maServices.get(appId);
        if (wxService == null) {
            throw new IllegalArgumentException(String.format("未找到对应appId=[%s]的配置，请核实！", appId));
        }

        return wxService;
    }

    public static WxMaProperties.Config getMaConfig (){
        if (maConfig == null){
            throw new IllegalArgumentException(String.format("未找到对应的配置，请核实！"));
        }
        return maConfig;
    }

    @PostConstruct
    public void init() {
        List<WxMaProperties.Config> configs = this.properties.getConfigs();
        if (configs == null) {
            throw new RuntimeException("大哥，拜托先看下项目首页的说明（readme文件），添加下相关配置，注意别配错了！");
        }

        maServices = configs.stream()
                .map(a -> {
                    WxMaDefaultConfigImpl config = new WxMaDefaultConfigImpl();
                    config.setAppid(a.getAppid());
                    config.setSecret(a.getSecret());
                    config.setToken(a.getToken());
                    config.setAesKey(a.getAesKey());
                    config.setMsgDataFormat(a.getMsgDataFormat());

                    WxMaService service = new WxMaServiceImpl();
                    service.setWxMaConfig(config);
                    ROUTERS.put(a.getAppid(), this.newRouter(service));
                    maConfig = a ;
                    return service;
                }).collect(Collectors.toMap(s -> s.getWxMaConfig().getAppid(), a -> a));
    }

    private WxMaMessageRouter newRouter(WxMaService service) {
        final WxMaMessageRouter router = new WxMaMessageRouter(service);

        router.rule().handler(logHandler).next();

        router.rule()
                .async(false)
                .msgType(WxConsts.XmlMsgType.EVENT)
                .handler(eventHandler)
                .end();

        router.rule()
                .async(false)
                .msgType(WxConsts.XmlMsgType.TEXT)
                .handler(textHandler)
                .end();

        router.rule()
                .async(false)
                .msgType(WxConsts.XmlMsgType.IMAGE)
                .handler(picHandler)
                .end();

        router.rule()
                .async(false)
                .handler(templateMsgHandler)
                .end();

        router.rule()
                .async(false)
                .handler(qrCodeHandler)
                .end();

        return router;
    }
}
