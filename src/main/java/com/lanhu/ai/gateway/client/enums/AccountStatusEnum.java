package com.lanhu.ai.gateway.client.enums;

/**
 * 会员账号状态   0:正常 1:冻结
 */
public enum AccountStatusEnum {

    NORMAL(0, "正常"),

    FREEZE(1, "冻结"),
    ;

    private final int code;

    private final String msg;

    AccountStatusEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static AccountStatusEnum convert(int code) {
        for (AccountStatusEnum statusEnum : AccountStatusEnum.values()) {
            if (statusEnum.getCode() == code) {
                return statusEnum;
            }
        }
        return AccountStatusEnum.NORMAL;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }


}
