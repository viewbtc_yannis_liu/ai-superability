package com.lanhu.ai.gateway.client.aspect;

import cn.hutool.core.util.ArrayUtil;
import com.lanhu.ai.gateway.client.utils.NetUtil;
import com.lanhu.ai.gateway.client.utils.JacksonUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Slf4j
@Aspect
@Component
public class LhLogAspect {


    @Value("${log.print.response:false}")
    private boolean printResponse;


    @Value("${log.print.request:true}")
    private boolean printRequest;

    @Pointcut("execution(public * com.lanhu.ai.gateway.client.controller.*.*(..)))")
    public void controllerPointcut() {
    }

    /**
     * 在切点之前织入
     *
     * @param joinPoint 切点
     */
    @Before("controllerPointcut()")
    public void doBefore(JoinPoint joinPoint) {
        // 开始打印请求日志
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = Objects.requireNonNull(attributes).getRequest();
        String requestUrl = request.getRequestURL().toString();


        // 打印请求相关参数
        log.info("========================================== Start ==========================================");
        // 打印请求 url
        log.info("URL                       : {}", requestUrl);
        // 打印 Http method
        log.info("HTTP Method               : {}", request.getMethod());
        // 打印调用 controller 的全路径以及执行方法
        log.info("Class Method              : {}.{}", joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
        // 打印请求的 IP
        log.info("IP                        : {}", NetUtil.getRemoteIpAddress(request));

        if(printRequest){
            // 打印请求入参
            Object[] args = joinPoint.getArgs();
            List<Object> filterArgs = filterArgs(args);

            if (filterArgs.size() > 0) {
                log.info("Request Args              : {}", serialization(filterArgs));
            } else {
                log.info("Request Args              : {}", "[]");
            }
        }

    }

    /**
     * 环绕
     *
     * @param proceedingJoinPoint 切点
     * @return object
     * @throws Throwable 异常
     */
    @Around("controllerPointcut()")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Object result = proceedingJoinPoint.proceed();
        stopWatch.stop();
        long timeConsuming = stopWatch.getTotalTimeMillis();
        if(printResponse){
            // 打印出参
            log.info("Response Args             : {}", serialization(result));
            // 执行耗时
            log.info("Time-Consuming            : {} ms", timeConsuming);

        }

        // 接口结束后换行，方便分割查看
        log.info("=========================================== End ===========================================");

        return result;
    }

    // ======================================================== 私用方法 ======================================================== //

    /**
     * 序列化
     *
     * @param obj 待序列化对象
     * @return 序列化的json字符串
     */
    private String serialization(Object obj) {
        try {
            return JacksonUtil.serialize(obj);
        } catch (Exception e) {
            log.error("JSON序列化出现错误！", e);
            return "json serialization error";
        }
    }

    /**
     * 过滤不可序列化的参数
     *
     * @param args 参数
     * @return 过滤后的参数
     */
    private List<Object> filterArgs(Object[] args) {
        return streamOf(args)
                .filter(arg -> (!(arg instanceof HttpServletRequest) && !(arg instanceof HttpServletResponse)))
                .collect(Collectors.toList());
    }

    /**
     * stream流
     *
     * @param array 数组
     * @param <T>   类型
     * @return stream流
     */
    private static <T> Stream<T> streamOf(T[] array) {
        return ArrayUtil.isEmpty(array) ? Stream.empty() : Arrays.stream(array);
    }
}
