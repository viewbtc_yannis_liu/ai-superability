package com.lanhu.ai.gateway.client.core;

import java.math.BigDecimal;
import java.util.TimeZone;

public class ProjectConstant {
    /**
     * 时区
     */
    //public static final TimeZone TIME_ZONE = TimeZone.getTimeZone("GMT+9");  //日本
    public static final TimeZone TIME_ZONE = TimeZone.getTimeZone("GMT+8");    //中国

    /**
     * 成功
     */
    public static final String SUCCESS = "SUCCESS";

    /**
     * ThreadLocal的用户标记
     */
    public static final String THREAD_LOCAL_MEMBER_FLAG = "memberUser";

    /**
     * ThreadLocal的appId标记
     */
    public static final String THREAD_LOCAL_WX_APP_ID_FLAG = "wxAppId";



    /**
     * 一百
     */
    public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    /**
     * 请求头——token
     */
    public static final String HEADER_TOKEN = "TOKEN";

    /**
     * 请求头标记——language
     */
    public static final String HEADER_LANGUAGE_FLAG = "LANGUAGE";

    /**
     * 请求头标记——platform
     */
    public static final String HEADER_PLATFORM_FLAG = "PLATFORM";

    /**
     * 请求头标记——appId
     */
    public static final String HEADER_APPID_FLAG = "APPID";



    // ================================================================================== 方法区 ================================================================================== //

    /**
     * 组装sql的limit
     *
     * @param num 数量
     * @return limit字符串
     */
    public static String assemblySqlLimit(int num) {
        return "limit " + num;
    }


    /**
     * 空字符
     */
    public static final String EMPTY = "";


    public  static final String UTF8 ="UTF-8";

    /**
     * 逗号
     */
    public static final String COMMA = ",";


    public static final String SCENARIO_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";


    public static final String CHATGPT_IMAGE_TYPE = "png";

    public static final String CHATGPT_IMAGE_TYPE_CASE = "PNG";


    public static final String WRAP = "\n";

    public static final int TEN = 10;
}
