package com.lanhu.ai.gateway.client.controller;
import com.lanhu.ai.gateway.client.annotation.IgnoreSecurity;
import com.lanhu.ai.gateway.client.core.BaseController;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.core.ResponseResult;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.service.WxpayService;
import com.lanhu.ai.gateway.client.vo.req.WxOrderPayForm;
import com.lanhu.ai.gateway.client.vo.resp.MemberPriceListVo;
import com.lanhu.ai.gateway.client.vo.resp.UnifiedOrderResultVo;
import com.lanhu.ai.gateway.client.wxpaysdk.ConvertUtils;
import com.lanhu.ai.gateway.client.wxpaysdk.WXPayUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
@Api(tags = "小程序支付")
@Slf4j
public class WxPayController extends BaseController {

    @Autowired
    private WxpayService wxpayService;



    @IgnoreSecurity
    @ApiOperation(value = "获取价格列表" , notes = "获取价格列表")
    @PostMapping("pay/v1/priceList")
    public PcsResult<MemberPriceListVo> priceList(HttpServletRequest request) throws Exception {
        ImuMemberUser imuMemberUser = this.currentUserIgnoreEx(request);
        MemberPriceListVo vo = wxpayService.priceList(imuMemberUser);
        return ResponseResult.ok(vo);
    }


    /**
     * JSAPI调起支付
     *
     * @param request 请求体
     * @param form  微信订单支付入参
     * @return 调起支付所需参数
     */
    @ApiOperation(value = "微信调起支付" , notes = "微信调起支付")
    @PostMapping("pay/v1/wxpay")
    public PcsResult<UnifiedOrderResultVo> pay(HttpServletRequest request, @RequestBody WxOrderPayForm form) throws Exception {
        ImuMemberUser imuMemberUser = this.currentUser(request);
        //获取请求ip
        String ip = request.getHeader("x-forwarded-for");
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getHeader("Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getHeader("WL-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getRemoteAddr();
        }
        if(ip.indexOf(",") != -1){
            String[] ips = ip.split(",");
            ip = ips[0].trim();
        }
        UnifiedOrderResultVo vo = wxpayService.wxpay(imuMemberUser , form , ip);
        return ResponseResult.ok(vo);
    }


    /**
     * 接收支付成功的接口，返回 return_code为 SUCCESS
     * 如果没有这个接口，微信服务器会隔一段时间通知一次
     * JSPAI 支付在JS中可以进行回调，可以简单通知微信服务器成功支付，也可直接处理
     *
     * @param request 请求题
     * @return 响应结果
     */
    @ApiOperation(value = "微信支付结果回调通知" , notes = "微信支付结果回调通知")
    @IgnoreSecurity
    @GetMapping("pay/v1/callback")
    public String payCallBack(HttpServletRequest request, @RequestBody String returnCode) throws Exception {
        log.info("WxPayController payCallBack  returnCode:{}" , returnCode );
        // 支付成功后微信讲通知我们
        // 获取微信带来的订单信息
        String data = ConvertUtils.convertToString(request.getInputStream());
        wxpayService.payCallBack(data , returnCode );

        // 响应微信服务器
        Map<String, String> result = new HashMap<>();
        if ("SUCCESS".equals(returnCode)) {
            result.put("return_code", "SUCCESS");
            result.put("return_msg", "OK");
        }
        return WXPayUtil.mapToXml(result);
    }

}
