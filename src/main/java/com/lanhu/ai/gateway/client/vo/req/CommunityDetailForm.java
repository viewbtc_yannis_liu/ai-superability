package com.lanhu.ai.gateway.client.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/06/26 17:50
 */
@ApiModel(value = "社区详情入参")
@Data
public class CommunityDetailForm {

    @ApiModelProperty(value = "社区ID", required = true)
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    private Long id;

}
