package com.lanhu.ai.gateway.client.enums;

/**
 * @author liuyi
 * @version 1.0
 * @date 2022/7/20 6:58 下午
 */
public enum ChatGptModelEnum {

    /**
     * 其它
     */

    //  /v1/chat/completions
    gpt_4("gpt-4", "gpt-4"),
    gpt_4_0613("gpt-4-0613", "gpt-4-0613"),
    gpt_4_32k("gpt-4-32k", "gpt-4-32k"),
    gpt_4_32k_0613("gpt-4-32k-0613", "gpt-4-32k-0613"),
    gpt_35_turbo("gpt-3.5-turbo", "gpt-3.5-turbo"),
    gpt_35_turbo_0613("gpt-3.5-turbo-0613", "gpt-3.5-turbo-0613"),
    gpt_35_turbo_16k("gpt-3.5-turbo-16k", "gpt-3.5-turbo-16k"),
    gpt_35_turbo_16k_0613("gpt-3.5-turbo-16k-0613", "gpt-3.5-turbo-16k-0613"),

    // /v1/completions
    text_davinci_003("text-davinci-003", "text-davinci-003"),
    text_davinci_002("text-davinci-002", "text-davinci-002"),
    text_curie_001("text-curie-001", "text-curie-001"),
    text_babbage_001("text-babbage-001", "text-babbage-001"),
    text_ada_001("text-ada-001", "text-ada-001"),

    // /v1/edits
    text_davinci_edit_001("text-davinci-edit-001", "text-davinci-edit-001"),
    code_davinci_edit_001("code-davinci-edit-001", "code-davinci-edit-001"),

    // /v1/audio/transcriptions
    // /v1/audio/translations
    whisper_1("whisper-1", "whisper-1"),


    //  /v1/fine-tunes


    davinci("davinci", "davinci"),
    curie("curie", "curie"),
    babbage("babbage", "babbage"),
    ada("ada", "ada"),


    // /v1/embeddings

    text_embedding_ada_002("text-embedding-ada-002", "text-embedding-ada-002"),
    text_search_ada_doc_001("text-search-ada-doc-001", "text-search-ada-doc-001"),


    // /v1/moderations
    text_moderation_stable("text-moderation-stable", "text-moderation-stable"),
    text_moderation_latest("text-moderation-latest", "text-moderation-latest"),



    ;
    private final String code;

    private final String msg;

    ChatGptModelEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
