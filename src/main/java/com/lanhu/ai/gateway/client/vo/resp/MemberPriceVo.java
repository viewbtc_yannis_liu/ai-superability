package com.lanhu.ai.gateway.client.vo.resp;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/06/28 17:51
 */
@ApiModel("会员价格信息出参")
@Data
public class MemberPriceVo {


    @ApiModelProperty(value = "ID")
    private Long id;

    @ApiModelProperty(value = "价格名称")
    private String name;

    @ApiModelProperty(value = "价格描述")
    private String desc;

    @ApiModelProperty(value = "价格 (元)")
    private BigDecimal price;

    @ApiModelProperty(value = "积分")
    private Integer point;

    @ApiModelProperty(value = "单次 (元)")
    private BigDecimal unitPrice;


}
