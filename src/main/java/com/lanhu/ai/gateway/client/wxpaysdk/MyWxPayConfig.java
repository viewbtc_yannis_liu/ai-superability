package com.lanhu.ai.gateway.client.wxpaysdk;

import com.lanhu.ai.gateway.client.configurer.wx.WxMaConfigurer;

import java.io.InputStream;

/**
 * @author durance
 */
public class MyWxPayConfig extends WXPayConfig{


    @Override
    String getAppID() {
        return WxMaConfigurer.getMaConfig().getAppid();
    }

    @Override
    String getMchID() {
        return WxMaConfigurer.getMaConfig().getMchId();
    }

    @Override
    String getKey() {
        return WxMaConfigurer.getMaConfig().getMchKey();
    }

    @Override
    InputStream getCertStream() {
        return null;
    }

    @Override
    IWXPayDomain getWXPayDomain() {
        return new IWXPayDomain() {
            @Override
            public void report(String domain, long elapsedTimeMillis, Exception ex) {

            }

            @Override
            public DomainInfo getDomain(WXPayConfig config) {
                return new IWXPayDomain.DomainInfo(WXPayConstants.DOMAIN_API,true);
            }
        };
    }
}
