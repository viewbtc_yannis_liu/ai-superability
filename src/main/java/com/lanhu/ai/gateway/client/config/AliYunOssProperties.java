package com.lanhu.ai.gateway.client.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/********************************
 * @title AliYunOssProperties
 * @package com.lanhu.imenu.gateway.client.config
 * @description description
 *
 * @date 2022/11/17 2:19 下午
 * @version 0.0.1
 *********************************/
@ConfigurationProperties(prefix = "oss")
@Data
public class AliYunOssProperties {
    /**
     * accessKeyId
     */
    private String accessKeyId;

    /**
     * accessKeySecret
     */
    private String accessKeySecret;

    /**
     * endPoint
     */
    private String endPoint;

    private String internalEndPoint;

    /**
     * bucketName
     */
    private String bucketName;

    /**
     * domain
     */
    private String domain;

    private String imageUrl;
}
