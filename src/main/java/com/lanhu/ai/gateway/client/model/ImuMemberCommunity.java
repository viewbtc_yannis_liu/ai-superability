package com.lanhu.ai.gateway.client.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 会员社区表
 */
@ApiModel(value = "会员社区表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "imu_member_community")
public class ImuMemberCommunity {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "ID")
    private Long id;

    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 标题
     */
    @TableField(value = "title")
    @ApiModelProperty(value = "标题")
    private String title;

    /**
     * 内容
     */
    @TableField(value = "content")
    @ApiModelProperty(value = "内容")
    private String content;

    /**
     * 图片,多个逗号
     */
    @TableField(value = "img")
    @ApiModelProperty(value = "图片,多个逗号")
    private String img;

    /**
     * 关键词
     */
    @TableField(value = "word")
    @ApiModelProperty(value = "关键词")
    private String word;

    /**
     * 模版风格
     */
    @TableField(value = "template_style")
    @ApiModelProperty(value = "模版风格")
    private String templateStyle;

    /**
     * 模版比例
     */
    @TableField(value = "template_scale")
    @ApiModelProperty(value = "模版比例")
    private String templateScale;

    /**
     * 状态  0未发布，1已发布
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "状态  0未发布，1已发布")
    private Integer status;

    /**
     * 预览数
     */
    @TableField(value = "preview_count")
    @ApiModelProperty(value = "预览数")
    private Integer previewCount;

    /**
     * 点赞数
     */
    @TableField(value = "like_count")
    @ApiModelProperty(value = "点赞数")
    private Integer likeCount;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "更新时间")
    private Long updateTime;

    /**
     * 逻辑删除的标记  0正常 , 1删除
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除的标记  0正常 , 1删除")
    private Integer isDelete;
}