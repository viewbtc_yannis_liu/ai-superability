package com.lanhu.ai.gateway.client.configurer.wx;

import com.google.common.collect.Maps;
import com.lanhu.ai.gateway.client.handler.wx.mp.WxMpLogHandler;
import com.lanhu.ai.gateway.client.handler.wx.mp.WxMpMsgHandler;
import com.lanhu.ai.gateway.client.handler.wx.mp.WxMpScanHandler;
import com.lanhu.ai.gateway.client.handler.wx.mp.WxMpSubscribeHandler;
import com.lanhu.ai.gateway.client.properties.wx.WxMpProperties;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.mp.api.WxMpMessageRouter;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Configuration
@EnableConfigurationProperties(WxMpProperties.class)
public class WxMpConfigurer {
    @Resource
    private WxMpProperties wxMpProperties;

    @Resource
    private WxMpMsgHandler msgHandler;

    @Resource
    private WxMpLogHandler logHandler;

    @Resource
    private WxMpSubscribeHandler subscribeHandler;

    @Resource
    private WxMpScanHandler scanHandler;

    /**
     * 消息事件路由
     */
    private static final Map<String, WxMpMessageRouter> ROUTERS = Maps.newHashMap();
    /**
     * 公众号service
     */
    private static Map<String, WxMpService> mpServices = Maps.newHashMap();

    public static WxMpMessageRouter getRouters(String appId) {
        WxMpMessageRouter wxMpMessageRouter = ROUTERS.get(appId);
        if (wxMpMessageRouter == null) {
            throw new IllegalArgumentException(String.format("未找到对应appid=[%s]的配置，请核实！", appId));
        }

        return wxMpMessageRouter;
    }

    public static WxMpService getMpService(String appId) {
        WxMpService wxService = mpServices.get(appId);
        if (wxService == null) {
            throw new IllegalArgumentException(String.format("未找到对应appid=[%s]的配置，请核实！", appId));
        }

        return wxService;
    }

    @PostConstruct
    public void initServices() {
        // 代码里 getConfigs()处报错的同学，请注意仔细阅读项目说明，你的IDE需要引入lombok插件！！！！
        final List<WxMpProperties.MpConfig> configs = this.wxMpProperties.getConfigs();
        if (configs == null) {
            throw new RuntimeException("大哥，拜托先看下项目首页的说明（readme文件），添加下相关配置，注意别配错了！");
        }

        mpServices = configs.stream().map(a -> {
            WxMpDefaultConfigImpl configStorage = new WxMpDefaultConfigImpl();
            configStorage.setAppId(a.getAppId());
            configStorage.setSecret(a.getSecret());
            configStorage.setToken(a.getToken());
            configStorage.setAesKey(a.getAesKey());

            WxMpService service = new WxMpServiceImpl();
            service.setWxMpConfigStorage(configStorage);
            ROUTERS.put(a.getAppId(), this.newRouter(service));
            return service;
        }).collect(Collectors.toMap(s -> s.getWxMpConfigStorage().getAppId(), a -> a, (o, n) -> o));
    }

    private WxMpMessageRouter newRouter(WxMpService wxMpService) {
        final WxMpMessageRouter newRouter = new WxMpMessageRouter(wxMpService);

        // 记录所有事件的日志 （异步执行）
        newRouter.rule()
                .async(false)
                .handler(this.logHandler)
                .next();

        // 关注事件
        newRouter.rule()
                .async(false)
                .msgType(WxConsts.XmlMsgType.EVENT)
                .event(WxConsts.EventType.SUBSCRIBE)
                .handler(this.subscribeHandler)
                .end();

        // 扫码事件
        newRouter.rule()
                .async(false)
                .msgType(WxConsts.XmlMsgType.EVENT)
                .event(WxConsts.EventType.SCAN)
                .handler(this.scanHandler)
                .end();

        // 默认
        newRouter.rule()
                .async(false)
                .handler(this.msgHandler)
                .end();

        return newRouter;
    }
}
