package com.lanhu.ai.gateway.client.interceptor;

import cn.hutool.core.util.StrUtil;
import com.lanhu.ai.gateway.client.core.BusinessException;
import com.lanhu.ai.gateway.client.core.PcsResultCode;
import com.lanhu.ai.gateway.client.core.ProjectConstant;
import com.lanhu.ai.gateway.client.enums.RequestHeaderPlatformEnum;
import com.lanhu.ai.gateway.client.utils.ThreadLocalUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class WxAppIdInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) {
        String platform = httpServletRequest.getHeader(ProjectConstant.HEADER_PLATFORM_FLAG);
        String appId = httpServletRequest.getHeader(ProjectConstant.HEADER_APPID_FLAG);
        if (StrUtil.equals(platform, RequestHeaderPlatformEnum.WECHAT.getPlatform()) && StrUtil.isBlank(appId)) {
            throw new BusinessException(PcsResultCode.PLAT_WX_APPID_EMPTY);
        }
        ThreadLocalUtil.put(ProjectConstant.THREAD_LOCAL_WX_APP_ID_FLAG, appId);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        ThreadLocalUtil.clear();
    }
}
