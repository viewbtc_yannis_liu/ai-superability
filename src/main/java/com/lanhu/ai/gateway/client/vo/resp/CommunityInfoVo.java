package com.lanhu.ai.gateway.client.vo.resp;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lanhu.ai.gateway.client.core.serialize.Timestamp2DateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/06/29 9:39
 */
@ApiModel(value = "社区信息出参")
@Data
public class CommunityInfoVo {

    @ApiModelProperty(value = "ID")
    private Long id;


    @ApiModelProperty(value = "用户ID")
    private Long userId;

    @ApiModelProperty(value = "昵称")
    private String nick;


    @ApiModelProperty(value = "标题")
    private String title;


    @ApiModelProperty(value = "内容")
    private String content;


    @ApiModelProperty(value = "图片,多个逗号")
    private String img;


    @ApiModelProperty(value = "关键词")
    private String word;


    @ApiModelProperty(value = "模版风格")
    private String templateStyle;


    @ApiModelProperty(value = "模版比例")
    private String templateScale;

    @ApiModelProperty(value = "状态  0未发布，1已发布")
    private Integer status;


    @ApiModelProperty(value = "预览数")
    private Integer previewCount;


    @ApiModelProperty(value = "点赞数")
    private Integer likeCount;


    @ApiModelProperty(value = "创建时间")
    @JsonSerialize(using = Timestamp2DateTimeSerializer.class)
    private Long createTime;


    @ApiModelProperty(value = "更新时间")
    @JsonSerialize(using = Timestamp2DateTimeSerializer.class)
    private Long updateTime;


    @ApiModelProperty(value = "是否点赞 0:未点赞  1:已点赞")
    private Integer isLike = 0 ;

}
