package com.lanhu.ai.gateway.client.enums;


/**
 * 社区操作类型  0:预览1:点赞
 */
public enum CommunityOperateTypeEnum {

    PREVIEW(0, "预览"),

    LIKE(1, "点赞"),
    ;

    private final int code;

    private final String msg;

    CommunityOperateTypeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static CommunityOperateTypeEnum convert(int code) {
        for (CommunityOperateTypeEnum typeEnum : CommunityOperateTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return CommunityOperateTypeEnum.PREVIEW;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }


}
