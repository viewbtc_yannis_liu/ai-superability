package com.lanhu.ai.gateway.client.enums;

/**
 * 订单状态 0未支付，1已支付
 */
public enum OrderStatus {

    UNPAY(0, "未支付"),


    PAYED(1, "已支付"),
    ;

    private final int code;

    private final String msg;

    OrderStatus(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static OrderStatus convert(int code) {
        for (OrderStatus orderStatus : OrderStatus.values()) {
            if (orderStatus.getCode() == code) {
                return orderStatus;
            }
        }
        return OrderStatus.UNPAY;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }



}
