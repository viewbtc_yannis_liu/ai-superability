package com.lanhu.ai.gateway.client.core.jwt;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtResult {
    /**
     * 状态
     */
    private String state;

    /**
     * 数据
     */
    private Object data;

    /**
     * 根据token返回userId
     *
     * @return userId
     */
    public Long getUserId() {
        Long memberId = null;
        JSONObject jsonObj = null;
        if (data != null) {
            jsonObj = JSONUtil.parseObj(data.toString());
        }
        if (jsonObj != null) {
            memberId = jsonObj.getLong("userId");
        }
        return memberId;
    }
}
