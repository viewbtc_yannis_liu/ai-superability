package com.lanhu.ai.gateway.client.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.core.Result;
import com.lanhu.ai.gateway.client.enums.EffectEnum;
import com.lanhu.ai.gateway.client.mapper.AiImageTemplateMapper;
import com.lanhu.ai.gateway.client.model.AiImageTemplate;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.vo.req.AiImageTemplateListPageForm;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/6/27 12:16 下午
 */
@Service
@Slf4j
public class AiImageTemplateService {

    @Autowired
    private AiImageTemplateMapper aiImageTemplateMapper;



    /**
     * @description: 分页查询
     * @param: [plaPcUser, request]
     * @return: com.lanhu.imenu.gateway.pc.core.PcsResult
     * @author: liuyi
     * @date: 8:21 下午 2023/1/15
     */
    public PcsResult<IPage<AiImageTemplate>> listPage(ImuMemberUser imuMemberUser, AiImageTemplateListPageForm request) {

        LambdaQueryWrapper<AiImageTemplate> wrapper = Wrappers.lambdaQuery();


        if(StringUtils.isNotBlank(request.getTemplateScale())){
            wrapper.eq(AiImageTemplate::getTemplateScale,request.getTemplateScale());
        }


        if(StringUtils.isNotBlank(request.getTemplateStyle())){
            wrapper.eq(AiImageTemplate::getTemplateStyle,request.getTemplateStyle());
        }


        if(StringUtils.isNotBlank(request.getTemplateName())){
            wrapper.like(AiImageTemplate::getTemplateName,request.getTemplateName());
        }

        wrapper.eq(AiImageTemplate::getIsEffect, EffectEnum.YES_EFFECT.getCode());


        // 默认根据addTime 倒序排列
        wrapper.orderByDesc(AiImageTemplate::getOrderBy);

        Page<AiImageTemplate> page = new Page<>(request.getPageIndex(), request.getPageSize());


        IPage<AiImageTemplate> pageList = aiImageTemplateMapper.selectPage(page, wrapper);




        return Result.ok(pageList);

    }


}
