package com.lanhu.ai.gateway.client.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum MemberUserGenderEnum {
    /**
     * 未知
     */
    UNKNOWN(new Byte("0"), "未知"),

    /**
     * 男
     */
    MEN(new Byte("1"), "男"),

    /**
     * 女
     */
    WOMEN(new Byte("2"), "女"),
    ;

    private final byte gender;

    private final String msg;
}
