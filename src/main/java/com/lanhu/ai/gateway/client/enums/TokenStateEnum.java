package com.lanhu.ai.gateway.client.enums;

import lombok.Getter;

@Getter
public enum TokenStateEnum {
    /**
     * 过期
     */
    EXPIRED("EXPIRED", "token 过期"),
    /**
     * 无效(token不合法)
     */
    INVALID("EXPIRED", "token 不合法"),
    /**
     * 有效的
     */
    VALID("VALID", "token 合法");

    private final String state;

    private final String msg;

    TokenStateEnum(String state, String msg) {
        this.state = state;
        this.msg = msg;
    }

    /**
     * 根据状态字符串获取token状态枚举对象
     *
     * @param tokenStateEnum token状态
     * @return token状态枚举对象
     */
    public static TokenStateEnum getTokenState(String tokenStateEnum) {
        TokenStateEnum[] states = TokenStateEnum.values();
        TokenStateEnum ts = null;
        for (TokenStateEnum state : states) {
            if (state.toString().equals(tokenStateEnum)) {
                ts = state;
                break;
            }
        }
        return ts;
    }
}
