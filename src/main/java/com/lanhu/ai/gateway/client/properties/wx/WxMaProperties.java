package com.lanhu.ai.gateway.client.properties.wx;

import cn.hutool.core.io.FileUtil;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties(prefix = "wx.ma")
@Data
public class WxMaProperties {
    private List<Config> configs;

    @Data
    public static class Config {
        /**
         * 设置微信小程序的appid
         */
        private String appid;

        /**
         * 设置微信小程序的Secret
         */
        private String secret;

        /**
         * 设置微信小程序消息服务器配置的token
         */
        private String token;

        /**
         * 设置微信小程序消息服务器配置的EncodingAESKey
         */
        private String aesKey;

        /**
         * 消息格式，XML或者JSON
         */
        private String msgDataFormat;






        /**
         * 微信支付商户号
         */
        private String mchId;


        /**
         * 微信支付商户密钥
         */
        private String mchKey;


        /**
         * 支付成功后回调通知地址
         */
        public String SUCCESS_NOTIFY ;


    }

    public List<Config> getConfigs() {
        return configs;
    }
}
