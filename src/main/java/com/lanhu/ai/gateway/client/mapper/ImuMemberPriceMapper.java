package com.lanhu.ai.gateway.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberPrice;
import java.util.List;

import com.lanhu.ai.gateway.client.model.ImuMemberZhifu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ImuMemberPriceMapper extends BaseMapper<ImuMemberPrice> {
    int updateBatch(List<ImuMemberPrice> list);

    int updateBatchSelective(List<ImuMemberPrice> list);

    int batchInsert(@Param("list") List<ImuMemberPrice> list);

    int insertOrUpdate(ImuMemberPrice record);

    int insertOrUpdateSelective(ImuMemberPrice record);

    /**
     * 获取 价格列表
     * @return
     */
    List<ImuMemberPrice> selectList();
}