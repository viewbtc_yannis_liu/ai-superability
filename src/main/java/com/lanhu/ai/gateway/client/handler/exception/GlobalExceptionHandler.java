package com.lanhu.ai.gateway.client.handler.exception;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.lanhu.ai.gateway.client.core.BusinessException;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.core.PcsResultCode;
import com.lanhu.ai.gateway.client.utils.MessageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.text.MessageFormat;
import java.util.Objects;


@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public <T> PcsResult<T> handler(BusinessException exception) {
        return this.assemblyPcsResult(PcsResultCode.convert(exception.getErrorCode()), exception.getMessage(), exception);
    }

    @ExceptionHandler(value = BindException.class)
    @ResponseBody
    public <T> PcsResult<T> handler(BindException exception) {
        return this.assemblyPcsResult(PcsResultCode.PARAM_LOSE, Objects.requireNonNull(exception.getBindingResult().getFieldError().getDefaultMessage(), PcsResultCode.PARAM_LOSE.getDesc()), exception,exception.getBindingResult().getFieldError().getField());
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public <T> PcsResult<T> handler(MethodArgumentNotValidException exception) {
       // ConstraintViolation<?> constraintViolation = exception.getConstraintViolations().iterator().next();
        return this.assemblyPcsResult(PcsResultCode.PARAM_LOSE, ObjectUtil.defaultIfNull(exception.getBindingResult().getFieldError().getDefaultMessage(), PcsResultCode.PARAM_LOSE.getDesc()), exception,exception.getBindingResult().getFieldError().getField());
    }

    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    public <T> PcsResult<T> handler(HttpRequestMethodNotSupportedException exception) {
        return this.assemblyPcsResult(PcsResultCode.METHOD_NOT_SUPPORT, StrUtil.EMPTY, exception);
    }

    @ExceptionHandler(value = NoHandlerFoundException.class)
    @ResponseBody
    public <T> PcsResult<T> handler(NoHandlerFoundException exception) {
        return this.assemblyPcsResult(PcsResultCode.NOT_FOUND_HANDLER, StrUtil.EMPTY, exception);
    }

    @ExceptionHandler(value = MissingServletRequestParameterException.class)
    @ResponseBody
    public <T> PcsResult<T> handler(MissingServletRequestParameterException exception) {
        return this.assemblyPcsResult(PcsResultCode.PARAM_LOSE, exception.getParameterName() + PcsResultCode.PARAM_LOSE.getDesc(), exception);
    }

    @ExceptionHandler(value = MissingServletRequestPartException.class)
    @ResponseBody
    public <T> PcsResult<T> handler(MissingServletRequestPartException exception) {
        return this.assemblyPcsResult(PcsResultCode.PARAM_LOSE, exception.getRequestPartName() + PcsResultCode.PARAM_LOSE.getDesc(), exception);
    }

    @ExceptionHandler(value = MultipartException.class)
    @ResponseBody
    public <T> PcsResult<T> handler(MultipartException exception) {
        return this.assemblyPcsResult(PcsResultCode.FILE_UPLOAD_ERROR, StrUtil.EMPTY, exception);
    }

    @ExceptionHandler(value = HttpMediaTypeNotSupportedException.class)
    @ResponseBody
    public <T> PcsResult<T> handler(HttpServletRequest req, HttpMediaTypeNotSupportedException exception) {
        return this.assemblyPcsResult(PcsResultCode.MEDIA_TYPE_NOT_SUPPORT, MessageFormat.format("请求类型不支持{0}", req.getContentType()), exception);
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    @ResponseBody
    public <T> PcsResult<T> handler(HttpMessageNotReadableException exception) {
        return this.assemblyPcsResult(PcsResultCode.PARAM_LOSE, StrUtil.EMPTY, exception);
    }

    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    @ResponseBody
    public <T> PcsResult<T> handler(MethodArgumentTypeMismatchException exception) {
        return this.assemblyPcsResult(PcsResultCode.PARAM_LOSE, exception.getName() + PcsResultCode.PARAM_TYPE_NOT_SUPPORT.getDesc(), exception);
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public <T> PcsResult<T> handler(Exception exception) {
        return this.assemblyPcsResult(PcsResultCode.ERROR_OPERATE, StrUtil.EMPTY, exception);
    }

    // ================================================================================== 私有方法区 ================================================================================== //

    /**
     * 组装返回结果实体
     *
     * @param pcsResultCode 结果码
     * @param message       信息
     * @param exception     异常
     * @param <T>           数据类型
     * @return 结果实体
     */
    private <T> PcsResult<T> assemblyPcsResult(PcsResultCode pcsResultCode, String message, Exception exception, Object... args) {
        log.error("============>>>>>> 全局异常 <<<<<<============", exception);
        PcsResult<T> pcsResult = new PcsResult<>();
        if (ObjectUtil.isNotNull(pcsResultCode)) {
            pcsResult.setErrorCode(pcsResultCode);
        }
        if (StrUtil.isNotBlank(message)) {
            pcsResult.setMessage(MessageUtil.message(message,args));
        }
        return pcsResult;
    }
}
