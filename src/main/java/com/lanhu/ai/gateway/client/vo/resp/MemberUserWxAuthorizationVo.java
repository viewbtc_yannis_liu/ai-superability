package com.lanhu.ai.gateway.client.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("用户获取微信授权参数出参")
@Data
public class MemberUserWxAuthorizationVo {
    /**
     * 用户的openid
     */
    @ApiModelProperty(value = "用户的openId")
    private String openid;

    /**
     * 会话的sessionKey
     */
    @ApiModelProperty(value = "会话的sessionKey")
    private String sessionKey;
}
