package com.lanhu.ai.gateway.client.core;
import com.lanhu.ai.gateway.client.utils.MessageUtil;

/********************************
 * @title Result
 * @package com.lanhu.imenu.gateway.client.core
 * @description description
 *
 * @date 2022/11/17 2:19 下午
 * @version 0.0.1
 *********************************/
public class Result {
    public static  PcsResult ok() {
        return new PcsResult(PcsResultCode.SUCCESS);
    }

    public static <T> PcsResult<T> ok(T data) {
        PcsResult<T> pcsResult = new PcsResult<>(PcsResultCode.SUCCESS);
        pcsResult.setData(data);
        return pcsResult;
    }

    public static <T> PcsResult<T> ok(PcsResultCode code, T data) {
        PcsResult<T> pcsResult = new PcsResult<>(code);
        pcsResult.setData(data);
        return pcsResult;
    }


    public static  PcsResult error() {
        return new PcsResult(PcsResultCode.ERROR_OPERATE, MessageUtil.message(PcsResultCode.ERROR_OPERATE.getDesc()));
    }

    public static  PcsResult error(PcsResultCode code, Object... args) {
        return new PcsResult(code.getCode(),MessageUtil.message(code.getDesc(),args));
    }

    public static  PcsResult error(int code,String message) {
        return new PcsResult(code,message);
    }
}
