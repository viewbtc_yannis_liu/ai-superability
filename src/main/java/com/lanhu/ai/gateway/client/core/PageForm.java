package com.lanhu.ai.gateway.client.core;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@ApiModel(value = "分页入数")
@Data
public class PageForm {
    /**
     * 当前页码数
     */
    @ApiModelProperty(value = "当前页码数", required = true)
    @NotNull(message = "当前页码不能为空")
    @Min(value = 1, message = "当前页码不能小于1")
    protected Integer pageIndex;

    /**
     * 页面显示条数
     */
    @ApiModelProperty(value = "页面显示条数", required = true)
    @NotNull(message = "每页条数不能为空")
    @Min(value = 1, message = "每页条数不能小于1")
    protected Integer pageSize;
}
