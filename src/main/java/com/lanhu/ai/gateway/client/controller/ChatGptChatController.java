package com.lanhu.ai.gateway.client.controller;

import com.lanhu.ai.gateway.client.annotation.IgnoreSecurity;
import com.lanhu.ai.gateway.client.core.BaseController;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.service.ChatGptService;
import com.lanhu.ai.gateway.client.utils.ServletUtils;
import com.lanhu.ai.gateway.client.vo.req.*;
import com.theokanning.openai.completion.chat.ChatCompletionChoice;
import com.theokanning.openai.image.Image;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/1/11 5:21 下午
 */

@Slf4j
@Api(value = "chatgpt  服务", tags = "chatgpt 服务")
@RestController
public class ChatGptChatController extends BaseController {

    @Autowired
    private ChatGptService chatGptChatCompletionService;



    @ApiOperation(value = "Chatgpt 聊天交互", notes = "Chatgpt 聊天交互")
    @PostMapping("/ai/v1/chat")
   // @IgnoreSecurity
    public PcsResult<List<ChatCompletionChoice>> chat(@Validated @RequestBody ChatGptChatForm request) {

       ImuMemberUser imuMemberUser = this.currentUser(ServletUtils.getRequest());

//        ImuMemberUser imuMemberUser = new ImuMemberUser();
//        imuMemberUser.setId(1L);

        return chatGptChatCompletionService.chat(imuMemberUser,request);
    }



    @ApiOperation(value = "Chatgpt 所有问题咨询都调用此接口", notes = "Chatgpt 所有问题咨询都调用此接口")
    @PostMapping("/ai/v1/qa")
    //@IgnoreSecurity
    public PcsResult<List<ChatCompletionChoice>> qa(@Validated @RequestBody ChatGptQaForm request) {

        ImuMemberUser imuMemberUser = this.currentUser(ServletUtils.getRequest());

//        ImuMemberUser imuMemberUser = new ImuMemberUser();
//        imuMemberUser.setId(1L);

        return chatGptChatCompletionService.qa(imuMemberUser,request);
    }



    @ApiOperation(value = "Chatgpt 根据文本图片生成", notes = "Chatgpt 根据文本图片生成")
    @PostMapping("/ai/v1/image/create")
   // @IgnoreSecurity
    public PcsResult<String> imageCreate(@Validated @RequestBody ChatGptCreateImageForm request) {

        ImuMemberUser imuMemberUser = this.currentUser(ServletUtils.getRequest());

//        ImuMemberUser imuMemberUser = new ImuMemberUser();
//        imuMemberUser.setId(1L);

        return chatGptChatCompletionService.createImage(imuMemberUser,request);
    }



    @ApiOperation(value = "Chatgpt 根据图片生成图片变体", notes = "Chatgpt 根据图片生成图片变体")
    @PostMapping("/ai/v1/imageVariation/create")
   // @IgnoreSecurity
    public PcsResult<String> imageVariationCreate(@Validated @RequestBody ChatGptCreateImageVariationForm request) {

        ImuMemberUser imuMemberUser = this.currentUser(ServletUtils.getRequest());

//        ImuMemberUser imuMemberUser = new ImuMemberUser();
//        imuMemberUser.setId(1L);

        return chatGptChatCompletionService.createImageVariation(imuMemberUser,request);
    }


    @ApiOperation(value = "Chatgpt 图片编辑", notes = "Chatgpt 图片编辑")
    @PostMapping("/ai/v1/image/edit")
    //@IgnoreSecurity
    public PcsResult<String> imageEdit(@Validated @RequestBody ChatGptCreateImageEditForm request) {

        ImuMemberUser imuMemberUser = this.currentUser(ServletUtils.getRequest());

//        ImuMemberUser imuMemberUser = new ImuMemberUser();
//        imuMemberUser.setId(1L);

        return chatGptChatCompletionService.createImageEdit(imuMemberUser,request);
    }


}
