package com.lanhu.ai.gateway.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanhu.ai.gateway.client.model.ImuMemberCommunity;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ImuMemberCommunityMapper extends BaseMapper<ImuMemberCommunity> {
    int updateBatch(List<ImuMemberCommunity> list);

    int updateBatchSelective(List<ImuMemberCommunity> list);

    int batchInsert(@Param("list") List<ImuMemberCommunity> list);

    int insertOrUpdate(ImuMemberCommunity record);

    int insertOrUpdateSelective(ImuMemberCommunity record);

    /**
     * 社区列表分页
     *
     * @return
     */
    IPage<ImuMemberCommunity> selectListPage(Page<ImuMemberCommunity> page);

    /**
     * 我的作品列表分页
     *
     * @return
     */
    IPage<ImuMemberCommunity> selectMyListPage(Page<ImuMemberCommunity> page, @Param("userId") Long userId);

    /**
     * 我的作品详情
     *
     * @return
     */
    ImuMemberCommunity selectMyCommunityDetail(@Param("userId") Long userId, @Param("communityId") Long communityId);
}