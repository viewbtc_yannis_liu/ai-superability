package com.lanhu.ai.gateway.client.bo;

import lombok.Data;


@Data
public class WxMaUserInfoResultBO {
    /**
     * 用户openid
     */
    private String openid;

    /**
     * 用户unionid
     */
    private String unionid;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 性别  0:未知, 1:男性, 2:女性
     */
    private Integer gender;

    /**
     * 语言 en:英文, zh_CN:简体中文, zh_TW:繁体中文
     */
    private String language;

    /**
     * 所在城市
     */
    private String city;

    /**
     * 所在省份
     */
    private String province;

    /**
     * 所在国家
     */
    private String country;

    /**
     * 头像地址
     */
    private String avatarUrl;

    /**
     * 水印
     */
    private Watermark watermark;

    @Data
    public static class Watermark {
        /**
         * 时间戳
         */
        private String timestamp;

        /**
         * 小程序appId
         */
        private String appid;
    }
}
