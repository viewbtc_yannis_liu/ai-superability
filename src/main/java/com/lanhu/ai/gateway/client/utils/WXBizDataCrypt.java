package com.lanhu.ai.gateway.client.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.AlgorithmParameters;
import java.security.Security;
import java.util.Base64;

/**
 * @Description 小程序数据 解密工具类
 * @Author huhouchun
 * @Date 2019/5/15 8:45
 * @Version 1.0
 */
public class WXBizDataCrypt {
    private String appId;
    private String sessionKey;

    public WXBizDataCrypt(String appId, String sessionKey)
    {
        this.appId = appId;
        this.sessionKey = sessionKey;
    }

    public WXBizDataCrypt(){ }

    public String decrypt(String encryptedData, String iv) throws Exception{
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] keyByte = decoder.decode(sessionKey);
        byte[] encryptedDataByte = decoder.decode(encryptedData);
        byte[] ivByte = decoder.decode(iv);
        if (Security.getProvider("BC") == null) {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        }
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding","BC");
        SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
        AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
        parameters.init(new IvParameterSpec(ivByte));
        //设置为解密模式
        cipher.init(Cipher.DECRYPT_MODE, spec,parameters);
        byte[] resultByte = cipher.doFinal(encryptedDataByte);
        String result = null;
        if (null != resultByte && resultByte.length > 0) {
            result = new String(resultByte, "UTF-8");
        }
        // 返回前，可以对比appId
        return result;
    }

    public String parseData(String appId , String sessionKey , String encryptedData , String iv) {
        WXBizDataCrypt wx = new WXBizDataCrypt(appId, sessionKey);
        try {
            String decrypt = wx.decrypt(encryptedData, iv);
            System.out.println("answer=====" + decrypt);
            return decrypt;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 测试用例
     * @param args
     */
    public static void main(String[] args) {
        //网上的信息
        String appId = "wx4f4bc4dec97d474b";
        /*String sessionKey = "tiihtNczf5v6AKRyjwEUhQ==";
        String encryptedData = "CiyLU1Aw2KjvrjMdj8YKliAjtP4gsMZMQmRzooG2xrDcvSnxIMXFufNstNGTyaGS9uT5geRa0W4oTOb1WT7fJlAC+oNPdbB+3hVbJSRgv+4lGOETKUQz6OYStslQ142dNCuabNPGBzlooOmB231qMM85d2/fV6ChevvXvQP8Hkue1poOFtnEtpyxVLW1zAo6/1Xx1COxFvrc2d7UL/lmHInNlxuacJXwu0fjpXfz/YqYzBIBzD6WUfTIF9GRHpOn/Hz7saL8xz+W//FRAUid1OksQaQx4CMs8LOddcQhULW4ucetDf96JcR3g0gfRK4PC7E/r7Z6xNrXd2UIeorGj5Ef7b1pJAYB6Y5anaHqZ9J6nKEBvB4DnNLIVWSgARns/8wR2SiRS7MNACwTyrGvt9ts8p12PKFdlqYTopNHR1Vf7XjfhQlVsAJdNiKdYmYVoKlaRv85IfVunYzO0IKXsyl7JCUjCpoG20f0a04COwfneQAGGwd5oa+T8yO5hzuyDb/XcxxmK01EpqOyuxINew==";
        String iv = "r7BXXKkLb8qrSNn05n0qiA==";*/

        //小程序同事给
        String sessionKey ="pNKYdfEykIcBOO4dREKSWQ==";
        String encryptedData ="W4vuq5/9HSz0iMcLFMbr5CT9RZ1oy1cMau6T1V3W5ZBYVFmNw7Be0I81Tf30Uw1BoKuyttUy+w5ep8ey3sOUVOWC4L7GmOlwY/P+Y76fA/sZiq5lSZaHu/Q8uhifb4A9vFOu+LBoMxSU6fA4JDVCEjkH7qUh0evyS34bM8LrP9IxWXqKZPpzv2HwwFiolh19Nak2f0TkmFmavXZA8aHjeA==";
        String iv = "ppyvo5nDZ3NPmh7tbZCmrg==";

        WXBizDataCrypt wxBizDataCrypt = new WXBizDataCrypt();
        wxBizDataCrypt.parseData(appId , sessionKey , encryptedData , iv);
    }

}



