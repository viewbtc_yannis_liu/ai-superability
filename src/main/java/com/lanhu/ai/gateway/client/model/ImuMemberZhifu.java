package com.lanhu.ai.gateway.client.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 会员订单支付表
 */
@ApiModel(value = "会员订单支付表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "imu_member_zhifu")
public class ImuMemberZhifu {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "ID")
    private Long id;

    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * openid
     */
    @TableField(value = "open_id")
    @ApiModelProperty(value = "openid")
    private String openId;

    /**
     * 订单ID
     */
    @TableField(value = "order_id")
    @ApiModelProperty(value = "订单ID")
    private Long orderId;

    /**
     * 订单号
     */
    @TableField(value = "out_trade_no")
    @ApiModelProperty(value = "订单号")
    private String outTradeNo;

    /**
     * 支付回调id
     */
    @TableField(value = "transaction_id")
    @ApiModelProperty(value = "支付回调id")
    private String transactionId;

    /**
     * 价格 (分)
     */
    @TableField(value = "price")
    @ApiModelProperty(value = "价格 (分)")
    private BigDecimal price;

    /**
     * 积分
     */
    @TableField(value = "point")
    @ApiModelProperty(value = "积分")
    private BigDecimal point;

    /**
     * 状态，0未支付，1已支付
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "状态，0未支付，1已支付")
    private Integer status;

    /**
     * 支付时间
     */
    @TableField(value = "pay_time")
    @ApiModelProperty(value = "支付时间")
    private Long payTime;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "更新时间")
    private Long updateTime;

    /**
     * 逻辑删除的标记  0正常 , 1删除
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除的标记  0正常 , 1删除")
    private Integer isDelete;
}