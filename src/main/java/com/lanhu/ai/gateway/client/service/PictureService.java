package com.lanhu.ai.gateway.client.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.IoUtil;
import com.lanhu.ai.gateway.client.config.FileUploadProperties;
import com.lanhu.ai.gateway.client.core.ProjectConstant;
import com.lanhu.ai.gateway.client.enums.ScaleEnum;
import com.lanhu.ai.gateway.client.helper.AliYunOssHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/6/28 8:42 下午
 */
@Slf4j
@Service
public class PictureService {

    @Autowired
    private FileUploadProperties fileUploadProperties;

    @Autowired
    private AliYunOssHelper aliYunOssHelper;



    /**
    * @description:
    * @param: [url, scale]
    * @return: java.lang.String
    * @author: liuyi
    * @date: 9:20 下午 2023/6/28
    */
    public String scale(String url,String scale){


        log.info("pic scale start:{},scale:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT),scale);
        BufferedImage newImage = null;

        BufferedImage originalImage = null;

        ByteArrayOutputStream outputStream =  null;

        byte[] imageBytes = null;

        InputStream inputStream = null;
        try {

            if(StringUtils.isBlank(url)){
                return ProjectConstant.EMPTY;
            }

            // 读取原始图片
            originalImage = ImageIO.read(new URL(url));



            //如果是等比例
            if(ScaleEnum.one_one.getCode().equalsIgnoreCase(scale)){
                newImage = originalImage;
            }else {


                if(ScaleEnum.nine_sixteen.getCode().equalsIgnoreCase(scale)){
                    // 计算9:16比例的高度
                    int height = originalImage.getWidth() * 16 / 9;

                    // 创建新的9:16图片
                     newImage = new BufferedImage(originalImage.getWidth(), height, BufferedImage.TYPE_INT_RGB);

                    // 绘制原始图片到新的9:16图片
                    Graphics2D graphics = newImage.createGraphics();
                    graphics.drawImage(originalImage, 0, 0, originalImage.getWidth(), height, null);
                    graphics.dispose();

                }else if(ScaleEnum.sixteen_nine.getCode().equalsIgnoreCase(scale)){
                    // 计算16:9比例的宽度
                    int width = originalImage.getHeight() * 16 / 9;

                    // 创建新的16:9图片
                     newImage = new BufferedImage(width, originalImage.getHeight(), BufferedImage.TYPE_INT_RGB);

                    // 绘制原始图片到新的16:9图片
                    Graphics2D graphics = newImage.createGraphics();
                    graphics.drawImage(originalImage, 0, 0, width, originalImage.getHeight(), null);
                    graphics.dispose();

                }else if(ScaleEnum.four_three.getCode().equalsIgnoreCase(scale)){
                    // 计算4:3比例的宽度
                    int width = originalImage.getHeight() * 4 / 3;

                    // 创建新的4:3图片
                     newImage = new BufferedImage(width, originalImage.getHeight(), BufferedImage.TYPE_INT_RGB);

                    // 绘制原始图片到新的4:3图片
                    Graphics2D graphics = newImage.createGraphics();
                    graphics.drawImage(originalImage, 0, 0, width, originalImage.getHeight(), null);
                    graphics.dispose();

                }else if(ScaleEnum.three_two.getCode().equalsIgnoreCase(scale)){
                    // 计算3:2比例的宽度
                    int width = originalImage.getHeight() * 3 / 2;

                    // 创建新的3:2图片
                     newImage = new BufferedImage(width, originalImage.getHeight(), BufferedImage.TYPE_INT_RGB);

                    // 绘制原始图片到新的3:2图片
                    Graphics2D graphics = newImage.createGraphics();
                    graphics.drawImage(originalImage, 0, 0, width, originalImage.getHeight(), null);
                    graphics.dispose();


                }



            }


            outputStream = new ByteArrayOutputStream();

            ImageIO.write(newImage, ProjectConstant.CHATGPT_IMAGE_TYPE, outputStream);

            imageBytes = outputStream.toByteArray();

            // 使用 Hutool 将字节数组转换为 InputStream
            inputStream = new ByteArrayResource(imageBytes).getInputStream();

            String ossUrl =  aliYunOssHelper.uploadImage2Oss(ProjectConstant.CHATGPT_IMAGE_TYPE,inputStream);



            log.info("pic scale end:{},scale:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT),scale);



            return ossUrl;

        }catch (Exception e){

            log.error("pic scale error",e);

            return  ProjectConstant.EMPTY;

        }finally {


            newImage = null;

            originalImage = null;

            imageBytes = null;

            if(outputStream != null){
                IoUtil.close(outputStream);
            }



            if(inputStream != null){
                IoUtil.close(inputStream);
            }




        }
    }



    /**
     * @description:
     * @param: [url, scale]
     * @return: java.lang.String
     * @author: liuyi
     * @date: 9:20 下午 2023/6/28
     */
    public String scale2(String url,String s){


        log.info("pic scale2 start:{},scale:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT),s);
        BufferedImage newImage = null;

        BufferedImage originalImage = null;

        ByteArrayOutputStream outputStream =  null;

        byte[] imageBytes = null;

        InputStream inputStream = null;
        try {

            if(StringUtils.isBlank(url)){
                return ProjectConstant.EMPTY;
            }

            // 读取原始图片
            originalImage = ImageIO.read(new URL(url));



            //如果是等比例
            if(ScaleEnum.one_one.getCode().equalsIgnoreCase(s)){
                newImage = originalImage;
            }else {


                if(ScaleEnum.nine_sixteen.getCode().equalsIgnoreCase(s)){

                    // 定义目标的宽度和高度
                    int destWidth = (int) (originalImage.getHeight() * (9.0 / 16.0));
                    int destHeight = originalImage.getHeight();

                    // 创建目标图片
                     newImage = new BufferedImage(destWidth, destHeight, BufferedImage.TYPE_INT_RGB);

                    // 创建Graphics2D对象并设置抗锯齿
                    Graphics2D g2d = newImage.createGraphics();
                    g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

                    // 计算缩放比例
                    double scaleX = (double) destWidth / originalImage.getWidth();
                    double scaleY = (double) destHeight / originalImage.getHeight();

                    // 计算缩放后的尺寸，保证图片不变形
                    double scale = Math.min(scaleX, scaleY);
                    int scaledWidth = (int) (originalImage.getWidth() * scale);
                    int scaledHeight = (int) (originalImage.getHeight() * scale);

                    // 计算平移量（使图像居中）
                    int offsetX = (destWidth - scaledWidth) / 2;
                    int offsetY = (destHeight - scaledHeight) / 2;

                    // 执行缩放和平移操作
                    g2d.scale(scale, scale);
                    g2d.translate(offsetX, offsetY);

                    // 绘制缩放后的图像
                    g2d.drawImage(originalImage, 0, 0, null);
                    g2d.dispose();





                }else if(ScaleEnum.sixteen_nine.getCode().equalsIgnoreCase(s)){

                    // 定义目标的宽度和高度
                    int destWidth = originalImage.getWidth();
                    int destHeight = (int) (originalImage.getWidth() * (9.0 / 16.0));

                    // 创建目标图片
                    newImage = new BufferedImage(destWidth, destHeight, BufferedImage.TYPE_INT_RGB);

                    // 创建Graphics2D对象并设置抗锯齿
                    Graphics2D g2d = newImage.createGraphics();
                    g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

                    // 计算缩放比例
                    double scaleX = (double) destWidth / originalImage.getWidth();
                    double scaleY = (double) destHeight / originalImage.getHeight();

                    // 计算缩放后的尺寸，保证图片不变形
                    double scale = Math.min(scaleX, scaleY);
                    int scaledWidth = (int) (originalImage.getWidth() * scale);
                    int scaledHeight = (int) (originalImage.getHeight() * scale);

                    // 计算平移量（使图像居中）
                    int offsetX = (destWidth - scaledWidth) / 2;
                    int offsetY = (destHeight - scaledHeight) / 2;

                    // 执行缩放和平移操作
                    g2d.scale(scale, scale);
                    g2d.translate(offsetX, offsetY);

                    // 绘制缩放后的图像
                    g2d.drawImage(originalImage, 0, 0, null);
                    g2d.dispose();


                }else if(ScaleEnum.four_three.getCode().equalsIgnoreCase(s)){

                    // 定义目标的宽度和高度
                    int destWidth = originalImage.getWidth();
                    int destHeight = (int) (originalImage.getWidth() * (3.0 / 4.0));

                    // 创建目标图片
                    newImage = new BufferedImage(destWidth, destHeight, BufferedImage.TYPE_INT_RGB);

                    // 创建Graphics2D对象并设置抗锯齿
                    Graphics2D g2d = newImage.createGraphics();
                    g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

                    // 计算缩放比例
                    double scaleX = (double) destWidth / originalImage.getWidth();
                    double scaleY = (double) destHeight / originalImage.getHeight();

                    // 计算缩放后的尺寸，保证图片不变形
                    double scale = Math.min(scaleX, scaleY);
                    int scaledWidth = (int) (originalImage.getWidth() * scale);
                    int scaledHeight = (int) (originalImage.getHeight() * scale);

                    // 计算平移量（使图像居中）
                    int offsetX = (destWidth - scaledWidth) / 2;
                    int offsetY = (destHeight - scaledHeight) / 2;

                    // 执行缩放和平移操作
                    g2d.scale(scale, scale);
                    g2d.translate(offsetX, offsetY);

                    // 绘制缩放后的图像
                    g2d.drawImage(originalImage, 0, 0, null);
                    g2d.dispose();



                }else if(ScaleEnum.three_two.getCode().equalsIgnoreCase(s)){
                    // 定义目标的宽度和高度
                    int destWidth = originalImage.getWidth();
                    int destHeight = (int) (originalImage.getWidth() * (2.0 / 3.0));

                    // 创建目标图片
                    newImage = new BufferedImage(destWidth, destHeight, BufferedImage.TYPE_INT_RGB);

                    // 创建Graphics2D对象并设置抗锯齿
                    Graphics2D g2d = newImage.createGraphics();
                    g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

                    // 计算缩放比例
                    double scaleX = (double) destWidth / originalImage.getWidth();
                    double scaleY = (double) destHeight / originalImage.getHeight();

                    // 计算缩放后的尺寸，保证图片不变形
                    double scale = Math.min(scaleX, scaleY);
                    int scaledWidth = (int) (originalImage.getWidth() * scale);
                    int scaledHeight = (int) (originalImage.getHeight() * scale);

                    // 计算平移量（使图像居中）
                    int offsetX = (destWidth - scaledWidth) / 2;
                    int offsetY = (destHeight - scaledHeight) / 2;

                    // 执行缩放和平移操作
                    g2d.scale(scale, scale);
                    g2d.translate(offsetX, offsetY);

                    // 绘制缩放后的图像
                    g2d.drawImage(originalImage, 0, 0, null);
                    g2d.dispose();


                }



            }


            outputStream = new ByteArrayOutputStream();

            ImageIO.write(newImage, ProjectConstant.CHATGPT_IMAGE_TYPE, outputStream);

            imageBytes = outputStream.toByteArray();

            // 使用 Hutool 将字节数组转换为 InputStream
            inputStream = new ByteArrayResource(imageBytes).getInputStream();

            String ossUrl =  aliYunOssHelper.uploadImage2Oss(ProjectConstant.CHATGPT_IMAGE_TYPE,inputStream);



            log.info("pic scale2 end:{},scale:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT),s);



            return ossUrl;

        }catch (Exception e){

            log.error("pic scale error",e);

            return  ProjectConstant.EMPTY;

        }finally {


            newImage = null;

            originalImage = null;

            imageBytes = null;

            if(outputStream != null){
                IoUtil.close(outputStream);
            }



            if(inputStream != null){
                IoUtil.close(inputStream);
            }




        }
    }



    /**
     * @description: hutool工具等比缩放
     * @param: [url, scale]
     * @return: java.lang.String
     * @author: liuyi
     * @date: 9:20 下午 2023/6/28
     */
    public String scale3(String url,String s){


        log.info("pic scale3 start:{},scale:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT),s);


        BufferedImage originalImage = null;

        Image image = null;

        InputStream inputStream = null;


        String ossUrl = ProjectConstant.EMPTY;

        try {

            if(StringUtils.isBlank(url)){
                return ProjectConstant.EMPTY;
            }

            // 读取原始图片
            originalImage = ImageIO.read(new URL(url));




            //如果是等比例
            if(ScaleEnum.one_one.getCode().equalsIgnoreCase(s)){
                image = originalImage;
            }else {


                if(ScaleEnum.nine_sixteen.getCode().equalsIgnoreCase(s)){
                    float scale = 9.0f / 16.0f;

                    image = ImgUtil.scale(originalImage,scale);


                }else if(ScaleEnum.sixteen_nine.getCode().equalsIgnoreCase(s)){
                    float scale = 16.0f / 9.0f;

                    image = ImgUtil.scale(originalImage,scale);

                }else if(ScaleEnum.four_three.getCode().equalsIgnoreCase(s)){

                    float scale = 4.0f / 3.0f;

                    image = ImgUtil.scale(originalImage,scale);


                }else if(ScaleEnum.three_two.getCode().equalsIgnoreCase(s)){
                    float scale = 3.0f / 2.0f;

                    image = ImgUtil.scale(originalImage,scale);


                }



            }


          if(image != null){

              // 使用 Hutool 将字节数组转换为 InputStream
              inputStream = imageToInputStream(image,ProjectConstant.CHATGPT_IMAGE_TYPE);

              ossUrl =  aliYunOssHelper.uploadImage2Oss(ProjectConstant.CHATGPT_IMAGE_TYPE,inputStream);
          }


            log.info("pic scale3 end:{},scale:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT),s);



            return ossUrl;

        }catch (Exception e){

            log.error("pic scale error",e);

            return  ProjectConstant.EMPTY;

        }finally {

            originalImage = null;


            image = null;

            if(inputStream != null){
                IoUtil.close(inputStream);
            }




        }
    }




    /**
     * @description: oss工具等比缩放
     * @param: [url, scale]
     * @return: java.lang.String
     * @author: liuyi
     * @date: 9:20 下午 2023/6/28
     */
    public String scale4(String url,String s){


        log.info("pic scale4 start:{},scale:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT),s);


        BufferedImage originalImage = null;



        String ossUrl = ProjectConstant.EMPTY;

        try {

            if(StringUtils.isBlank(url)){
                return ProjectConstant.EMPTY;
            }

            // 读取原始图片
            originalImage = ImageIO.read(new URL(url));


            //原始图片上传
            if(originalImage != null){
                ossUrl =  aliYunOssHelper.uploadImage2Oss(ProjectConstant.CHATGPT_IMAGE_TYPE,bufferedImageToInputStream(originalImage,ProjectConstant.CHATGPT_IMAGE_TYPE));
            }


            if(StringUtils.isNotBlank(ossUrl)){
                //如果是等比例
                if(ScaleEnum.one_one.getCode().equalsIgnoreCase(s)){
                    ossUrl = ossUrl;
                }else {


                    if(ScaleEnum.nine_sixteen.getCode().equalsIgnoreCase(s)){


                        ossUrl = ossUrl.concat(ScaleEnum.nine_sixteen.getMsg());

                    }else if(ScaleEnum.sixteen_nine.getCode().equalsIgnoreCase(s)){
                        ossUrl = ossUrl.concat(ScaleEnum.sixteen_nine.getMsg());

                    }else if(ScaleEnum.four_three.getCode().equalsIgnoreCase(s)){

                        ossUrl = ossUrl.concat(ScaleEnum.four_three.getMsg());

                    }else if(ScaleEnum.three_two.getCode().equalsIgnoreCase(s)){
                        ossUrl = ossUrl.concat(ScaleEnum.three_two.getMsg());

                    }



                }


            }







            log.info("pic scale4 end:{},scale:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT),s);



            return ossUrl;

        }catch (Exception e){

            log.error("pic scale error",e);

            return  ProjectConstant.EMPTY;

        }finally {

            originalImage = null;


        }
    }



    private  InputStream bufferedImageToInputStream(BufferedImage bufferedImage,String imageType) {
        try {
            // 将Image对象写入到ByteArrayOutputStream中
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ImageIO.write(bufferedImage, imageType, outputStream);

            // 将ByteArrayOutputStream转换为InputStream
            byte[] bytes = outputStream.toByteArray();
            return new ByteArrayInputStream(bytes);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    private  InputStream imageToInputStream(Image image,String imageType) {
        try {
            // 将Image对象写入到ByteArrayOutputStream中
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ImageIO.write(toBufferedImage(image), imageType, outputStream);

            // 将ByteArrayOutputStream转换为InputStream
            byte[] bytes = outputStream.toByteArray();
            return new ByteArrayInputStream(bytes);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    // 将Image对象转换为BufferedImage对象
    private  BufferedImage toBufferedImage(Image image) {
        if (image instanceof BufferedImage) {
            return (BufferedImage) image;
        }

        BufferedImage bufferedImage = new BufferedImage(
                image.getWidth(null),
                image.getHeight(null),
                BufferedImage.TYPE_INT_ARGB
        );

        bufferedImage.getGraphics().drawImage(image, 0, 0, null);
        return bufferedImage;
    }



}
