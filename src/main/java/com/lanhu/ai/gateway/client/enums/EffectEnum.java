package com.lanhu.ai.gateway.client.enums;

/**
 * 是否有效
 *
 * @author liuyi
 * @version 1.0
 * @date 2021/11/29 10:48 下午
 */

public enum EffectEnum {
    /**
     * 是
     */
    YES_EFFECT(0, "有效"),

    /**
     * 否
     */
    NO_EFFECT(1, "无效"),
    ;

    private final int code;

    private final String msg;

    EffectEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static EffectEnum convert(int code) {
        for (EffectEnum whetherEnum : EffectEnum.values()) {
            if (whetherEnum.getCode() == code) {
                return whetherEnum;
            }
        }
        return EffectEnum.YES_EFFECT;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
