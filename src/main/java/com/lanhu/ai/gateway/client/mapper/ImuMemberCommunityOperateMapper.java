package com.lanhu.ai.gateway.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberCommunityOperate;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ImuMemberCommunityOperateMapper extends BaseMapper<ImuMemberCommunityOperate> {
    int updateBatch(List<ImuMemberCommunityOperate> list);

    int updateBatchSelective(List<ImuMemberCommunityOperate> list);

    int batchInsert(@Param("list") List<ImuMemberCommunityOperate> list);

    int insertOrUpdate(ImuMemberCommunityOperate record);

    int insertOrUpdateSelective(ImuMemberCommunityOperate record);

    int selectCountByUserIdAndCommunityId(@Param("userId") Long userId, @Param("communityId") Long communityId, @Param("operateType") Integer operateType );

    ImuMemberCommunityOperate selectByUserIdAndCommunityId(@Param("userId") Long userId, @Param("communityId") Long communityId, @Param("operateType") Integer operateType );
}