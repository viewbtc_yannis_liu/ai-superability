package com.lanhu.ai.gateway.client.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanhu.ai.gateway.client.annotation.LhTransaction;
import com.lanhu.ai.gateway.client.core.PageForm;
import com.lanhu.ai.gateway.client.enums.CommunityOperateTypeEnum;
import com.lanhu.ai.gateway.client.enums.EffectEnum;
import com.lanhu.ai.gateway.client.mapper.ImuMemberCommunityMapper;
import com.lanhu.ai.gateway.client.mapper.ImuMemberCommunityOperateMapper;
import com.lanhu.ai.gateway.client.mapper.ImuMemberUserMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberCommunity;
import com.lanhu.ai.gateway.client.model.ImuMemberCommunityOperate;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.vo.req.CommunityDetailForm;
import com.lanhu.ai.gateway.client.vo.resp.CommunityInfoVo;
import com.lanhu.ai.gateway.client.vo.resp.CommunityListPageVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/06/29 9:27
 */

@Slf4j
@Service
public class CommunityService {


    @Resource
    private ImuMemberCommunityMapper imuMemberCommunityMapper;

    @Resource
    private ImuMemberCommunityOperateMapper imuMemberCommunityOperateMapper;

    @Resource
    private ImuMemberUserMapper imuMemberUserMapper;


    /**
     * 社区分页记录
     * @param imuMemberUser
     * @param form
     * @return
     */
    public CommunityListPageVo communityListPage(ImuMemberUser imuMemberUser , PageForm form){
        Page<ImuMemberCommunity> page = new Page<>(form.getPageIndex(), form.getPageSize());
        IPage<ImuMemberCommunity> pageList = imuMemberCommunityMapper.selectListPage(page);
        List<ImuMemberCommunity> records = pageList.getRecords();
        List<CommunityInfoVo> resultList = this.assemblCommunityList(imuMemberUser , records);
        CommunityListPageVo listPageVO = new CommunityListPageVo();
        listPageVO.setList(resultList);
        listPageVO.setTotal(pageList.getTotal());
        listPageVO.setTotalPages(pageList.getPages());
        listPageVO.setCurrentPage(pageList.getCurrent());
        return listPageVO;
    }



    /**
     * 社区详情
     * @param imuMemberUser
     * @param form
     * @return
     */
    public CommunityInfoVo communityDetail(ImuMemberUser imuMemberUser , CommunityDetailForm form){
        Long userId = null == imuMemberUser ? null : imuMemberUser.getId();
        ImuMemberCommunity community = imuMemberCommunityMapper.selectById(form.getId());
        CommunityInfoVo vo = new CommunityInfoVo();
        if (ObjectUtil.isNotNull(community)) {
            BeanUtil.copyProperties(community, vo);
            if (null != userId && userId > 0 ){
                int count = imuMemberCommunityOperateMapper.selectCountByUserIdAndCommunityId(userId , community.getId() , CommunityOperateTypeEnum.LIKE.getCode() );
                vo.setIsLike(count > 0 ? 1 : 0 );
            }

            //昵称
            ImuMemberUser user = imuMemberUserMapper.selectById(community.getUserId());
            if (ObjectUtil.isNotNull(user)) {
                vo.setNick(user.getNick());
            }

        }

        //预览
        this.communityOperate( imuMemberUser , form.getId() , CommunityOperateTypeEnum.PREVIEW.getCode() );

        return vo;
    }


    /**
     * 社区操作记录  预览 、点赞
     * @param imuMemberUser
     * @param communityId
     * @param type
     */
    @LhTransaction
    public void communityOperate(ImuMemberUser imuMemberUser , Long communityId ,  int type ){
        Long userId = null == imuMemberUser ? null : imuMemberUser.getId();
        if (null != userId && userId > 0 ){
            ImuMemberCommunity community = imuMemberCommunityMapper.selectById(communityId);
            if (ObjectUtil.isNotNull(community)) {
                int count = imuMemberCommunityOperateMapper.selectCountByUserIdAndCommunityId(userId , community.getId() , type );
                if (count <= 0 ){
                    ImuMemberCommunityOperate communityOperate = new ImuMemberCommunityOperate();
                    communityOperate.setUserId(userId);
                    communityOperate.setCommunityId(communityId);
                    communityOperate.setOperateType(type);
                    long time = DateUtil.currentSeconds();
                    communityOperate.setCreateTime(time);
                    communityOperate.setUpdateTime(time);
                    communityOperate.setIsDelete(EffectEnum.YES_EFFECT.getCode());
                    imuMemberCommunityOperateMapper.insert(communityOperate);

                    if (type == CommunityOperateTypeEnum.PREVIEW.getCode()){
                        community.setPreviewCount(community.getPreviewCount() + 1);

                    }else if (type == CommunityOperateTypeEnum.LIKE.getCode()){
                        community.setLikeCount(community.getLikeCount() + 1);
                    }
                    imuMemberCommunityMapper.insertOrUpdateSelective(community);
                }
            }
        }
    }


    /**
     * 取消点赞
     * @param imuMemberUser
     * @param communityId
     * @param type
     */
    @LhTransaction
    public void communityUnOperate(ImuMemberUser imuMemberUser , Long communityId ,  int type ){
        ImuMemberCommunity community = imuMemberCommunityMapper.selectById(communityId);
        if (ObjectUtil.isNotNull(community)) {
            ImuMemberCommunityOperate communityOperate = imuMemberCommunityOperateMapper.selectByUserIdAndCommunityId(imuMemberUser.getId() , community.getId() , type );
            if (ObjectUtil.isNotNull(communityOperate)) {

                long time = DateUtil.currentSeconds();
                communityOperate.setUpdateTime(time);
                communityOperate.setIsDelete(EffectEnum.NO_EFFECT.getCode());
                imuMemberCommunityOperateMapper.insertOrUpdate(communityOperate);

                int likeCount = community.getLikeCount() - 1 <= 0 ? 0 : community.getLikeCount() - 1 ;
                community.setLikeCount(likeCount);
                imuMemberCommunityMapper.insertOrUpdateSelective(community);
            }
        }
    }


    /**
     * 组装社区信息
     * @param imuMemberUser
     * @param list
     * @return
     */
    private List<CommunityInfoVo> assemblCommunityList(ImuMemberUser imuMemberUser , List<ImuMemberCommunity> list) {
        List<CommunityInfoVo> resultList = CollUtil.newArrayList();
        Long userId = null == imuMemberUser ? null : imuMemberUser.getId();
        if (CollUtil.isNotEmpty(list)) {
            for (ImuMemberCommunity community : list) {
                CommunityInfoVo vo = new CommunityInfoVo();
                BeanUtil.copyProperties(community, vo);

                //昵称
                ImuMemberUser user = imuMemberUserMapper.selectById(community.getUserId());
                if (ObjectUtil.isNotNull(user)) {
                    vo.setNick(user.getNick());
                }

                if (null != userId && userId > 0 ){
                    int count = imuMemberCommunityOperateMapper.selectCountByUserIdAndCommunityId(userId , community.getId() , CommunityOperateTypeEnum.LIKE.getCode() );
                    vo.setIsLike(count > 0 ? 1 : 0 );
                }
                resultList.add(vo);
            }
        }
        return resultList;
    }



}
