package com.lanhu.ai.gateway.client.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


@ApiModel("微信用户授权解析手机号参数入参")
@Data
public class MemberUserWxNewPhoneForm {

    @ApiModelProperty(value = "code" , required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String code;

}
