package com.lanhu.ai.gateway.client.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 会员订单表
 */
@ApiModel(value = "会员订单表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "imu_member_order")
public class ImuMemberOrder {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "ID")
    private Long id;

    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * openid
     */
    @TableField(value = "open_id")
    @ApiModelProperty(value = "openid")
    private String openId;

    /**
     * 订单号
     */
    @TableField(value = "out_trade_no")
    @ApiModelProperty(value = "订单号")
    private String outTradeNo;

    /**
     * ip
     */
    @TableField(value = "ip")
    @ApiModelProperty(value = "ip")
    private String ip;

    /**
     * appid
     */
    @TableField(value = "app_id")
    @ApiModelProperty(value = "appid")
    private String appId;

    /**
     * 时间戳
     */
    @TableField(value = "time_stamp")
    @ApiModelProperty(value = "时间戳")
    private Long timeStamp;

    /**
     * 随机数
     */
    @TableField(value = "nonce_str")
    @ApiModelProperty(value = "随机数")
    private String nonceStr;

    /**
     * 签名类型
     */
    @TableField(value = "sign_type")
    @ApiModelProperty(value = "签名类型")
    private String signType;

    /**
     * 签名串
     */
    @TableField(value = "pay_sign")
    @ApiModelProperty(value = "签名串")
    private String paySign;

    /**
     * 预支付id
     */
    @TableField(value = "prepay_id")
    @ApiModelProperty(value = "预支付id")
    private String prepayId;

    /**
     * 商品ID
     */
    @TableField(value = "good_id")
    @ApiModelProperty(value = "商品ID")
    private Long goodId;

    /**
     * 价格 (分)
     */
    @TableField(value = "good_price")
    @ApiModelProperty(value = "价格 (分)")
    private BigDecimal goodPrice;

    /**
     * 积分
     */
    @TableField(value = "good_point")
    @ApiModelProperty(value = "积分")
    private BigDecimal goodPoint;

    /**
     * 商品名称
     */
    @TableField(value = "good_name")
    @ApiModelProperty(value = "商品名称")
    private String goodName;

    /**
     * 商品描述
     */
    @TableField(value = "good_desc")
    @ApiModelProperty(value = "商品描述")
    private String goodDesc;

    /**
     * 状态，0未支付，1已支付
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "状态，0未支付，1已支付")
    private Integer status;

    /**
     * 支付时间
     */
    @TableField(value = "pay_time")
    @ApiModelProperty(value = "支付时间")
    private Long payTime;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "更新时间")
    private Long updateTime;

    /**
     * 逻辑删除的标记  0正常 , 1删除
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除的标记  0正常 , 1删除")
    private Integer isDelete;
}