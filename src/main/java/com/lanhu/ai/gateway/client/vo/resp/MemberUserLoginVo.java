package com.lanhu.ai.gateway.client.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


@ApiModel("用户登录信息出参")
@EqualsAndHashCode(callSuper = true)
@Data
public class MemberUserLoginVo extends MemberUserInfoVo {
    /**
     * token
     */
    @ApiModelProperty(value = "登录身份信息")
    private String token;
}
