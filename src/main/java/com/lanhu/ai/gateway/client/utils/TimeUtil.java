package com.lanhu.ai.gateway.client.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Description TODO
 * @Author huhouchun
 * @Date 2021/8/18 14:51
 * @Version 1.0
 */
public class TimeUtil {

    /**
     * 获取当前时间后十分钟的时间
     * @return
     */
    public static String afterTenMinsToNowDate() {
        try{
            Calendar now = Calendar.getInstance();
            now.setTime(new Date());
            now.add(Calendar.MINUTE , 10);
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateStr = sdf2.format(now.getTime());
            System.out.println("TimeUtil afterTenMinsToNowDate dateStr :" + dateStr );
            return dateStr;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public static Date getCurrentYMDDate() {
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            String ymd = sdf.format(new Date());
            return sdf.parse(ymd);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 本月开始日期 第一天
     * @return
     */
    public static Date getThisMonthStartDate(Date date) {
        try{
            Calendar now = Calendar.getInstance();
            now.setTime(date);
            //now.set(Calendar.DAY_OF_MONTH, 01);

            now.add(Calendar.MONTH, 0);
            now.set(Calendar.DAY_OF_MONTH, 1);

            now.set(Calendar.HOUR_OF_DAY , 00);
            now.set(Calendar.MINUTE , 00);
            now.set(Calendar.SECOND , 00);
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String str = sdf2.format(now.getTime());
            System.out.println("TimeUtil getThisMonthStartDate str :" + str );
            Date newDate = sdf2.parse(str);
            return newDate;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 本月结束日期 本月前一天
     * @return
     */
    public static Date getThisMonthEndDate(Date date) {
        try{
            Calendar now = Calendar.getInstance();
            now.setTime(date);
            //now.add(Calendar.DAY_OF_MONTH, -1);
            now.add(Calendar.MONTH, 1);
            now.set(Calendar.DAY_OF_MONTH, 0);

            now.set(Calendar.HOUR_OF_DAY , 24);
            now.set(Calendar.MINUTE , 00);
            now.set(Calendar.SECOND , 00);
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String str = sdf2.format(now.getTime());
            System.out.println("TimeUtil getThisMonthEndDate str :" + str );
            Date newDate = sdf2.parse(str);
            return newDate;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }



    public static String getHourTime(Date date) {
        try{
            Calendar now = Calendar.getInstance();
            now.setTime(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
            String str = sdf2.format(now.getTime());
            System.out.println("TimeUtil getHourTime str :" + str );
            return str;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
