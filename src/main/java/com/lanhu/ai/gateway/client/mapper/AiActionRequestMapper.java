package com.lanhu.ai.gateway.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanhu.ai.gateway.client.model.AiActionRequest;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AiActionRequestMapper extends BaseMapper<AiActionRequest> {
    int updateBatch(List<AiActionRequest> list);

    int updateBatchSelective(List<AiActionRequest> list);

    int batchInsert(@Param("list") List<AiActionRequest> list);

    int insertOrUpdate(AiActionRequest record);

    int insertOrUpdateSelective(AiActionRequest record);

    AiActionRequest isExistHanderingReqeust(@Param("userId") Long userId, @Param("expireTime") Integer expireTime);
}