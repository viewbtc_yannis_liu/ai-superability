package com.lanhu.ai.gateway.client.enums;

/**
 * 会员账号积分变化类型 0:充值 1:消费
 */
public enum AccountChangeTypeEnum {

    RECHARGE(0, "充值"),

    CONSUME(1, "消费"),
    ;

    private final int code;

    private final String msg;

    AccountChangeTypeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static AccountChangeTypeEnum convert(int code) {
        for (AccountChangeTypeEnum typeEnum : AccountChangeTypeEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return AccountChangeTypeEnum.RECHARGE;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }


}
