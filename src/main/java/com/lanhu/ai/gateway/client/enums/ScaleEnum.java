package com.lanhu.ai.gateway.client.enums;

/**
 * @author liuyi
 * @version 1.0
 * @date 2022/7/20 6:58 下午
 */
public enum ScaleEnum {

    /**
     * 其它
     */

    //  /v1/chat/completions
    one_one("1:1", "1:1"),

    sixteen_nine("16:9", "?image/auto-orient,1/resize,m_lfit,w_16,h_9"),

    nine_sixteen("9:16", "?image/auto-orient,1/resize,m_lfit,w_9,h_16"),

    three_two("3:2", "?image/auto-orient,1/resize,m_lfit,w_3,h_2"),

    four_three("4:3", "?image/auto-orient,1/resize,m_lfit,w_4,h_3"),


    ;
    private final String code;

    private final String msg;

    ScaleEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static ScaleEnum convert(String scale) {
        for (ScaleEnum scaleEnum : ScaleEnum.values()) {
            if (scaleEnum.getCode().equalsIgnoreCase(scale)) {
                return scaleEnum;
            }
        }
        return ScaleEnum.one_one;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
