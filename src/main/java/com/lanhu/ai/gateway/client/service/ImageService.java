package com.lanhu.ai.gateway.client.service;
import cn.hutool.core.util.ObjectUtil;
import com.lanhu.ai.gateway.client.config.FileUploadProperties;
import com.lanhu.ai.gateway.client.core.BusinessException;
import com.lanhu.ai.gateway.client.core.PcsResultCode;
import com.lanhu.ai.gateway.client.helper.AliYunOssHelper;
import com.lanhu.ai.gateway.client.vo.req.ImageFileUploadForm;
import com.lanhu.ai.gateway.client.vo.resp.ImageFileUploadVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.List;

@Service
@Slf4j
public class ImageService {

    private static final Logger LOG = LoggerFactory.getLogger(ImageService.class);

    @Resource
    private AliYunOssHelper aliYunOssHelper;

    @Autowired
    private FileUploadProperties fileUploadProperties;



    public ImageFileUploadVo imageFileUpload(ImageFileUploadForm imageFileUploadForm, HttpServletRequest request) {
        FileUploadProperties.Config config = null;
        List<FileUploadProperties.Config> configList = fileUploadProperties.getConfigs();
        for (FileUploadProperties.Config tempConfig : configList) {
            if (StringUtils.equals(tempConfig.getFileType(), imageFileUploadForm.getFileType())) {
                config = tempConfig;
                break;
            }
        }
        if (ObjectUtil.isNull(config)) {
            throw new BusinessException(PcsResultCode.UPLOAD_FILE_SUF_ERROR);
        }

        try {
            MultipartHttpServletRequest mhs = (MultipartHttpServletRequest) request;
            List<MultipartFile> commonsFiles = mhs.getFiles("file");
            if (commonsFiles.size() > 0) {
                MultipartFile commonsFile = commonsFiles.get(0);
                //获取文件原始名称
                String fileName = commonsFile.getOriginalFilename();
                //获取文件后缀
                assert fileName != null;
                String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
                //如果上传文件后缀不在规定列表
                if (!config.getFileSuffix().contains(suffix.toLowerCase())) {
                    throw new BusinessException(PcsResultCode.UPLOAD_FILE_SUF_ERROR);
                }
                //如果上传文件大小超过规定值
                if (commonsFile.getSize() > config.getFileMax()) {
                    throw new BusinessException(PcsResultCode.UPLOAD_OVER_LIMIT);
                }
                String url = null;

                url = aliYunOssHelper.uploadFile2Oss(config.getUploadDir() , suffix, commonsFile.getInputStream());


                ImageFileUploadVo imageFileUploadVO = new ImageFileUploadVo();
                imageFileUploadVO.setImg(url);
                return imageFileUploadVO;
            } else {
                throw new BusinessException(PcsResultCode.FILE_UPLOAD_ERROR);
            }
        } catch (Exception e) {
            LOG.error("文件上传失败",e);
            throw new BusinessException(PcsResultCode.FILE_UPLOAD_ERROR);
        }
    }


    public ImageFileUploadVo imageFileUpload(ImageFileUploadForm imageFileUploadForm, InputStream inputStream) {
        FileUploadProperties.Config config = null;
        List<FileUploadProperties.Config> configList = fileUploadProperties.getConfigs();
        for (FileUploadProperties.Config tempConfig : configList) {
            if (StringUtils.equals(tempConfig.getFileType(), imageFileUploadForm.getFileType())) {
                config = tempConfig;
                break;
            }
        }
        if (ObjectUtil.isNull(config)) {
            throw new BusinessException(PcsResultCode.UPLOAD_FILE_SUF_ERROR);
        }

        try {
            String url = null;
            url = aliYunOssHelper.uploadFile2Oss(config.getUploadDir(), "jpg", inputStream);
            ImageFileUploadVo imageFileUploadVO = new ImageFileUploadVo();
            imageFileUploadVO.setImg(url);
            return imageFileUploadVO;
        } catch (Exception e) {
            LOG.error("文件上传失败",e);
            throw new BusinessException(PcsResultCode.FILE_UPLOAD_ERROR);
        }
    }


}
