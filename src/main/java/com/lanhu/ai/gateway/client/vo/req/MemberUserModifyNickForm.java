package com.lanhu.ai.gateway.client.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


@ApiModel("用户修改昵称入参")
@Data
public class MemberUserModifyNickForm {
    /**
     * 昵称
     */
    @ApiModelProperty(value = "昵称", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String nick;
}
