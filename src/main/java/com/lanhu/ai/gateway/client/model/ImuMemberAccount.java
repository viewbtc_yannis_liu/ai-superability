package com.lanhu.ai.gateway.client.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 会员账号表
 */
@ApiModel(value = "会员账号表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "imu_member_account")
public class ImuMemberAccount {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "ID")
    private Long id;

    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 总积分(可用积分+冻结积分)
     */
    @TableField(value = "point")
    @ApiModelProperty(value = "总积分(可用积分+冻结积分)")
    private BigDecimal point;

    /**
     * 可用积分
     */
    @TableField(value = "available_point")
    @ApiModelProperty(value = "可用积分")
    private BigDecimal availablePoint;

    /**
     * 冻结积分
     */
    @TableField(value = "freeze_point")
    @ApiModelProperty(value = "冻结积分")
    private BigDecimal freezePoint;

    /**
     * 状态:0: 正常 1:冻结
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "状态:0: 正常 1:冻结")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "更新时间")
    private Long updateTime;

    /**
     * 逻辑删除的标记  0正常 , 1删除
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除的标记  0正常 , 1删除")
    private Integer isDelete;
}