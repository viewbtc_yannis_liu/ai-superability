package com.lanhu.ai.gateway.client.enums;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum RequestHeaderPlatformEnum {
    /**
     * 微信
     */
    WECHAT("wechat", "微信"),

    /**
     * Line
     */
    LINE("line", "Line"),
    ;

    private final String platform;

    private final String msg;

    public static RequestHeaderPlatformEnum convert(String platform) {
        for (RequestHeaderPlatformEnum platformEnum : RequestHeaderPlatformEnum.values()) {
            if (StrUtil.equals(platformEnum.getPlatform(), platform)) {
                return platformEnum;
            }
        }
        return RequestHeaderPlatformEnum.WECHAT;
    }
}
