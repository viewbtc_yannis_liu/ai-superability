package com.lanhu.ai.gateway.client.vo.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/06/28 15:12
 */
@ApiModel("微信下单出参")
@Data
public class UnifiedOrderResultVo {

    @ApiModelProperty(value = "appId")
    private String appId;

    @ApiModelProperty(value = "时间戳")
    private Long timeStamp;

    @ApiModelProperty(value = "随机数")
    private String nonceStr;

    @ApiModelProperty(value = "签名方式")
    private String signType;

    @ApiModelProperty(value = "package")
    private String packageStr;

    @ApiModelProperty(value = "签名串")
    private String paySign;


    @JsonIgnore
    private String prepayID;


}
