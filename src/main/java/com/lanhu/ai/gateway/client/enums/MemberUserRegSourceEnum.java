package com.lanhu.ai.gateway.client.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum MemberUserRegSourceEnum {
    /**
     * 未知
     */
    UNKNOWN(new Byte("0"), "未知"),

    /**
     * 微信
     */
    WECHAT(new Byte("1"), "微信"),

    /**
     * Line
     */
    LINE(new Byte("2"), "Line"),

    /**
     * 微信小程序
     */
    WECHAT_MA(new Byte("3"), "微信小程序"),

    /**
     * Apple
     */
    APPLE(new Byte("4"), "Apple"),
    ;

    private final byte source;

    private final String msg;

    public static MemberUserRegSourceEnum convert(int source) {
        for (MemberUserRegSourceEnum enums : MemberUserRegSourceEnum.values()) {
            if (enums.getSource() == source) {
                return enums;
            }
        }
        return MemberUserRegSourceEnum.UNKNOWN;
    }
}
