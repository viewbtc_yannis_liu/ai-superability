package com.lanhu.ai.gateway.client.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/********************************
 * @title FileProperties
 * @package com.lanhu.imenu.gateway.client.config
 * @description description
 *
 * @author Mr.ｓｕ＇ｑｉａｎｇ
 * @date 2020/11/28 3:25 下午
 * @version 0.0.1
 *********************************/
@Data
@ConfigurationProperties(prefix = "file.upload")
@Component
public class FileUploadProperties {
    private List<Config> configs;

    @Data
    public static class Config {
        /**
         * 文件类型
         */
        private String fileType;

        /**
         * 可上传文件数量
         */
        private Integer fileSize;

        /**
         * 可上传单个文件大小
         */
        private Integer fileMax;

        /**
         * 文件后缀
         */
        private String fileSuffix;

        /**
         * 上传目录
         */
        private String uploadDir;


    }
}
