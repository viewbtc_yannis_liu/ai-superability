package com.lanhu.ai.gateway.client.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@ApiModel(value = "用户id")
@Data
public class MemberUserForm{

    @ApiModelProperty(value = "用户id")
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    private Long userId ;


}
