package com.lanhu.ai.gateway.client.vo.req;

import com.google.common.collect.Lists;
import com.lanhu.ai.gateway.client.enums.ChatGptModelEnum;
import com.lanhu.ai.gateway.client.model.RoleMessage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/1/11 4:53 下午
 */

@ApiModel(value = "chatgpt 问题咨询入参")
@Data
public class ChatGptQaForm {


    @ApiModelProperty(value = "分类ID", required = true)
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    private Integer categoryId;


    @ApiModelProperty(value = "温度，使用什么采样温度，介于 0 和 2 之间。较高的值（如 0.8）将使输出更加随机，而较低的值（如 0.2）将使其更加集中和确定，默认为0。", required = true)
    private Double temperature;




    /**
     * 角色名称
     */
    @ApiModelProperty(value = "提示信息", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String message;



    @ApiModelProperty(value = "chatgpt模式(chatgpt3.5: gpt-3.5-turbo, chatgpt4: gpt-4,默认为：gpt-3.5-turbo)")
    private String model = ChatGptModelEnum.gpt_35_turbo.getCode();



}
