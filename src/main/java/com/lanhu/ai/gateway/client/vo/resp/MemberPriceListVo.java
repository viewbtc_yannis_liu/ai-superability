package com.lanhu.ai.gateway.client.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @auth: huhouchun
 * @version: 1.0.0
 * @date: 2023/06/28 17:51
 */
@ApiModel("会员价格列表出参")
@Data
public class MemberPriceListVo {

    
    @ApiModelProperty(value = "价格列表")
    private List<MemberPriceVo> list;


}
