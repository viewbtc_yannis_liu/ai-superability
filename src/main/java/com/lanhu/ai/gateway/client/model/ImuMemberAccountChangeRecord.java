package com.lanhu.ai.gateway.client.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 会员账号积分变化记录表
 */
@ApiModel(value = "会员账号积分变化记录表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "imu_member_account_change_record")
public class ImuMemberAccountChangeRecord {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "ID")
    private Long id;

    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 账户ID
     */
    @TableField(value = "account_id")
    @ApiModelProperty(value = "账户ID")
    private Long accountId;

    /**
     * 修改前账户积分
     */
    @TableField(value = "before_point")
    @ApiModelProperty(value = "修改前账户积分")
    private BigDecimal beforePoint;

    /**
     * 修改后账户积分
     */
    @TableField(value = "after_point")
    @ApiModelProperty(value = "修改后账户积分")
    private BigDecimal afterPoint;

    /**
     * 变动积分
     */
    @TableField(value = "change_point")
    @ApiModelProperty(value = "变动积分")
    private BigDecimal changePoint;

    /**
     * 0:充值 1:消费
     */
    @TableField(value = "`type`")
    @ApiModelProperty(value = "0:充值 1:消费")
    private Integer type;

    /**
     * 描述
     */
    @TableField(value = "`desc`")
    @ApiModelProperty(value = "描述")
    private String desc;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "更新时间")
    private Long updateTime;

    /**
     * 逻辑删除的标记  0正常 , 1删除
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除的标记  0正常 , 1删除")
    private Integer isDelete;
}