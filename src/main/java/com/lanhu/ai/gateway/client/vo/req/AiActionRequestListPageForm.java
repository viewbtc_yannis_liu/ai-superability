package com.lanhu.ai.gateway.client.vo.req;

import com.lanhu.ai.gateway.client.core.PageForm;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/1/16 10:42 上午
 */

@ApiModel(value = "ai操作请求结果分页查询入参")
@Data
public class AiActionRequestListPageForm extends PageForm {


    @ApiModelProperty(value = "开始时间")
    private String start;

    @ApiModelProperty(value = "结束时间")
    private String end;




}
