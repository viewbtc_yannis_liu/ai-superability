package com.lanhu.ai.gateway.client.annotation;
import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IgnoreSecurity {
    // 加上此注解，忽略身份token检验
}
