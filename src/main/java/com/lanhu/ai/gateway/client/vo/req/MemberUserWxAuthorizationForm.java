package com.lanhu.ai.gateway.client.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


@ApiModel("微信用户授权参数入参")
@Data
public class MemberUserWxAuthorizationForm {
    /**
     * 微信小程序获取的jsCode
     */
    @ApiModelProperty(value = "微信小程序获取的jsCode", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String jsCode;
}
