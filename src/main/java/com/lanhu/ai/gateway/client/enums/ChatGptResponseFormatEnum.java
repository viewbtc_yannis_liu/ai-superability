package com.lanhu.ai.gateway.client.enums;

/**
 * @author liuyi
 * @version 1.0
 * @date 2022/7/20 6:58 下午
 */
public enum ChatGptResponseFormatEnum {

    /**
     * 其它
     */

    //  /v1/chat/completions
    b64_json("b64_json", "b64_json"),

    url("url", "url"),


    ;
    private final String code;

    private final String msg;

    ChatGptResponseFormatEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
