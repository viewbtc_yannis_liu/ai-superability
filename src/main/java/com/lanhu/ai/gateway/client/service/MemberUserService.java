package com.lanhu.ai.gateway.client.service;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.lanhu.ai.gateway.client.annotation.LhTransaction;
import com.lanhu.ai.gateway.client.bo.WxMaUserInfoRequestBO;
import com.lanhu.ai.gateway.client.bo.WxMaUserInfoResultBO;
import com.lanhu.ai.gateway.client.configurer.wx.WxMaConfigurer;
import com.lanhu.ai.gateway.client.core.*;
import com.lanhu.ai.gateway.client.mapper.ImuMemberAccountMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberAccount;
import com.lanhu.ai.gateway.client.utils.WXBizDataCrypt;
import com.lanhu.ai.gateway.client.vo.req.*;
import com.lanhu.ai.gateway.client.core.jwt.Jwt;
import com.lanhu.ai.gateway.client.enums.MemberUserGenderEnum;
import com.lanhu.ai.gateway.client.enums.MemberUserRegSourceEnum;
import com.lanhu.ai.gateway.client.enums.MemberUserStatusEnum;
import com.lanhu.ai.gateway.client.mapper.ImuMemberUserMapper;
import com.lanhu.ai.gateway.client.mapper.ImuMemberUserThirdpartMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.model.ImuMemberUserThirdpart;
import com.lanhu.ai.gateway.client.utils.ThreadLocalUtil;
import com.lanhu.ai.gateway.client.vo.resp.MemberUserInfoVo;
import com.lanhu.ai.gateway.client.vo.resp.MemberUserLoginVo;
import com.lanhu.ai.gateway.client.vo.resp.MemberUserWxAuthorizationNewVo;
import com.lanhu.ai.gateway.client.vo.resp.MemberUserWxAuthorizationVo;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;

@Slf4j
@Service
public class MemberUserService {


    /**
     * 有效天数
     */
    public static final int EFFECTIVE_DAYS = 180;


    @Resource
    private ImuMemberUserMapper imuMemberUserMapper;

    @Resource
    private ImuMemberUserThirdpartMapper imuMemberUserThirdpartMapper;

    @Resource
    private ImuMemberAccountMapper imuMemberAccountMapper;


    public ImuMemberUser findMemberUserById(Long id) {
        return imuMemberUserMapper.selectById(id);
    }

    public PcsResult<MemberUserWxAuthorizationVo> wxAuthorization(MemberUserWxAuthorizationForm memberUserWxAuthorizationForm) {
        String maAppId = (String) ThreadLocalUtil.get(ProjectConstant.THREAD_LOCAL_WX_APP_ID_FLAG);
        try {
            final WxMaService wxService = WxMaConfigurer.getMaService(maAppId);
            WxMaJscode2SessionResult session = wxService.getUserService().getSessionInfo(memberUserWxAuthorizationForm.getJsCode());
            MemberUserWxAuthorizationVo memberUserWxAuthorizationVo = new MemberUserWxAuthorizationVo();
            memberUserWxAuthorizationVo.setOpenid(session.getOpenid());
            memberUserWxAuthorizationVo.setSessionKey(session.getSessionKey());


            return ResponseResult.ok(memberUserWxAuthorizationVo);
        } catch (WxErrorException e) {
            log.error("根据jsCode换取openId以及sessionKey信息出现错误, maAppId={}, jsCode={}", maAppId, memberUserWxAuthorizationForm.getJsCode(), e);
            //return new PcsResult(PcsResultCode.ERROR_OPERATE.getCode(), e.getError().getErrorMsg() );
            return new PcsResult(PcsResultCode.ERROR_OPERATE.getCode(), e.getMessage());
        }
    }

    public PcsResult<MemberUserWxAuthorizationNewVo> wxAuthorizationV2(MemberUserWxAuthorizationForm memberUserWxAuthorizationForm) {
        String maAppId = (String) ThreadLocalUtil.get(ProjectConstant.THREAD_LOCAL_WX_APP_ID_FLAG);
        try {
            final WxMaService wxService = WxMaConfigurer.getMaService(maAppId);
            log.info("MemberUserService wxAuthorizationV2 maAppId:{} , wxService:{}" , maAppId , wxService );
            WxMaJscode2SessionResult session = wxService.getUserService().getSessionInfo(memberUserWxAuthorizationForm.getJsCode());
            log.info("MemberUserService wxAuthorizationV2 session:{}" , session );
            MemberUserWxAuthorizationNewVo memberUserWxAuthorizationVo = new MemberUserWxAuthorizationNewVo();
            memberUserWxAuthorizationVo.setOpenid(session.getOpenid());
            memberUserWxAuthorizationVo.setSessionKey(session.getSessionKey());

            //获取token  如果有值，则说明用户存在已经登录
            ImuMemberUser memberUser = this.gainMemberUserByOpenid(session.getOpenid());
            if (ObjectUtil.isNotNull(memberUser)) {
                MemberUserLoginVo loginVo = this.assemblyMemberUserLoginVo(memberUser);
                if (ObjectUtil.isNotNull(loginVo)) {
                    memberUserWxAuthorizationVo.setToken(loginVo.getToken());
                    log.info("MemberUserService wxAuthorizationV2 memberUser:{} , token:{}" , memberUser , loginVo.getToken() );
                }
            }
            return ResponseResult.ok(memberUserWxAuthorizationVo);
        } catch (WxErrorException e) {
            log.error("根据jsCode换取openId以及sessionKey信息出现错误, wxAuthorizationV2 maAppId={}, jsCode={}", maAppId, memberUserWxAuthorizationForm.getJsCode(), e);
            return new PcsResult(PcsResultCode.ERROR_OPERATE.getCode(), e.getMessage());
        }
    }


    /**
     * 授权解析手机号
     * @param form
     * @return
     */
    public WxMaPhoneNumberInfo wxGetNewPhoneNoInfo(MemberUserWxNewPhoneForm form ) {
        String maAppId = (String) ThreadLocalUtil.get(ProjectConstant.THREAD_LOCAL_WX_APP_ID_FLAG);
        final WxMaService wxService = WxMaConfigurer.getMaService(maAppId);
        WxMaPhoneNumberInfo newPhoneNoInfo = null;
        // 解密手机号
        try {
            newPhoneNoInfo = wxService.getUserService().getNewPhoneNoInfo(form.getCode());
            log.info("MemberUserService wxGetNewPhoneNoInfo code:{} , newPhoneNoInfo:{}" , form.getCode() , newPhoneNoInfo);
        } catch (WxErrorException e) {
            log.error("根据code换取手机号信息出现错误, maAppId={}, code={}", maAppId, form.getCode(), e);
            e.printStackTrace();
        }
        return newPhoneNoInfo;
    }




    /**
     * @deprecated
     * 授权解析手机号-老版
     * @param form
     * @return
     */
    public String wxGetPhoneNoInfo(MemberUserWxPhoneForm form ) {
        WXBizDataCrypt wxBizDataCrypt = new WXBizDataCrypt();
        return wxBizDataCrypt.parseData(null , form.getSessionKey() , form.getEncryptedPhoneData() , form.getPhoneIv() );
    }



    @LhTransaction
    public MemberUserLoginVo wxRegister(MemberUserWxRegisterForm memberUserWxRegisterForm) {
        ImuMemberUser memberUser = this.gainMemberUserByOpenid(memberUserWxRegisterForm.getOpenid());
        if (ObjectUtil.isNotNull(memberUser)) {
            return this.assemblyMemberUserLoginVo(memberUser);
        }
        WxMaUserInfoRequestBO wxMaUserInfoRequestBo = BeanUtil.copyProperties(memberUserWxRegisterForm, WxMaUserInfoRequestBO.class);
        log.info("MemberUserService wxRegister memberUser:{} , wxMaUserInfoRequestBo:{}" , memberUser , wxMaUserInfoRequestBo );
        WxMaUserInfoResultBO wxMaUserInfoResultBo = this.resolveWxMaUserInfo(wxMaUserInfoRequestBo);
        if (ObjectUtil.isNull(wxMaUserInfoResultBo)) {
            throw new BusinessException(PcsResultCode.GATEWAY_WX_USER_INFO_ERROR);
        }
        log.info("MemberUserService wxRegister wxMaUserInfoResultBo:{}" , wxMaUserInfoResultBo );
        String nickName = wxMaUserInfoResultBo.getNickName();
        String avatarUrl = wxMaUserInfoResultBo.getAvatarUrl();

        nickName = memberUserWxRegisterForm.getNick();
        avatarUrl = memberUserWxRegisterForm.getHeadPortrait();
        String mobile = memberUserWxRegisterForm.getMobile();
        String countryPrefix = memberUserWxRegisterForm.getCountryPrefix();

        // 写入用户信息
        //this.saveUser(wxMaUserInfoResultBo.getNickName(), wxMaUserInfoResultBo.getAvatarUrl(), wxMaUserInfoResultBo.getOpenid(), JSONUtil.toJsonStr(wxMaUserInfoResultBo), MemberUserRegSourceEnum.WECHAT_MA);
        this.saveUser(nickName , avatarUrl , mobile , countryPrefix , wxMaUserInfoResultBo.getOpenid(), JSONUtil.toJsonStr(wxMaUserInfoResultBo), MemberUserRegSourceEnum.WECHAT_MA);

        return this.wxLogin(wxMaUserInfoResultBo.getOpenid());
    }

    public MemberUserLoginVo wxLogin(String openid) {
        ImuMemberUser memberUser = this.gainMemberUserByOpenid(openid);
        if (ObjectUtil.isNull(memberUser)) {
            throw new BusinessException(PcsResultCode.MEMBER_USER_NOT_EXIST);
        }
        log.info("MemberUserService wxLogin memberUser:{}" , memberUser );
        return this.assemblyMemberUserLoginVo(memberUser);
    }


    public MemberUserInfoVo info(ImuMemberUser memberUser) {
        return this.assemblyMemberUserInfo(memberUser);
    }

    public void modifyHeadPortrait(MemberUserModifyHeadPortraitForm memberUserModifyHeadPortraitForm, ImuMemberUser imuMemberUser) {
        ImuMemberUser update = new ImuMemberUser();
        update.setId(imuMemberUser.getId());
        update.setHeadPortrait(memberUserModifyHeadPortraitForm.getHeadPortrait());
        imuMemberUserMapper.updateById(update);
    }

    public void modifyNick(MemberUserModifyNickForm memberUserModifyNickForm, ImuMemberUser imuMemberUser) {
        ImuMemberUser update = new ImuMemberUser();
        update.setId(imuMemberUser.getId());
        update.setNick(memberUserModifyNickForm.getNick());
        imuMemberUserMapper.updateById(update);
    }



    // ================================================================================== 私有方法区 ================================================================================== //

    /**
     * 根据openid获取用户信息
     *
     * @param openid openid
     * @return 用户信息
     */
    private ImuMemberUser gainMemberUserByOpenid(String openid) {
        ImuMemberUser imuMemberUser = null;
        LambdaQueryWrapper<ImuMemberUserThirdpart> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(ImuMemberUserThirdpart::getOpenId, openid);
        wrapper.eq(ImuMemberUserThirdpart::getThirdPartyType, MemberUserRegSourceEnum.WECHAT_MA.getSource());
        ImuMemberUserThirdpart imuMemberUserThirdpart = imuMemberUserThirdpartMapper.selectOne(wrapper);
        if (ObjectUtil.isNotNull(imuMemberUserThirdpart)) {
            imuMemberUser = imuMemberUserMapper.selectById(imuMemberUserThirdpart.getUserId());
        }
        log.info("MemberUserService gainMemberUserByOpenid imuMemberUser:{}" , imuMemberUser );
        return imuMemberUser;
    }

    /**
     * 根据line用户ID获取用户信息
     *
     * @param userId 用户ID
     * @return 用户信息
     */
    private ImuMemberUser gainMemberUserByLineUserId(String userId) {
        ImuMemberUser imuMemberUser = null;
        LambdaQueryWrapper<ImuMemberUserThirdpart> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(ImuMemberUserThirdpart::getUnionId, userId);
        wrapper.eq(ImuMemberUserThirdpart::getThirdPartyType, MemberUserRegSourceEnum.LINE.getSource());
        ImuMemberUserThirdpart imuMemberUserThirdpart = imuMemberUserThirdpartMapper.selectOne(wrapper);
        if (ObjectUtil.isNotNull(imuMemberUserThirdpart)) {
            imuMemberUser = imuMemberUserMapper.selectById(imuMemberUserThirdpart.getUserId());
        }
        return imuMemberUser;
    }

    /**
     * 根据手机号获取用户信息
     *
     * @param prefix 前缀
     * @param mobile 手机号
     * @return 用户信息
     */
    private ImuMemberUser gainMemberUserByMobile(String prefix, String mobile, Byte regSource) {
        LambdaQueryWrapper<ImuMemberUser> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(ImuMemberUser::getCountryPrefix, prefix);
        wrapper.eq(ImuMemberUser::getMobile, mobile);
        wrapper.eq(ImuMemberUser::getRegSource, regSource);
        return imuMemberUserMapper.selectOne(wrapper);
    }

    /**
     * 解析微信用户信息
     *
     * @param wxMaUserInfoRequestBo 用户原始信息
     * @return 微信用户信息
     */
    private WxMaUserInfoResultBO resolveWxMaUserInfo(WxMaUserInfoRequestBO wxMaUserInfoRequestBo) {
        String maAppId = (String) ThreadLocalUtil.get(ProjectConstant.THREAD_LOCAL_WX_APP_ID_FLAG);
        final WxMaService wxService = WxMaConfigurer.getMaService(maAppId);
        log.info("MemberUserService resolveWxMaUserInfo wxService:{}" , wxService );
        WxMaUserInfoResultBO wxMaUserInfoResultBo = null;
        // 用户信息校验
        if (wxService.getUserService().checkUserInfo(wxMaUserInfoRequestBo.getSessionKey(), wxMaUserInfoRequestBo.getRawData(), wxMaUserInfoRequestBo.getSignature())) {
            // 解密用户信息
            WxMaUserInfo userInfo = wxService.getUserService().getUserInfo(wxMaUserInfoRequestBo.getSessionKey(), wxMaUserInfoRequestBo.getEncryptedData(), wxMaUserInfoRequestBo.getIv());
            log.info("MemberUserService resolveWxMaUserInfo userInfo:{}" , userInfo );
            wxMaUserInfoResultBo = new WxMaUserInfoResultBO();
            wxMaUserInfoResultBo.setOpenid(wxMaUserInfoRequestBo.getOpenid());
            wxMaUserInfoResultBo.setUnionid(wxMaUserInfoRequestBo.getUnionid());
            wxMaUserInfoResultBo.setNickName(userInfo.getNickName());
            wxMaUserInfoResultBo.setGender(Integer.parseInt(userInfo.getGender()));
            wxMaUserInfoResultBo.setLanguage(userInfo.getLanguage());
            wxMaUserInfoResultBo.setCity(userInfo.getCity());
            wxMaUserInfoResultBo.setProvince(userInfo.getProvince());
            wxMaUserInfoResultBo.setCountry(userInfo.getCountry());
            wxMaUserInfoResultBo.setAvatarUrl(userInfo.getAvatarUrl());

            WxMaUserInfoResultBO.Watermark watermark = new WxMaUserInfoResultBO.Watermark();
            watermark.setTimestamp(userInfo.getWatermark().getTimestamp());
            watermark.setAppid(userInfo.getWatermark().getAppid());
            wxMaUserInfoResultBo.setWatermark(watermark);
            log.info("MemberUserService resolveWxMaUserInfo wxMaUserInfoResultBo:{}" , wxMaUserInfoResultBo );
        } else {
            log.error("获取用户信息时，用户信息校验失败！wxMaUserInfoRequestBo={}", wxMaUserInfoRequestBo);
        }

        return wxMaUserInfoResultBo;
    }

    /**
     * 组装用户登录信息
     *
     * @param memberUser 用户信息
     * @return 用户登录信息
     */
    private MemberUserLoginVo assemblyMemberUserLoginVo(ImuMemberUser memberUser) {
        MemberUserLoginVo memberLoginVo = BeanUtil.copyProperties(this.assemblyMemberUserInfo(memberUser), MemberUserLoginVo.class);
        memberLoginVo.setToken(Jwt.createToken(memberUser.getId()));
        log.info("MemberUserService assemblyMemberUserLoginVo memberLoginVo:{}" , memberLoginVo );
        return memberLoginVo;
    }

    /**
     * 组装微信用户信息
     *
     * @param memberUser 用户信息
     * @return 用户信息
     */
    private MemberUserInfoVo assemblyMemberUserInfo(ImuMemberUser memberUser) {
        MemberUserInfoVo memberUserInfoVo = new MemberUserInfoVo();
        memberUserInfoVo.setId(memberUser.getId());
        memberUserInfoVo.setNick(memberUser.getNick());
        memberUserInfoVo.setHeadPortrait(memberUser.getHeadPortrait());
        memberUserInfoVo.setPrefix(memberUser.getCountryPrefix());
        memberUserInfoVo.setMobile(memberUser.getMobile());
        //memberUserInfoVo.setCToken(memberUser.getCToken());
        //memberUserInfoVo.setBEncryptSign(this.assemblyBEncryptSign(memberUser.getId()));
        // 查询会员卡信息
        //MemberInfoVo memberInfoVo = SpringUtil.getBean(MemberService.class).info(memberUser);
        //memberUserInfoVo.setMemberInfoVo(memberInfoVo);
        log.info("MemberUserService assemblyMemberUserInfo memberUserInfoVo:{}" , memberUserInfoVo );

        //我的积分
        ImuMemberAccount account = imuMemberAccountMapper.selectByUserId(memberUser.getId());
        if (ObjectUtil.isNotNull(account)) {
            memberUserInfoVo.setPoint(account.getPoint());
        }else {
            memberUserInfoVo.setPoint(new BigDecimal(0));
        }
        return memberUserInfoVo;
    }

    /**
     * 保存用户信息
     *
     * @param nickname                昵称
     * @param headPortrait            头像
     * @param platUserId              平台用户ID
     * @param platUserInfoJson        平台用户信息json
     * @param memberUserRegSourceEnum 平台枚举
     * @return 用户信息
     */
    private ImuMemberUser saveUser(String nickname, String headPortrait, String mobile, String countryPrefix,  String platUserId, String platUserInfoJson, MemberUserRegSourceEnum memberUserRegSourceEnum) {
        Date now = new Date();
        // 写入用户表
        ImuMemberUser memberUserInsert = new ImuMemberUser();
        memberUserInsert.setName(StrUtil.EMPTY);
        memberUserInsert.setNick(nickname);
        //memberUserInsert.setMobile(StrUtil.EMPTY);
        //memberUserInsert.setCountryPrefix(StrUtil.EMPTY);
        memberUserInsert.setMobile(mobile);
        memberUserInsert.setCountryPrefix(countryPrefix);

        memberUserInsert.setEmail(StrUtil.EMPTY);
        memberUserInsert.setGender(MemberUserGenderEnum.UNKNOWN.getGender());
        memberUserInsert.setHeadPortrait(headPortrait);
        memberUserInsert.setBgImage(StrUtil.EMPTY);
        memberUserInsert.setOpenId(platUserId);
        memberUserInsert.setRegSource(memberUserRegSourceEnum.getSource());
        memberUserInsert.setPassword(StrUtil.EMPTY);
        memberUserInsert.setStatus(MemberUserStatusEnum.NORMAL.getStatus());
        memberUserInsert.setConstellation(StrUtil.EMPTY);
        memberUserInsert.setPersonalIntro(StrUtil.EMPTY);
        memberUserInsert.setRegion(StrUtil.EMPTY);
        memberUserInsert.setCreatedAt(now);
        memberUserInsert.setIsDelete(Boolean.FALSE);
        imuMemberUserMapper.insert(memberUserInsert);
        // 写入第三方平台
        ImuMemberUserThirdpart imuMemberUserThirdpart = new ImuMemberUserThirdpart();
        imuMemberUserThirdpart.setUserId(memberUserInsert.getId());
        imuMemberUserThirdpart.setThirdPartyType(memberUserRegSourceEnum.getSource());
        imuMemberUserThirdpart.setOpenId(ObjectUtil.equals(memberUserRegSourceEnum, MemberUserRegSourceEnum.WECHAT_MA) ? platUserId : StrUtil.EMPTY);
        imuMemberUserThirdpart.setUnionId(ObjectUtil.equals(memberUserRegSourceEnum, MemberUserRegSourceEnum.LINE) ? platUserId : StrUtil.EMPTY);
        imuMemberUserThirdpart.setThirdPartyUserInfo(platUserInfoJson);
        imuMemberUserThirdpart.setCreatedAt(now);
        imuMemberUserThirdpart.setIsDelete(Boolean.FALSE);
        imuMemberUserThirdpartMapper.insert(imuMemberUserThirdpart);
        log.info("MemberUserService saveUser imuMemberUserId:{} , imuMemberUserThirdpartId:{}" , memberUserInsert.getId() , imuMemberUserThirdpart.getId() );
        // 查询用户信息
        return imuMemberUserMapper.selectById(memberUserInsert.getId());
    }

}
