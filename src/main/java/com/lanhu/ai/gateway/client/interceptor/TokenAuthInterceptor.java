package com.lanhu.ai.gateway.client.interceptor;
import com.lanhu.ai.gateway.client.annotation.IgnoreSecurity;
import com.lanhu.ai.gateway.client.core.BaseController;
import com.lanhu.ai.gateway.client.core.ProjectConstant;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.utils.ThreadLocalUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

@Component
public class TokenAuthInterceptor extends BaseController implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) {
        if (o instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) o;
            Method method = handlerMethod.getMethod();
            if (method.isAnnotationPresent(IgnoreSecurity.class)) {
                return true;
            }
            ImuMemberUser memberUser = this.resolveUser(httpServletRequest);
            ThreadLocalUtil.put(ProjectConstant.THREAD_LOCAL_MEMBER_FLAG, memberUser);
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        // 删除ThreadLocal信息
        ThreadLocalUtil.remove();
    }
}
