package com.lanhu.ai.gateway.client.enums;


/**
 * 社区 状态  0未发布，1已发布
 */
public enum CommunityStatusEnum {

    OFFLINE(0, "未发布"),

    ONLINE(1, "已发布"),
    ;

    private final int code;

    private final String msg;

    CommunityStatusEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static CommunityStatusEnum convert(int code) {
        for (CommunityStatusEnum typeEnum : CommunityStatusEnum.values()) {
            if (typeEnum.getCode() == code) {
                return typeEnum;
            }
        }
        return CommunityStatusEnum.OFFLINE;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }


}
