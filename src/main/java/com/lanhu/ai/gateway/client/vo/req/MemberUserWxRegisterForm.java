package com.lanhu.ai.gateway.client.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel("微信用户注册入参")
@Data
public class MemberUserWxRegisterForm {
    /**
     * openid
     */
    @ApiModelProperty(value = "openid", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String openid;

    /**
     * 会话sessionKey
     */
    @ApiModelProperty(value = "会话sessionKey", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String sessionKey;

    /**
     * 使用 sha1( rawData + sessionKey ) 得到字符串，用于校验用户信息，
     */
    @ApiModelProperty(value = "签名", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String signature;

    /**
     * 不包括敏感信息的原始数据字符串，用于计算签名
     */
    @ApiModelProperty(value = "原始数据字符串", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String rawData;

    /**
     * 包括敏感数据在内的完整用户信息的加密数据
     */
    @ApiModelProperty(value = "加密数据", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String encryptedData;

    /**
     * 加密算法的初始向量
     */
    @ApiModelProperty(value = "加密算法的初始向量", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String iv;




    //2023-07-01
    @ApiModelProperty(value = "昵称")
    private String nick;

    @ApiModelProperty(value = "头像")
    private String headPortrait;

    @ApiModelProperty(value = "手机号(手机号码前缀+手机号码 如08076748710)")
    private String mobile;

    @ApiModelProperty(value = "国家前缀 如+81")
    private String countryPrefix;

}
