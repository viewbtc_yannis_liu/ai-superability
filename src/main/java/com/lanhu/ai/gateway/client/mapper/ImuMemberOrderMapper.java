package com.lanhu.ai.gateway.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberOrder;
import java.util.List;

import com.lanhu.ai.gateway.client.model.ImuMemberZhifu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ImuMemberOrderMapper extends BaseMapper<ImuMemberOrder> {
    int updateBatch(List<ImuMemberOrder> list);

    int updateBatchSelective(List<ImuMemberOrder> list);

    int batchInsert(@Param("list") List<ImuMemberOrder> list);

    int insertOrUpdate(ImuMemberOrder record);

    int insertOrUpdateSelective(ImuMemberOrder record);


    /**
     * 根据订单号查询
     * @param outTradeNo
     * @return
     */
    ImuMemberOrder selectByOutTradeNo(@Param("outTradeNo") String outTradeNo);
}