package com.lanhu.ai.gateway.client.controller;
import com.lanhu.ai.gateway.client.annotation.IgnoreSecurity;
import com.lanhu.ai.gateway.client.service.ImageService;
import com.lanhu.ai.gateway.client.core.BaseController;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.core.Result;
import com.lanhu.ai.gateway.client.vo.req.ImageFileUploadForm;
import com.lanhu.ai.gateway.client.vo.resp.ImageFileUploadVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@Api(value = "图片操作相关", tags = "图片操作相关")
@RestController
public class ImageController extends BaseController {

    @Autowired
    private ImageService imageService;



    @IgnoreSecurity
    @ApiOperation(value = "图片上传", notes = "图片上传")
    @PostMapping("/image/upload")
    public PcsResult<ImageFileUploadVo> imageFileUpload(@Validated ImageFileUploadForm imageFileUploadForm, HttpServletRequest request) {
        ImageFileUploadVo imageFileUploadVO = imageService.imageFileUpload(imageFileUploadForm, request);
        return Result.ok(imageFileUploadVO);
    }

}
