package com.lanhu.ai.gateway.client.vo.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("用户获取微信授权参数出参")
@Data
public class MemberUserWxAuthorizationNewVo {
    /**
     * 用户的openid
     */
    @ApiModelProperty(value = "用户的openId")
    private String openid;

    /**
     * 会话的sessionKey
     */
    @ApiModelProperty(value = "会话的sessionKey")
    private String sessionKey;


    
    @ApiModelProperty(value = "登录身份信息 (有值用户存在登录成功; 否则需要授权注册用户信息)")
    private String token;
}
