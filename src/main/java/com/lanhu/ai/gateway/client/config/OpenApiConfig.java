package com.lanhu.ai.gateway.client.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Component
@ConfigurationProperties(prefix = "openapi")
@Data
public class OpenApiConfig {
	
	
	//过期时间
	private List<String> tokens;





	private String token;




	private int duration;

	//过期时间:单位：分
	private int expireTime;


	public String getToken() {


		int index = new Random().nextInt(tokens.size());

		return tokens.get(index);

	}


}
