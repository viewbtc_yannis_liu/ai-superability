package com.lanhu.ai.gateway.client.cloud;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import com.lanhu.ai.gateway.client.config.OpenApiConfig;
import com.lanhu.ai.gateway.client.core.ProjectConstant;
import com.theokanning.openai.completion.chat.ChatCompletionChoice;
import com.theokanning.openai.completion.chat.ChatCompletionRequest;
import com.theokanning.openai.image.CreateImageEditRequest;
import com.theokanning.openai.image.CreateImageRequest;
import com.theokanning.openai.image.CreateImageVariationRequest;
import com.theokanning.openai.image.Image;
import com.theokanning.openai.service.OpenAiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.time.Duration;
import java.util.Date;
import java.util.List;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/6/25 4:45 下午
 */

@Service
@Slf4j
public class ChatGptCloudService {


    @Autowired
    private OpenApiConfig openApiConfig;


    /**
    * @description: 给定包含会话的消息列表，模型将返回响应
    * @param: [chatGptChatForm]
    * @return: java.lang.String
    * @author: liuyi
    * @date: 12:17 下午 2023/6/26
    */
    public List<ChatCompletionChoice>  chatCompletion(ChatCompletionRequest request){


        OpenAiService openAiService = new OpenAiService(openApiConfig.getToken(), Duration.ofSeconds(openApiConfig.getDuration()));



        List<ChatCompletionChoice> completionChoiceList =   openAiService.createChatCompletion(request).getChoices();


       return completionChoiceList;

    }

    /**
     * @description: 给定提示，模型将返回一个或多个预测完成
     * @param: [chatGptChatForm]
     * @return: java.lang.String
     * @author: liuyi
     * @date: 12:17 下午 2023/6/26
     */
    public  List<Image> createImage(CreateImageRequest request){


        log.info("====chatgpt image create start:{},prompt:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT),request.getPrompt());

        OpenAiService openAiService = new OpenAiService(openApiConfig.getToken(), Duration.ofSeconds(openApiConfig.getDuration()));




        List<Image> imageList =   openAiService.createImage(request).getData();



        log.info("====chatgpt image create end:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT));


        return imageList;

    }



    /**
     * @description: 创建图片变体
     * @param: [chatGptChatForm]
     * @return: java.lang.String
     * @author: liuyi
     * @date: 12:17 下午 2023/6/26
     */
    public  List<Image> createImageVariation(CreateImageVariationRequest request, File file){


        log.info("====chatgpt image variation create  start:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT));

        OpenAiService openAiService = new OpenAiService(openApiConfig.getToken(), Duration.ofSeconds(openApiConfig.getDuration()));




        List<Image> imageList =   openAiService.createImageVariation(request,file).getData();



        log.info("====chatgpt image variation create end:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT));


        return imageList;

    }




    /**
     * @description: 创建图片编辑
     * @param: [chatGptChatForm]
     * @return: java.lang.String
     * @author: liuyi
     * @date: 12:17 下午 2023/6/26
     */
    public  List<Image> createImageEdit(CreateImageEditRequest request, File file,File mask){


        log.info("====chatgpt image edit create  start:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT));

        OpenAiService openAiService = new OpenAiService(openApiConfig.getToken(), Duration.ofSeconds(openApiConfig.getDuration()));



        List<Image> imageList =   openAiService.createImageEdit(request,file,mask).getData();



        log.info("====chatgpt image edit create end:{}", DateUtil.format(new Date(), ProjectConstant.SCENARIO_DATE_FORMAT));


        return imageList;

    }

}
