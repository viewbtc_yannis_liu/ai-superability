package com.lanhu.ai.gateway.client.core;

import lombok.Getter;


@Getter
public enum PcsResultCode {
    /**
     * 0-成功
     * 1~99 基础业务错误信息
     * sys enum config
     */
    SUCCESS(0, "SUCCESS"),
    ERROR_OPERATE(1, "ERROR_OPERATE"),
    PARAMS_ERROR(2, "PARAMS_ERROR"),
    DATA_HANDLE_ERROR(3, "DATA_HANDLE_ERROR"),
    SEARCH_ERROR(4, "SEARCH_ERROR"),
    NOT_DATA_OWNER(5, "NOT_DATA_OWNER"),
    NOT_FOUND_HANDLER(7, "NOT_FOUND_HANDLER"),
    PARAM_VALIDATED_ERROR(8, "PARAM_VALIDATED_ERROR"),
    METHOD_NOT_SUPPORT(9, "METHOD_NOT_SUPPORT"),
    MEDIA_TYPE_NOT_SUPPORT(10, "MEDIA_TYPE_NOT_SUPPORT"),
    PARAM_LOSE(11, "PARAM_LOSE"),
    PARAM_TYPE_NOT_SUPPORT(12, "PARAM_TYPE_NOT_SUPPORT"),
    FILE_UPLOAD_ERROR(13, "FILE_UPLOAD_ERROR"),
    UPLOAD_FILE_CONFIG_ISNULL_ERROR(14, "UPLOAD_FILE_CONFIG_ISNULL_ERROR"),
    UPLOAD_OVER_SIZE(15, "UPLOAD_OVER_SIZE"),
    UPLOAD_FILE_SUF_ERROR(16, "UPLOAD_FILE_SUF_ERROR"),
    UPLOAD_OVER_LIMIT(17, "UPLOAD_OVER_LIMIT"),

    GATEWAY_TOKEN_FAILURE(90, "GATEWAY_TOKEN_FAILURE"),
    GATEWAY_WX_AUTHORIZATION_ERROR(91, "GATEWAY_WX_AUTHORIZATION_ERROR"),
    GATEWAY_WX_USER_INFO_ERROR(92, "GATEWAY_WX_USER_INFO_ERROR"),

    // 平台相关
    PLAT_CAPTCHA_SEND_FREQUENT(1000, "PLAT_CAPTCHA_SEND_FREQUENT"),
    PLAT_CAPTCHA_NOT_EXIST(1001, "PLAT_CAPTCHA_NOT_EXIST"),
    PLAT_WX_APPID_EMPTY(1002, "PLAT_WX_APPID_EMPTY"),

    // 用户相关
    MEMBER_USER_NOT_EXIST(1100, "MEMBER_USER_NOT_EXIST"),
    MEMBER_USER_MOBILE_WAS_EXIST(1101, "MEMBER_USER_MOBILE_WAS_EXIST"),
    MEMBER_USER_MOBILE_BIND_ERROR(1102, "MEMBER_USER_MOBILE_BIND_ERROR"),

    //下单支付相关
    ORDER_PARAM_ERROR(1200, "ORDER_PARAM_ERROR"),




    //chatgpt相关
    CHATGPT_CATEGORY_EMPTY(2000, "CHATGPT_CATEGORY_EMPTY"),
    CHATGPT_EXIST_TASK_HANDING(2001, "CHATGPT_EXIST_TASK_HANDING"),
    CHATGPT_IMAGE_DOWNLOAD_ERROR(2002, "CHATGPT_IMAGE_DOWNLOAD_ERROR"),
    TEMPLATE_NOT_EXIST(2003, "TEMPLATE_NOT_EXIST"),
    TEMPLATE_PROMPT_NOT_EMPTY(2004, "TEMPLATE_PROMPT_NOT_EMPTY"),
    CHATGPT_SERVICE_NEED_RECHARGE(2005, "CHATGPT_SERVICE_NEED_RECHARGE"),
    CHATGPT_SERVICE_NOT_BALANCE(2006, "CHATGPT_SERVICE_NOT_BALANCE"),


    ;

    private final Integer code;

    private final String desc;

    PcsResultCode(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 查询异常类型
     *
     * @param code 异常code
     * @return enum pcs
     */
    public static PcsResultCode convert(Integer code) {
        for (PcsResultCode returnCode : values()) {
            if (code.equals(returnCode.code)) {
                return returnCode;
            }
        }
        throw new IllegalArgumentException("No matching constant for [" + code + "]");
    }
}
