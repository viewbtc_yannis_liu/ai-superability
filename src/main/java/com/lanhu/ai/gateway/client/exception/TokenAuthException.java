package com.lanhu.ai.gateway.client.exception;

import com.lanhu.ai.gateway.client.core.BusinessException;
import com.lanhu.ai.gateway.client.core.PcsResultCode;


public class TokenAuthException extends BusinessException {
    public TokenAuthException() {
        super(PcsResultCode.GATEWAY_TOKEN_FAILURE);
    }

    public TokenAuthException(PcsResultCode code) {
        super(code);
    }

    public TokenAuthException(PcsResultCode code, String message) {
        super(code, message);
    }
}
