package com.lanhu.ai.gateway.client.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lanhu.ai.gateway.client.model.ImuMemberAccount;
import java.math.BigDecimal;import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ImuMemberAccountMapper extends BaseMapper<ImuMemberAccount> {
    int updateBatch(List<ImuMemberAccount> list);

    int updateBatchSelective(List<ImuMemberAccount> list);

    int batchInsert(@Param("list") List<ImuMemberAccount> list);

    int insertOrUpdate(ImuMemberAccount record);

    int insertOrUpdateSelective(ImuMemberAccount record);

    int reduceBalance(@Param("userId") Long userId, @Param("reduceAmount") BigDecimal reduceAmount);

    ImuMemberAccount selectByUserId(@Param("userId") Long userId);

    /**
     * 充值
     * @param userId
     * @param rechargeAmount
     * @return
     */
    int rechargeBalance(@Param("userId") Long userId, @Param("rechargeAmount") BigDecimal rechargeAmount);
}