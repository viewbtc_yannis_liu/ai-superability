package com.lanhu.ai.gateway.client.vo.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@ApiModel("用户修改头像入参")
@Data
public class MemberUserModifyHeadPortraitForm {
    /**
     * 头像地址
     */
    @ApiModelProperty(value = "头像地址", required = true)
    @NotBlank(message = "javax.validation.constraints.NotBlank.message")
    private String headPortrait;
}
