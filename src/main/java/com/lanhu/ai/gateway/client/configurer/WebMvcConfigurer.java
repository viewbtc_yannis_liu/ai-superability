package com.lanhu.ai.gateway.client.configurer;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.lanhu.ai.gateway.client.resolver.I18nLocaleResolver;
import com.lanhu.ai.gateway.client.core.ProjectConstant;
import com.lanhu.ai.gateway.client.interceptor.MdcInterceptor;
import com.lanhu.ai.gateway.client.interceptor.TokenAuthInterceptor;
import com.lanhu.ai.gateway.client.interceptor.WxAppIdInterceptor;
import org.hibernate.validator.HibernateValidator;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;


@Configuration
public class WebMvcConfigurer extends WebMvcConfigurationSupport {
    @Resource
    private MessageSource messageSource;

    @Resource
    private MdcInterceptor mdcInterceptor;


    @Resource
    private WxAppIdInterceptor wxAppIdInterceptor;

    @Resource
    private TokenAuthInterceptor tokenAuthInterceptor;

    @Override
    protected void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper objectMapper = converter.getObjectMapper();
        // 生成JSON时,将所有Long转换成String
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
        simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
        objectMapper.registerModule(simpleModule);
        // 时间格式化
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setDateFormat(new SimpleDateFormat(DatePattern.NORM_DATETIME_PATTERN));
        objectMapper.setTimeZone(ProjectConstant.TIME_ZONE);
        // 设置格式化内容
        converter.setObjectMapper(objectMapper);
        converters.add(0, converter);
    }

    /**
     * 解决跨域问题
     *
     * @param registry 注册
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowCredentials(true)
                .allowedHeaders("*")
                .allowedOriginPatterns("*")
                .allowedMethods("GET", "POST", "PUT", "OPTIONS");
    }

    /**
     * 添加拦截器
     *
     * @param registry 拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        String[] exclude = new String[]{
                "/swagger-resources/**",
                "/v2/api-docs",
                "/v2/api-docs-ext",
                "/configuration/security",
                "/configuration/ui",
                "/swagger-ui.html",
                "/error",
                "/webjars/**",
                "/favicon.ico",
                "/doc.html"
        };
        registry.addInterceptor(tokenAuthInterceptor).addPathPatterns("/**").excludePathPatterns(exclude);
        registry.addInterceptor(mdcInterceptor).addPathPatterns("/**").excludePathPatterns(exclude);
        registry.addInterceptor(wxAppIdInterceptor).addPathPatterns("/**").excludePathPatterns(exclude);
    }

    /**
     * 静态资源拦截器
     *
     * @param registry 拦截器
     */
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        //定向swagger 位置
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("doc.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("service-worker.js").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Override
    public Validator mvcValidator() {
        LocalValidatorFactoryBean factoryBean = new LocalValidatorFactoryBean();
        factoryBean.setValidationMessageSource(messageSource);
        factoryBean.setProviderClass(HibernateValidator.class);
        Properties properties = new Properties();
        properties.setProperty("hibernate.validator.fail_fast", "true");
        factoryBean.setValidationProperties(properties);
        factoryBean.afterPropertiesSet();
        return factoryBean;
    }

    @Override
    public LocaleResolver localeResolver() {
        return new I18nLocaleResolver();
    }
}
