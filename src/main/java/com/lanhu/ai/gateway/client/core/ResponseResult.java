package com.lanhu.ai.gateway.client.core;


public class ResponseResult {
    public static  PcsResult<Void> ok() {
        return new PcsResult<>(PcsResultCode.SUCCESS);
    }

    public static <T> PcsResult<T> ok(T data) {
        PcsResult<T> pcsResult = new PcsResult<>(PcsResultCode.SUCCESS);
        pcsResult.setData(data);
        return pcsResult;
    }

    public static <T> PcsResult<T> ok(PcsResultCode code, T data) {
        PcsResult<T> pcsResult = new PcsResult<>(code);
        pcsResult.setData(data);
        return pcsResult;
    }

    public static  PcsResult<Void> error(PcsResultCode code) {
        return new PcsResult<>(code);
    }

    public static  PcsResult<Void> error() {
        return new PcsResult<>(PcsResultCode.ERROR_OPERATE);
    }
}
