package com.lanhu.ai.gateway.client.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;


@Data
@TableName(value = "imu_member_user")
public class ImuMemberUser {
    /**
     * 用户ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 姓名
     */
    @TableField(value = "`name`")
    private String name;

    /**
     * 昵称
     */
    @TableField(value = "nick")
    private String nick;

    /**
     * 手机号(手机号码前缀+手机号码)
     */
    @TableField(value = "mobile")
    private String mobile;

    /**
     * 国家前缀
     */
    @TableField(value = "country_prefix")
    private String countryPrefix;

    /**
     * 邮箱
     */
    @TableField(value = "email")
    private String email;

    /**
     * 性别，0表示未知，1表示男，2表示女
     */
    @TableField(value = "gender")
    private Byte gender;

    /**
     * 头像图片的url
     */
    @TableField(value = "head_portrait")
    private String headPortrait;

    /**
     * 背景图url
     */
    @TableField(value = "bg_image")
    private String bgImage;

    /**
     * open_id(不同注册来源open_id，当直接是手机号码或者邮箱注册，open_id为32唯一随机数)
     */
    @TableField(value = "open_id")
    private String openId;

    /**
     * 注册来源，0表示未知，1表示微信注册，2表示Line注册，3表示微信小程序  ，4表示APPLE
     */
    @TableField(value = "reg_source")
    private Byte regSource;

    /**
     * 登录密码
     */
    @TableField(value = "`password`")
    private String password;

    /**
     * 账户的状态，0表示正常，1表示禁用
     */
    @TableField(value = "`status`")
    private Byte status;

    /**
     * 生日
     */
    @TableField(value = "birthday")
    private Date birthday;

    /**
     * 星座
     */
    @TableField(value = "constellation")
    private String constellation;

    /**
     * 个人简介
     */
    @TableField(value = "personal_intro")
    private String personalIntro;

    /**
     * 所在地区
     */
    @TableField(value = "region")
    private String region;

    /**
     * 创建时间
     */
    @TableField(value = "created_at")
    private Date createdAt;

    /**
     * 更新时间
     */
    @TableField(value = "updated_at")
    private Date updatedAt;

    /**
     * 逻辑删除的标记  0正常 , 1删除
     */
    @TableField(value = "is_delete")
    private Boolean isDelete;

}