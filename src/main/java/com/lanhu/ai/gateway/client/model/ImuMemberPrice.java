package com.lanhu.ai.gateway.client.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 会员价格表
 */
@ApiModel(value = "会员价格表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "imu_member_price")
public class ImuMemberPrice {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "ID")
    private Long id;

    /**
     * 价格名称
     */
    @TableField(value = "`name`")
    @ApiModelProperty(value = "价格名称")
    private String name;

    /**
     * 价格描述
     */
    @TableField(value = "`desc`")
    @ApiModelProperty(value = "价格描述")
    private String desc;

    /**
     * 价格 (分)
     */
    @TableField(value = "price")
    @ApiModelProperty(value = "价格 (分)")
    private BigDecimal price;

    /**
     * 积分
     */
    @TableField(value = "point")
    @ApiModelProperty(value = "积分")
    private BigDecimal point;

    /**
     * 单次 (分)
     */
    @TableField(value = "unit_price")
    @ApiModelProperty(value = "单次 (分)")
    private BigDecimal unitPrice;

    /**
     * 状态，0正常，1禁用
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "状态，0正常，1禁用")
    private Byte status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "更新时间")
    private Long updateTime;

    /**
     * 逻辑删除的标记  0正常 , 1删除
     */
    @TableField(value = "is_delete")
    @ApiModelProperty(value = "逻辑删除的标记  0正常 , 1删除")
    private Integer isDelete;
}