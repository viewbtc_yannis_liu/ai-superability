package com.lanhu.ai.gateway.client.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "imu_member_user_thirdpart")
public class ImuMemberUserThirdpart {
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户信息ID
     */
    @TableField(value = "user_id")
    private Long userId;

    /**
     * 第三方的类型，1表示微信登录  2表示Line小程序  3表示微信小程序  4表示APPLE登录
     */
    @TableField(value = "third_party_type")
    private Byte thirdPartyType;

    /**
     * openid
     */
    @TableField(value = "open_id")
    private String openId;

    /**
     * 第三方用户的唯一ID 微信的unionid，Line的userId
     */
    @TableField(value = "union_id")
    private String unionId;

    /**
     * 第三方用户数据
     */
    @TableField(value = "third_party_user_info")
    private String thirdPartyUserInfo;

    /**
     * 创建时间
     */
    @TableField(value = "created_at")
    private Date createdAt;

    /**
     * 更新时间
     */
    @TableField(value = "updated_at")
    private Date updatedAt;

    /**
     * 逻辑删除的标记  0正常 , 1删除
     */
    @TableField(value = "is_delete")
    private Boolean isDelete;
}