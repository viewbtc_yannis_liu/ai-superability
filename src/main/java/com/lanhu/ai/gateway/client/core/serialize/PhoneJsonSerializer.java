package com.lanhu.ai.gateway.client.core.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.lanhu.ai.gateway.client.utils.SensitiveInfoUtil;

import java.io.IOException;

public class PhoneJsonSerializer extends JsonSerializer<String> {

    @Override
    public void serialize(String s, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (s != null) {
            jsonGenerator.writeString(SensitiveInfoUtil.mobilePhone(s));
        }else{
            jsonGenerator.writeString("");
        }

    }
}