package com.lanhu.ai.gateway.client.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONUtil;
import com.aliyuncs.unmarshaller.JsonUnmashaller;
import com.google.common.collect.Lists;
import com.lanhu.ai.gateway.client.cloud.ChatGptCloudService;
import com.lanhu.ai.gateway.client.config.FileUploadProperties;
import com.lanhu.ai.gateway.client.config.OpenApiConfig;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.core.PcsResultCode;
import com.lanhu.ai.gateway.client.core.ProjectConstant;
import com.lanhu.ai.gateway.client.core.Result;
import com.lanhu.ai.gateway.client.enums.*;
import com.lanhu.ai.gateway.client.helper.AliYunOssHelper;
import com.lanhu.ai.gateway.client.mapper.AiActionRequestMapper;
import com.lanhu.ai.gateway.client.mapper.AiChatgptCategoryMapper;
import com.lanhu.ai.gateway.client.mapper.AiImageTemplateMapper;
import com.lanhu.ai.gateway.client.mapper.ImuMemberAccountMapper;
import com.lanhu.ai.gateway.client.model.*;
import com.lanhu.ai.gateway.client.utils.FileUtil;
import com.lanhu.ai.gateway.client.vo.req.*;
import com.theokanning.openai.completion.chat.ChatCompletionChoice;
import com.theokanning.openai.completion.chat.ChatCompletionRequest;
import com.theokanning.openai.completion.chat.ChatMessage;
import com.theokanning.openai.completion.chat.ChatMessageRole;
import com.theokanning.openai.image.CreateImageEditRequest;
import com.theokanning.openai.image.CreateImageRequest;
import com.theokanning.openai.image.CreateImageVariationRequest;
import com.theokanning.openai.image.Image;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
* @description: chagpt Completion 服务
* @param: 
* @return: 
* @author: liuyi
* @date: 1:14 下午 2023/6/26 
*/
@Service
@Slf4j
public class ChatGptService {

    @Autowired
    private ChatGptCloudService chatGptCloudService;

    @Autowired
    private AiChatgptCategoryMapper chatgptCategoryMapper;


    @Resource(name = "asyncExecutor")
    private ThreadPoolTaskExecutor asyncExecutor;

    @Autowired
    private AiActionRequestMapper aiActionRequestMapper;


    @Autowired
    private AliYunOssHelper aliYunOssHelper;

    @Resource
    private FileUploadProperties fileUploadProperties;

    @Autowired
    private AiImageTemplateMapper aiImageTemplateMapper;

    @Autowired
    private OpenApiConfig openApiConfig;

    @Autowired
    private ImuMemberAccountMapper imuMemberAccountMapper;

    @Autowired
    private ImuMemberAccountService imuMemberAccountService;

    @Autowired
    private PictureService pictureService;
    /**
    * @description:   chat @ Open ended conversation with an AI assistant. @ 与 AI 助手进行开放式对话。
    * @param: [chatGptChatWithAiForm]
    * @return: com.lanhu.chatgpt.gateway.client.core.PcsResult
    * @author: liuyi
    * @date: 1:09 下午 2023/6/26 
    */
    public PcsResult<List<ChatCompletionChoice>> chat(ImuMemberUser imuMemberUser, ChatGptChatForm chatGptChatForm){

        AiChatgptCategory chatgptCategory =  chatgptCategoryMapper.selectById(chatGptChatForm.getCategoryId());

        if(chatgptCategory == null || EffectEnum.YES_EFFECT.getCode() != chatgptCategory.getIsEffect()){
             return Result.error(PcsResultCode.CHATGPT_CATEGORY_EMPTY);
        }

        ImuMemberAccount imuMemberAccount = imuMemberAccountMapper.selectByUserId(imuMemberUser.getId());
        //请先充值
        if(imuMemberAccount == null){
            return Result.error(PcsResultCode.CHATGPT_SERVICE_NEED_RECHARGE);
        }

        //请先充值
        if(chatgptCategory.getCost()!=null && chatgptCategory.getCost().compareTo(BigDecimal.ZERO) >0 && chatgptCategory.getCost().compareTo(imuMemberAccount.getAvailablePoint())>0){
            return Result.error(PcsResultCode.CHATGPT_SERVICE_NOT_BALANCE);
        }


        List<ChatMessage> messages = new ArrayList<>();

        //设置消息类型
        ChatMessage systemMessage = new ChatMessage(ChatMessageRole.SYSTEM.value(),chatgptCategory.getChatgptContext().trim());
        messages.add(systemMessage);

        if(CollectionUtil.isNotEmpty(chatGptChatForm.getMessages())){

            for (RoleMessage roleMessage : chatGptChatForm.getMessages()) {

                if(ChatMessageRole.USER.value().equalsIgnoreCase(roleMessage.getRole()) || ChatMessageRole.ASSISTANT.value().equalsIgnoreCase(roleMessage.getRole())){
                    ChatMessage message = new ChatMessage(roleMessage.getRole(),roleMessage.getContent());
                    messages.add(message);
                }

            }

        }




        String model = ChatGptModelEnum.gpt_35_turbo.getCode();

        //如果指定为gpt4话，则为gpt4，其他情况则为 chatgpt3.5
        if(ChatGptModelEnum.gpt_4.getCode().equalsIgnoreCase(chatGptChatForm.getModel())){
            model = ChatGptModelEnum.gpt_4.getCode();
        }

        Double temperature = new Double("0");

        if(chatGptChatForm.getTemperature() != null){
            temperature = chatGptChatForm.getTemperature();
        }else {
            if(chatgptCategory.getChatgptTemperature() != null){
                temperature = chatgptCategory.getChatgptTemperature();
            }

        }


        Double topP = chatgptCategory.getChatgptTopP() != null?chatgptCategory.getChatgptTopP():new Double("1");


        Double chatgptFrequencyPenalty = chatgptCategory.getChatgptFrequencyPenalty() != null?chatgptCategory.getChatgptFrequencyPenalty():new Double("0.0");


        Double chatgptPresencePenalty = chatgptCategory.getChatgptPresencePenalty() != null?chatgptCategory.getChatgptPresencePenalty():new Double("0.0");

        ChatCompletionRequest request = ChatCompletionRequest.builder()
                .model(model)
                .messages(messages)
                .temperature(temperature)
                //  .maxTokens(2000)
                .topP(topP)
                .frequencyPenalty(chatgptFrequencyPenalty)
                .presencePenalty(chatgptPresencePenalty)
                //.stop(Lists.newArrayList(" Human:"," AI:"))
                .build();


        if(chatgptCategory.getChatgptMaxTokens() != null){
            request.setMaxTokens(chatgptCategory.getChatgptMaxTokens());
        }

        if(StringUtils.isNotBlank(chatgptCategory.getChatgptStop())){

            List<String> stopList = Arrays.asList(chatgptCategory.getChatgptStop().split(ProjectConstant.COMMA));

            if(CollectionUtil.isNotEmpty(stopList)){
                request.setStop(stopList);
            }


        }


        List<ChatCompletionChoice>  completionChoiceList =  chatGptCloudService.chatCompletion(request);



        //扣款
        if(chatgptCategory.getCost()!=null && chatgptCategory.getCost().compareTo(BigDecimal.ZERO) > 0){
            imuMemberAccountService.chatgptServiceReduce(imuMemberUser,chatgptCategory.getCost(),"CHAT".concat(ProjectConstant.COMMA).concat(String.valueOf(chatgptCategory.getId())));
        }

        return Result.ok(completionChoiceList);


    }



    /**
     * @description:  问题咨询
     * @param: [chatGptChatWithAiForm]
     * @return: com.lanhu.chatgpt.gateway.client.core.PcsResult
     * @author: liuyi
     * @date: 1:09 下午 2023/6/26
     */
    public PcsResult<List<ChatCompletionChoice>> qa(ImuMemberUser imuMemberUser, ChatGptQaForm chatGptChatForm){

        AiChatgptCategory chatgptCategory =  chatgptCategoryMapper.selectById(chatGptChatForm.getCategoryId());

        if(chatgptCategory == null || EffectEnum.YES_EFFECT.getCode() != chatgptCategory.getIsEffect()){
            return Result.error(PcsResultCode.CHATGPT_CATEGORY_EMPTY);
        }

        ImuMemberAccount imuMemberAccount = imuMemberAccountMapper.selectByUserId(imuMemberUser.getId());
        //请先充值
        if(imuMemberAccount == null){
            return Result.error(PcsResultCode.CHATGPT_SERVICE_NEED_RECHARGE);
        }

        //请先充值
        if(chatgptCategory.getCost()!=null && chatgptCategory.getCost().compareTo(BigDecimal.ZERO) >0 && chatgptCategory.getCost().compareTo(imuMemberAccount.getAvailablePoint())>0){
            return Result.error(PcsResultCode.CHATGPT_SERVICE_NOT_BALANCE);
        }


        List<ChatMessage> messages = new ArrayList<>();

        //设置消息类型
        ChatMessage systemMessage = new ChatMessage(ChatMessageRole.SYSTEM.value(),chatgptCategory.getChatgptContext().trim());
        messages.add(systemMessage);

        String message = chatgptCategory.getStart();

        if(StringUtils.isNotBlank(message)){
            message = message.concat( chatGptChatForm.getMessage());
        }else {
            message = chatGptChatForm.getMessage();
        }

        ChatMessage userMessage = new ChatMessage(ChatMessageRole.USER.value(),message);


        messages.add(userMessage);


        String model = ChatGptModelEnum.gpt_35_turbo.getCode();

        //如果指定为gpt4话，则为gpt4，其他情况则为 chatgpt3.5
        if(ChatGptModelEnum.gpt_4.getCode().equalsIgnoreCase(chatGptChatForm.getModel())){
            model = ChatGptModelEnum.gpt_4.getCode();
        }

        Double temperature = new Double("0");

        if(chatGptChatForm.getTemperature() != null){
            temperature = chatGptChatForm.getTemperature();
        }else {
            if(chatgptCategory.getChatgptTemperature() != null){
                temperature = chatgptCategory.getChatgptTemperature();
            }

        }


        Double topP = chatgptCategory.getChatgptTopP() != null?chatgptCategory.getChatgptTopP():new Double("1");


        Double chatgptFrequencyPenalty = chatgptCategory.getChatgptFrequencyPenalty() != null?chatgptCategory.getChatgptFrequencyPenalty():new Double("0.0");


        Double chatgptPresencePenalty = chatgptCategory.getChatgptPresencePenalty() != null?chatgptCategory.getChatgptPresencePenalty():new Double("0.0");

        ChatCompletionRequest request = ChatCompletionRequest.builder()
                .model(model)
                .messages(messages)
                .temperature(temperature)
                //  .maxTokens(2000)
                .topP(topP)
                .frequencyPenalty(chatgptFrequencyPenalty)
                .presencePenalty(chatgptPresencePenalty)
                //.stop(Lists.newArrayList(" Human:"," AI:"))
                .build();


        if(chatgptCategory.getChatgptMaxTokens() != null){
            request.setMaxTokens(chatgptCategory.getChatgptMaxTokens());
        }

        if(StringUtils.isNotBlank(chatgptCategory.getChatgptStop())){

            List<String> stopList = Arrays.asList(chatgptCategory.getChatgptStop().split(ProjectConstant.COMMA));

            if(CollectionUtil.isNotEmpty(stopList)){
                request.setStop(stopList);
            }


        }


        List<ChatCompletionChoice>  completionChoiceList =  chatGptCloudService.chatCompletion(request);



        //扣款
        if(chatgptCategory.getCost()!=null && chatgptCategory.getCost().compareTo(BigDecimal.ZERO) > 0){
            imuMemberAccountService.chatgptServiceReduce(imuMemberUser,chatgptCategory.getCost(),"QA".concat(ProjectConstant.COMMA).concat(String.valueOf(chatgptCategory.getId())));
        }

        return Result.ok(completionChoiceList);


    }





    /**
     * @description:   图片生成
     * @param: [chatGptChatWithAiForm]
     * @return: com.lanhu.chatgpt.gateway.client.core.PcsResult
     * @author: liuyi
     * @date: 1:09 下午 2023/6/26
     */
    public PcsResult<String> createImage(ImuMemberUser imuMemberUser, ChatGptCreateImageForm chatGptCreateImageForm){

        AiChatgptCategory chatgptCategory =  chatgptCategoryMapper.selectById(chatGptCreateImageForm.getCategoryId());

        if(chatgptCategory == null || EffectEnum.YES_EFFECT.getCode() != chatgptCategory.getIsEffect()){
            return Result.error(PcsResultCode.CHATGPT_CATEGORY_EMPTY);
        }

        ImuMemberAccount imuMemberAccount = imuMemberAccountMapper.selectByUserId(imuMemberUser.getId());
        //请先充值
        if(imuMemberAccount == null){
            return Result.error(PcsResultCode.CHATGPT_SERVICE_NEED_RECHARGE);
        }

        final Integer n = chatGptCreateImageForm.getN() != null?chatGptCreateImageForm.getN():chatgptCategory.getChatgptN();

        //消费总价
        final  BigDecimal total =  chatgptCategory.getCost() != null? chatgptCategory.getCost().multiply(new BigDecimal(n)):BigDecimal.ZERO;


        //请先充值
        if(chatgptCategory.getCost()!=null && total.compareTo(BigDecimal.ZERO) >0 && total.compareTo(imuMemberAccount.getAvailablePoint())>0){
            return Result.error(PcsResultCode.CHATGPT_SERVICE_NOT_BALANCE);
        }


        //用户是否存在上一次请求

        AiActionRequest reqeust =  aiActionRequestMapper.isExistHanderingReqeust(imuMemberUser.getId(), openApiConfig.getExpireTime());

        if(reqeust != null){
            return Result.error(PcsResultCode.CHATGPT_EXIST_TASK_HANDING,openApiConfig.getExpireTime());
        }

        final AiImageTemplate aiImageTemplate  = aiImageTemplateMapper.selectById(chatGptCreateImageForm.getTemplateId());

        if(aiImageTemplate == null){

            return Result.error(PcsResultCode.TEMPLATE_NOT_EXIST);

        }

        if(StringUtils.isBlank(aiImageTemplate.getPrompt())){
            return Result.error(PcsResultCode.TEMPLATE_PROMPT_NOT_EMPTY);
        }



        final  Long requestId = IdUtil.getSnowflakeNextId();





       final String size =  StringUtils.isNotBlank(chatGptCreateImageForm.getSize())?chatGptCreateImageForm.getSize():chatgptCategory.getChatgptImageSize();


        //会员判断
        AiActionRequest aiActionRequest = AiActionRequest.builder()
                .categoryId(chatGptCreateImageForm.getCategoryId())
                .chatgptN(n)
                .chatgptImageSize(size)
                .chatgptImageResponseFormat(chatgptCategory.getChatgptImageResponseFormat().trim())
                .prompt(aiImageTemplate.getPrompt().concat(ProjectConstant.WRAP).concat(chatGptCreateImageForm.getPrompt()).concat(ProjectConstant.WRAP))
                .status(AiActionRequestStatusEnum.HANDLERING.getCode())
                .createTime(new Date())
                .userId(imuMemberUser.getId())
                .isEffect(EffectEnum.YES_EFFECT.getCode())
                .requestId(requestId)
                .build();




        aiActionRequestMapper.insert(aiActionRequest);





        //异步处理
        asyncExecutor.submit(() -> {

            CreateImageRequest request = CreateImageRequest.builder()
                    .prompt(aiImageTemplate.getPrompt().concat(ProjectConstant.WRAP).concat(chatGptCreateImageForm.getPrompt()).concat(ProjectConstant.WRAP))
                    .n(n)
                    .size(size)
                    .responseFormat(chatgptCategory.getChatgptImageResponseFormat().trim())
                    .build();


            List<Image>  imageList =  chatGptCloudService.createImage(request);



            List<String> urls = Lists.newArrayList();


            if(CollectionUtil.isNotEmpty(imageList)){


                for (Image image : imageList) {


                    //如果是base64直接上传到我们的oss
//                    if(ChatGptResponseFormatEnum.b64_json.getCode().equalsIgnoreCase(chatgptCategory.getChatgptImageResponseFormat().trim())){
//
//                        String url =  aliYunOssHelper.uploadBaseImage2Oss(image.getB64Json());
//
//                        if(StringUtils.isNotBlank(url)){
//                            urls.add(url);
//                        }
//
//
//                    }else {
//
//                        if(StringUtils.isNotBlank(image.getUrl())){
//                            urls.add(image.getUrl());
//                        }
//                    }


                    String url = pictureService.scale(image.getUrl(),chatGptCreateImageForm.getScale());

                    if(StringUtils.isNotBlank(url)){
                        urls.add(url);
                    }


                }



            }

            int stauts = AiActionRequestStatusEnum.SUCCESS.getCode();


            if(CollectionUtil.isEmpty(urls)){
                stauts =  AiActionRequestStatusEnum.FAIL.getCode();
            }



            AiActionRequest update = AiActionRequest.builder()
                    .requestId(requestId)
                    .updateTime(new Date())
                    .status(stauts)
                    .result(String.join(ProjectConstant.COMMA,urls))
                    .build();

            aiActionRequestMapper.updateById(update);



            //扣款
            if(chatgptCategory.getCost()!=null && total.compareTo(BigDecimal.ZERO) > 0){
                imuMemberAccountService.chatgptServiceReduce(imuMemberUser,total,"createImage".concat(ProjectConstant.COMMA).concat(String.valueOf(chatgptCategory.getId())));
            }


        });






        return Result.ok(String.valueOf(requestId));


    }



    /**
     * @description:   图片变体生成
     * @param: [chatGptChatWithAiForm]
     * @return: com.lanhu.chatgpt.gateway.client.core.PcsResult
     * @author: liuyi
     * @date: 1:09 下午 2023/6/26
     */
    public PcsResult<String> createImageVariation(ImuMemberUser imuMemberUser, ChatGptCreateImageVariationForm chatGptCreateImageForm){

        AiChatgptCategory chatgptCategory =  chatgptCategoryMapper.selectById(chatGptCreateImageForm.getCategoryId());

        if(chatgptCategory == null || EffectEnum.YES_EFFECT.getCode() != chatgptCategory.getIsEffect()){
            return Result.error(PcsResultCode.CHATGPT_CATEGORY_EMPTY);
        }

        ImuMemberAccount imuMemberAccount = imuMemberAccountMapper.selectByUserId(imuMemberUser.getId());
        //请先充值
        if(imuMemberAccount == null){
            return Result.error(PcsResultCode.CHATGPT_SERVICE_NEED_RECHARGE);
        }

        final Integer n = chatGptCreateImageForm.getN() != null?chatGptCreateImageForm.getN():chatgptCategory.getChatgptN();


        //消费总价
        final  BigDecimal total =  chatgptCategory.getCost() != null? chatgptCategory.getCost().multiply(new BigDecimal(n)):BigDecimal.ZERO;

        //请先充值
        if(chatgptCategory.getCost()!=null && total.compareTo(BigDecimal.ZERO) >0 && total.compareTo(imuMemberAccount.getAvailablePoint())>0){
            return Result.error(PcsResultCode.CHATGPT_SERVICE_NOT_BALANCE);
        }

        //用户是否存在上一次请求

        AiActionRequest reqeust =  aiActionRequestMapper.isExistHanderingReqeust(imuMemberUser.getId(),openApiConfig.getExpireTime());

        if(reqeust != null){
            return Result.error(PcsResultCode.CHATGPT_EXIST_TASK_HANDING,openApiConfig.getExpireTime());
        }

        FileUploadProperties.Config config = null;
        List<FileUploadProperties.Config> configList = fileUploadProperties.getConfigs();
        for (FileUploadProperties.Config tempConfig : configList) {
            if (StringUtils.equals(tempConfig.getFileType(), UploadTypeEnum.local_store_image.getCode())) {
                config = tempConfig;
                break;
            }
        }


        final File file = FileUtil.createFileByWeb(chatGptCreateImageForm.getImageUrl(),ProjectConstant.CHATGPT_IMAGE_TYPE,config.getUploadDir(),false);

        if(file == null){
            return Result.error(PcsResultCode.CHATGPT_IMAGE_DOWNLOAD_ERROR);
        }

        //文件超过4M
        if(file.length()>config.getFileMax()){
            return Result.error(PcsResultCode.UPLOAD_OVER_LIMIT);
        }


        //格式不对
        if(!chatGptCreateImageForm.getImageUrl().contains(ProjectConstant.CHATGPT_IMAGE_TYPE) && !chatGptCreateImageForm.getImageUrl().contains(ProjectConstant.CHATGPT_IMAGE_TYPE_CASE)){
            return Result.error(PcsResultCode.UPLOAD_FILE_SUF_ERROR);
        }



        final  Long requestId = IdUtil.getSnowflakeNextId();




        final String size =  StringUtils.isNotBlank(chatGptCreateImageForm.getSize())?chatGptCreateImageForm.getSize():chatgptCategory.getChatgptImageSize();







        //会员判断
        AiActionRequest aiActionRequest = AiActionRequest.builder()
                .categoryId(chatGptCreateImageForm.getCategoryId())
                .chatgptN(n)
                .prompt(chatGptCreateImageForm.getImageUrl())
                .chatgptImageSize(size)
                .chatgptImageResponseFormat(chatgptCategory.getChatgptImageResponseFormat().trim())
                .status(AiActionRequestStatusEnum.HANDLERING.getCode())
                .createTime(new Date())
                .userId(imuMemberUser.getId())
                .isEffect(EffectEnum.YES_EFFECT.getCode())
                .requestId(requestId)
                .build();



        aiActionRequestMapper.insert(aiActionRequest);





        //异步处理
        asyncExecutor.submit(() -> {

            CreateImageVariationRequest request = CreateImageVariationRequest.builder()
                    .n(n)
                    .size(size)
                    .responseFormat(chatgptCategory.getChatgptImageResponseFormat().trim())
                    .build();


            List<Image>  imageList =  chatGptCloudService.createImageVariation(request,file);



            List<String> urls = Lists.newArrayList();


            if(CollectionUtil.isNotEmpty(imageList)){


                for (Image image : imageList) {


                    //如果是base64直接上传到我们的oss
//                    if(ChatGptResponseFormatEnum.b64_json.getCode().equalsIgnoreCase(chatgptCategory.getChatgptImageResponseFormat().trim())){
//
//                        String url =  aliYunOssHelper.uploadBaseImage2Oss(image.getB64Json());
//
//                        if(StringUtils.isNotBlank(url)){
//                            urls.add(url);
//                        }
//
//
//                    }else {
//
//                        if(StringUtils.isNotBlank(image.getUrl())){
//                            urls.add(image.getUrl());
//                        }
//                    }

                    String url = pictureService.scale(image.getUrl(),chatGptCreateImageForm.getScale());

                    if(StringUtils.isNotBlank(url)){
                        urls.add(url);
                    }


                }



            }

            int stauts = AiActionRequestStatusEnum.SUCCESS.getCode();


            if(CollectionUtil.isEmpty(urls)){
                stauts =  AiActionRequestStatusEnum.FAIL.getCode();
            }



            AiActionRequest update = AiActionRequest.builder()
                    .requestId(requestId)
                    .updateTime(new Date())
                    .status(stauts)
                    .result(String.join(ProjectConstant.COMMA,urls))
                    .build();

            aiActionRequestMapper.updateById(update);

            if(file != null){
                file.delete();
            }

            //扣款
            if(chatgptCategory.getCost()!=null && total.compareTo(BigDecimal.ZERO) > 0){
                imuMemberAccountService.chatgptServiceReduce(imuMemberUser,total,"createImageVariation".concat(ProjectConstant.COMMA).concat(String.valueOf(chatgptCategory.getId())));
            }


        });






        return Result.ok(String.valueOf(requestId));


    }

    /**
     * @description:   图片编辑
     * @param: [chatGptChatWithAiForm]
     * @return: com.lanhu.chatgpt.gateway.client.core.PcsResult
     * @author: liuyi
     * @date: 1:09 下午 2023/6/26
     */
    public PcsResult<String> createImageEdit(ImuMemberUser imuMemberUser, ChatGptCreateImageEditForm chatGptCreateImageForm){

        AiChatgptCategory chatgptCategory =  chatgptCategoryMapper.selectById(chatGptCreateImageForm.getCategoryId());

        if(chatgptCategory == null || EffectEnum.YES_EFFECT.getCode() != chatgptCategory.getIsEffect()){
            return Result.error(PcsResultCode.CHATGPT_CATEGORY_EMPTY);
        }

        ImuMemberAccount imuMemberAccount = imuMemberAccountMapper.selectByUserId(imuMemberUser.getId());
        //请先充值
        if(imuMemberAccount == null){
            return Result.error(PcsResultCode.CHATGPT_SERVICE_NEED_RECHARGE);
        }



        final Integer n = chatGptCreateImageForm.getN() != null?chatGptCreateImageForm.getN():chatgptCategory.getChatgptN();


        //消费总价
        final  BigDecimal total =  chatgptCategory.getCost() != null? chatgptCategory.getCost().multiply(new BigDecimal(n)):BigDecimal.ZERO;


        //请先充值
        if(chatgptCategory.getCost()!=null && total.compareTo(BigDecimal.ZERO) >0 && total.compareTo(imuMemberAccount.getAvailablePoint())>0){
            return Result.error(PcsResultCode.CHATGPT_SERVICE_NOT_BALANCE);
        }


        //用户是否存在上一次请求

        AiActionRequest reqeust =  aiActionRequestMapper.isExistHanderingReqeust(imuMemberUser.getId(),openApiConfig.getExpireTime());

        if(reqeust != null){
            return Result.error(PcsResultCode.CHATGPT_EXIST_TASK_HANDING,openApiConfig.getExpireTime());
        }


       final AiImageTemplate aiImageTemplate  = aiImageTemplateMapper.selectById(chatGptCreateImageForm.getTemplateId());

        if(aiImageTemplate == null){

            return Result.error(PcsResultCode.TEMPLATE_NOT_EXIST);

        }

        if(StringUtils.isBlank(aiImageTemplate.getPrompt())){
            return Result.error(PcsResultCode.TEMPLATE_PROMPT_NOT_EMPTY);
        }

        FileUploadProperties.Config config = null;
        List<FileUploadProperties.Config> configList = fileUploadProperties.getConfigs();
        for (FileUploadProperties.Config tempConfig : configList) {
            if (StringUtils.equals(tempConfig.getFileType(), UploadTypeEnum.local_store_image.getCode())) {
                config = tempConfig;
                break;
            }
        }


        final File file = FileUtil.createFileByWeb(chatGptCreateImageForm.getImageUrl(),ProjectConstant.CHATGPT_IMAGE_TYPE,config.getUploadDir(),true);

        if(file == null){
            return Result.error(PcsResultCode.CHATGPT_IMAGE_DOWNLOAD_ERROR);
        }

        //文件超过4M
        if(file.length()>config.getFileMax()){
            return Result.error(PcsResultCode.UPLOAD_OVER_LIMIT);
        }


        //格式不对
        if(!chatGptCreateImageForm.getImageUrl().contains(ProjectConstant.CHATGPT_IMAGE_TYPE) && !chatGptCreateImageForm.getImageUrl().contains(ProjectConstant.CHATGPT_IMAGE_TYPE_CASE)){
            return Result.error(PcsResultCode.UPLOAD_FILE_SUF_ERROR);
        }



        final  Long requestId = IdUtil.getSnowflakeNextId();




        final String size =  StringUtils.isNotBlank(chatGptCreateImageForm.getSize())?chatGptCreateImageForm.getSize():chatgptCategory.getChatgptImageSize();




        //会员判断
        AiActionRequest aiActionRequest = AiActionRequest.builder()
                .categoryId(chatGptCreateImageForm.getCategoryId())
                .chatgptN(n)
                .prompt(chatGptCreateImageForm.getImageUrl().concat(ProjectConstant.COMMA).concat(aiImageTemplate.getPrompt()))
                .chatgptImageSize(size)
                .chatgptImageResponseFormat(chatgptCategory.getChatgptImageResponseFormat().trim())
                .status(AiActionRequestStatusEnum.HANDLERING.getCode())
                .createTime(new Date())
                .userId(imuMemberUser.getId())
                .isEffect(EffectEnum.YES_EFFECT.getCode())
                .requestId(requestId)
                .build();



        aiActionRequestMapper.insert(aiActionRequest);





        //异步处理
        asyncExecutor.submit(() -> {

            CreateImageEditRequest request = CreateImageEditRequest.builder()
                    .n(n)
                    .size(size)
                    .responseFormat(chatgptCategory.getChatgptImageResponseFormat().trim())
                    .prompt(aiImageTemplate.getPrompt())
                    .build();


            List<Image>  imageList =  chatGptCloudService.createImageEdit(request,file,null);



            List<String> urls = Lists.newArrayList();


            if(CollectionUtil.isNotEmpty(imageList)){


                for (Image image : imageList) {


                    //如果是base64直接上传到我们的oss
//                    if(ChatGptResponseFormatEnum.b64_json.getCode().equalsIgnoreCase(chatgptCategory.getChatgptImageResponseFormat().trim())){
//
//                        String url =  aliYunOssHelper.uploadBaseImage2Oss(image.getB64Json());
//
//                        if(StringUtils.isNotBlank(url)){
//                            urls.add(url);
//                        }
//
//
//                    }else {
//
//                        if(StringUtils.isNotBlank(image.getUrl())){
//                            urls.add(image.getUrl());
//                        }
//                    }

                    String url = pictureService.scale(image.getUrl(),chatGptCreateImageForm.getScale());

                    if(StringUtils.isNotBlank(url)){
                        urls.add(url);
                    }


                }



            }

            int stauts = AiActionRequestStatusEnum.SUCCESS.getCode();


            if(CollectionUtil.isEmpty(urls)){
                stauts =  AiActionRequestStatusEnum.FAIL.getCode();
            }



            AiActionRequest update = AiActionRequest.builder()
                    .requestId(requestId)
                    .updateTime(new Date())
                    .status(stauts)
                    .result(String.join(ProjectConstant.COMMA,urls))
                    .build();

            aiActionRequestMapper.updateById(update);


            if(file != null){
                file.delete();
            }

            //扣款
            if(chatgptCategory.getCost()!=null && total.compareTo(BigDecimal.ZERO) > 0){
                imuMemberAccountService.chatgptServiceReduce(imuMemberUser,total,"createImageEdit".concat(ProjectConstant.COMMA).concat(String.valueOf(chatgptCategory.getId())));
            }
        });






        return Result.ok(String.valueOf(requestId));


    }

}
