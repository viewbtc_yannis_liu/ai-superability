package com.lanhu.ai.gateway.client.service;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lanhu.ai.gateway.client.config.OpenApiConfig;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.core.Result;
import com.lanhu.ai.gateway.client.enums.AiActionRequestStatusEnum;
import com.lanhu.ai.gateway.client.enums.EffectEnum;
import com.lanhu.ai.gateway.client.mapper.AiActionRequestMapper;
import com.lanhu.ai.gateway.client.mapper.AiImageTemplateMapper;
import com.lanhu.ai.gateway.client.model.AiActionRequest;
import com.lanhu.ai.gateway.client.model.AiImageTemplate;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.vo.req.AiActionRequestDetailForm;
import com.lanhu.ai.gateway.client.vo.req.AiActionRequestListPageForm;
import com.lanhu.ai.gateway.client.vo.req.AiImageTemplateListPageForm;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author liuyi
 * @version 1.0
 * @description: TODO
 * @date 2023/6/27 12:16 下午
 */
@Service
@Slf4j
public class AiActionRequestService {

    @Autowired
    private AiActionRequestMapper aiActionRequestMapper;

    @Autowired
    private OpenApiConfig openApiConfig;

    /**
     * @description: 分页查询
     * @param: [plaPcUser, request]
     * @return: com.lanhu.imenu.gateway.pc.core.PcsResult
     * @author: liuyi
     * @date: 8:21 下午 2023/1/15
     */
    public PcsResult<IPage<AiActionRequest>> listPage(ImuMemberUser imuMemberUser, AiActionRequestListPageForm request) {

        LambdaQueryWrapper<AiActionRequest> wrapper = Wrappers.lambdaQuery();


        wrapper.eq(AiActionRequest::getUserId,imuMemberUser.getId());

        if(StringUtils.isNotBlank(request.getStart())){
            wrapper.gt(AiActionRequest::getCreateTime, DateUtil.beginOfDay(DateUtil.parse(request.getStart())));
        }


        if(StringUtils.isNotBlank(request.getEnd())){
            wrapper.lt(AiActionRequest::getCreateTime, DateUtil.endOfDay(DateUtil.parse(request.getEnd())));
        }




        wrapper.eq(AiActionRequest::getIsEffect, EffectEnum.YES_EFFECT.getCode());


        // 默认根据addTime 倒序排列
        wrapper.orderByDesc(AiActionRequest::getCreateTime);

        Page<AiActionRequest> page = new Page<>(request.getPageIndex(), request.getPageSize());


        IPage<AiActionRequest> pageList = aiActionRequestMapper.selectPage(page, wrapper);


        if(CollectionUtil.isNotEmpty(pageList.getRecords())){

            for (AiActionRequest aiActionRequest : pageList.getRecords()) {
                //如果超过时间,则认为失败
                if(DateUtil.offsetMinute(aiActionRequest.getCreateTime(),openApiConfig.getExpireTime()).getTime() > System.currentTimeMillis() ){
                    aiActionRequest.setStatus(AiActionRequestStatusEnum.FAIL.getCode());
                }
            }
        }



        return Result.ok(pageList);

    }



    /**
     * @description: 详情
     * @param: [plaPcUser, request]
     * @return: com.lanhu.imenu.gateway.pc.core.PcsResult
     * @author: liuyi
     * @date: 8:21 下午 2023/1/15
     */
    public PcsResult<AiActionRequest> detail(ImuMemberUser imuMemberUser, AiActionRequestDetailForm request) {


        AiActionRequest aiActionRequest = aiActionRequestMapper.selectById(request.getRequestId());

        if(aiActionRequest == null || !aiActionRequest.getUserId().equals(imuMemberUser.getId())){
            return Result.ok();
        }

        if(aiActionRequest.getStatus() == AiActionRequestStatusEnum.WAIT_HANDLER.getCode() || aiActionRequest.getStatus() == AiActionRequestStatusEnum.HANDLERING.getCode()  ){

            //如果超过时间,则认为失败
            if(DateUtil.offsetMinute(aiActionRequest.getCreateTime(),openApiConfig.getExpireTime()).getTime() > System.currentTimeMillis() ){
                aiActionRequest.setStatus(AiActionRequestStatusEnum.FAIL.getCode());
            }

        }


        return Result.ok(aiActionRequest);

    }



}
