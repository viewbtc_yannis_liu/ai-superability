package com.lanhu.ai.gateway.client.controller;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import com.lanhu.ai.gateway.client.annotation.IgnoreSecurity;
import com.lanhu.ai.gateway.client.service.MemberUserService;
import com.lanhu.ai.gateway.client.core.BaseController;
import com.lanhu.ai.gateway.client.core.PcsResult;
import com.lanhu.ai.gateway.client.core.ResponseResult;
import com.lanhu.ai.gateway.client.model.ImuMemberUser;
import com.lanhu.ai.gateway.client.vo.req.*;
import com.lanhu.ai.gateway.client.vo.resp.MemberUserInfoVo;
import com.lanhu.ai.gateway.client.vo.resp.MemberUserLoginVo;
import com.lanhu.ai.gateway.client.vo.resp.MemberUserWxAuthorizationNewVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


@Slf4j
@Api(tags = "用户相关")
@RestController
public class MemberUserController extends BaseController {
    @Resource
    private MemberUserService memberUserService;

    /**
     * 微信授权
     *
     * @param memberUserWxAuthorizationForm 微信授权请求对象
     * @return 微信授权结果
     */
    /*@ApiOperation(value = "微信授权" , notes = "第一步授权")
    @IgnoreSecurity
    @PostMapping("/user/wx/authorization")
    public PcsResult<MemberUserWxAuthorizationVo> wxAuthorization(@Validated @RequestBody MemberUserWxAuthorizationForm memberUserWxAuthorizationForm) {
        PcsResult<MemberUserWxAuthorizationVo> result = memberUserService.wxAuthorization(memberUserWxAuthorizationForm);
        return result;
    }*/


    @ApiOperation(value = "微信授权" , notes = "第一步授权（返回登录身份信息 (有值用户存在登录成功; 否则需要授权注册用户信息) ）")
    @IgnoreSecurity
    @PostMapping("/user/wx/authorization")
    public PcsResult<MemberUserWxAuthorizationNewVo> wxAuthorization(@Validated @RequestBody MemberUserWxAuthorizationForm memberUserWxAuthorizationForm) {
        log.info("MemberUserController wxAuthorization memberUserWxAuthorizationForm:{}" , memberUserWxAuthorizationForm );
        PcsResult<MemberUserWxAuthorizationNewVo> result = memberUserService.wxAuthorizationV2(memberUserWxAuthorizationForm);
        return result;
    }



    /**
     * 微信注册
     *
     * @param memberUserWxRegisterForm 微信注册请求对象
     * @return 微信注册结果
     */
    @ApiOperation(value = "微信注册" , notes = "第二步用户不存在，授权注册流程")
    @IgnoreSecurity
    @PostMapping("/user/wx/register")
    public PcsResult<MemberUserLoginVo> wxRegister(@Validated @RequestBody MemberUserWxRegisterForm memberUserWxRegisterForm) {
        log.info("MemberUserController wxRegister memberUserWxRegisterForm:{}" , memberUserWxRegisterForm );
        MemberUserLoginVo memberUserLoginVo = memberUserService.wxRegister(memberUserWxRegisterForm);
        return ResponseResult.ok(memberUserLoginVo);
    }

    /**
     * 微信登录
     *
     * @param memberUserWxLoginForm 微信登录请求对象
     * @return 微信登录结果
     */
    @ApiOperation(value = "微信登录" )
    @IgnoreSecurity
    @PostMapping("/user/wx/login")
    public PcsResult<MemberUserLoginVo> wxLogin(@Validated @RequestBody MemberUserWxLoginForm memberUserWxLoginForm) {
        log.info("MemberUserController wxLogin memberUserWxLoginForm:{}" , memberUserWxLoginForm );
        MemberUserLoginVo memberUserLoginVo = memberUserService.wxLogin(memberUserWxLoginForm.getOpenid());
        return ResponseResult.ok(memberUserLoginVo);
    }




    @ApiOperation(value = "授权解析手机号" , notes = "授权解析手机号")
    @IgnoreSecurity
    @PostMapping("/user/wx/getNewPhoneNoInfo")
    public PcsResult<WxMaPhoneNumberInfo> wxGetNewPhoneNoInfo(@Validated @RequestBody MemberUserWxNewPhoneForm form) {
        log.info("MemberUserController wxGetNewPhoneNoInfo form:{}" , form );
        WxMaPhoneNumberInfo phoneNumberInfo = memberUserService.wxGetNewPhoneNoInfo(form);
        return ResponseResult.ok(phoneNumberInfo);
    }



    /*
    @deprecated
    @ApiOperation(value = "授权解析手机号-老版" , notes = "授权解析手机号-老版")
    @IgnoreSecurity
    @PostMapping("/user/wx/getPhoneNoInfo")
    public PcsResult<String> wxGetPhoneNoInfo(@Validated @RequestBody MemberUserWxPhoneForm form) {
        log.info("MemberUserController wxGetPhoneNoInfo form:{}" , form );
        String phone = memberUserService.wxGetPhoneNoInfo(form);
        return ResponseResult.ok(phone);
    }*/







    /**
     * 用户信息
     *
     * @return 用户信息
     */
    @ApiOperation(value = "用户信息")
    @PostMapping("/user/info")
    public PcsResult<MemberUserInfoVo> info(HttpServletRequest request) {
        ImuMemberUser imuMemberUser = this.currentUser(request);
        log.info("MemberUserController info imuMemberUser:{}" , imuMemberUser );
        MemberUserInfoVo memberUserInfoVo = memberUserService.info(imuMemberUser);
        return ResponseResult.ok(memberUserInfoVo);
    }


    @ApiOperation(value = "退出登录")
    @PostMapping("/user/logout")
    public PcsResult userLogout(HttpServletRequest request) {
        this.logout(request);
        return ResponseResult.ok();
    }




    /**
     * 用户修改头像
     */
    /*@ApiOperation(value = "用户修改头像")
    @PostMapping("/member/user/modify/headPortrait")
    public PcsResult<Void> modifyHeadPortrait(@Validated @RequestBody MemberUserModifyHeadPortraitForm memberUserModifyHeadPortraitForm, HttpServletRequest request) {
        ImuMemberUser imuMemberUser = this.currentUser(request);
        memberUserService.modifyHeadPortrait(memberUserModifyHeadPortraitForm, imuMemberUser);
        return ResponseResult.ok();
    }*/

    /**
     * 用户修改昵称
     */
    /*@ApiOperation(value = "用户修改昵称")
    @PostMapping("/member/user/modify/nick")
    public PcsResult<Void> modifyNick(@Validated @RequestBody MemberUserModifyNickForm memberUserModifyNickForm, HttpServletRequest request) {
        ImuMemberUser imuMemberUser = this.currentUser(request);
        memberUserService.modifyNick(memberUserModifyNickForm, imuMemberUser);
        return ResponseResult.ok();
    }*/

}
