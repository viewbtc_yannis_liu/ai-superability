package com.lanhu.ai.gateway.client.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * AI操作请求表
 */
@ApiModel(value = "AI操作请求表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ai_action_request")
public class AiActionRequest {
    /**
     * 请求ID
     */
    @TableId(value = "request_id", type = IdType.INPUT)
    @ApiModelProperty(value = "请求ID")
    private Long requestId;

    /**
     * 分类ID
     */
    @TableField(value = "category_id")
    @ApiModelProperty(value = "分类ID")
    private Integer categoryId;

    /**
     * chatgpt上下文
     */
    @TableField(value = "chatgpt_context")
    @ApiModelProperty(value = "chatgpt上下文")
    private String chatgptContext;

    /**
     * chatgpt温度
     */
    @TableField(value = "chatgpt_temperature")
    @ApiModelProperty(value = "chatgpt温度")
    private Double chatgptTemperature;

    /**
     * chatgpt模型
     */
    @TableField(value = "chatgpt_model")
    @ApiModelProperty(value = "chatgpt模型")
    private String chatgptModel;

    /**
     * chatgpt tokens
     */
    @TableField(value = "chatgpt_max_tokens")
    @ApiModelProperty(value = "chatgpt tokens")
    private Integer chatgptMaxTokens;

    /**
     * chatgpt top p
     */
    @TableField(value = "chatgpt_top_p")
    @ApiModelProperty(value = "chatgpt top p")
    private Double chatgptTopP;

    @TableField(value = "chatgpt_frequency_penalty")
    @ApiModelProperty(value = "")
    private Double chatgptFrequencyPenalty;

    @TableField(value = "chatgpt_presence_penalty")
    @ApiModelProperty(value = "")
    private Double chatgptPresencePenalty;

    @TableField(value = "chatgpt_stop")
    @ApiModelProperty(value = "")
    private String chatgptStop;

    /**
     * 返回数据数量
     */
    @TableField(value = "chatgpt_n")
    @ApiModelProperty(value = "返回数据数量")
    private Integer chatgptN;

    /**
     * 图片尺寸
     */
    @TableField(value = "chatgpt_image_size")
    @ApiModelProperty(value = "图片尺寸")
    private String chatgptImageSize;

    /**
     * 图片返回格式
     */
    @TableField(value = "chatgpt_image_response_format")
    @ApiModelProperty(value = "图片返回格式")
    private String chatgptImageResponseFormat;

    /**
     * 是否有效(0:有效 1删除)
     */
    @TableField(value = "is_effect")
    @ApiModelProperty(value = "是否有效(0:有效 1删除)")
    private Integer isEffect;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField(value = "update_time")
    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 状态(0:待处理 1:处理中 2: 成功 3: 失败)
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value = "状态(0:待处理 1:处理中 2: 成功 3: 失败)")
    private Integer status;

    /**
     * 处理结果
     */
    @TableField(value = "`result`")
    @ApiModelProperty(value = "处理结果")
    private String result;

    /**
     * 提示信息
     */
    @TableField(value = "prompt")
    @ApiModelProperty(value = "提示信息")
    private String prompt;

    /**
     * 费用
     */
    @TableField(value = "cost")
    @ApiModelProperty(value = "费用")
    private BigDecimal cost;

    /**
     * 费用类型(调用次数:1 ，积分:2 )
     */
    @TableField(value = "cost_type")
    @ApiModelProperty(value = "费用类型(调用次数:1 ，积分:2 )")
    private Integer costType;

    /**
     * 交易状态(0:待扣除 1: 扣除成功 2：扣除失败 3：已退款)
     */
    @TableField(value = "trade_status")
    @ApiModelProperty(value = "交易状态(0:待扣除 1: 扣除成功 2：扣除失败 3：已退款)")
    private Integer tradeStatus;

    /**
     * 交易时间
     */
    @TableField(value = "trade_time")
    @ApiModelProperty(value = "交易时间")
    private Date tradeTime;

    /**
     * 完成时间
     */
    @TableField(value = "completion_time")
    @ApiModelProperty(value = "完成时间")
    private Date completionTime;

    /**
     * 退款时间
     */
    @TableField(value = "refund_time")
    @ApiModelProperty(value = "退款时间")
    private Date refundTime;
}