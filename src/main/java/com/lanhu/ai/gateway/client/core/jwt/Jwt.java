package com.lanhu.ai.gateway.client.core.jwt;

import cn.hutool.core.util.ObjectUtil;
import com.lanhu.ai.gateway.client.core.BusinessException;
import com.lanhu.ai.gateway.client.core.PcsResultCode;
import com.lanhu.ai.gateway.client.enums.TokenStateEnum;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jose.shaded.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Jwt {
    /**
     * token有效时长
     */
    private static final long EFFECTIVE_TIME = 2 * 30 * 24 * 60 * 60 * 1000L;

    /**
     * ext
     */
    private static final String EXT = "ext";

    /**
     * 秘钥
     */
    private static final byte[] SECRET = "!ZNL9Iwpp3b$H!D#7F!WunM@53R%GsCgBoCMGCfyx8AU9D2ugF@pQgc2*I&E".getBytes();

    /**
     * 初始化head部分的数据为 { "alg":"HS256", "type":"JWT" }
     */
    private static final JWSHeader HEADER = new JWSHeader(JWSAlgorithm.HS256, JOSEObjectType.JWT, null, null, null,
            null, null, null, null, null, null, true, null, null);

    /**
     * 生成token，该方法只在用户登录成功后调用
     *
     * @return token字符串, 若失败则返回null
     */
    public static String createToken(Long userId) {
        if (ObjectUtil.isNull(userId)) {
            throw new BusinessException(PcsResultCode.MEMBER_USER_NOT_EXIST);
        }
        Map<String, Object> payload = new HashMap<>(3);
        Date date = new Date();
        // 用户id
        payload.put("userId", userId);
        // 生成时间
        payload.put("iat", date.getTime());
        // 过期时间两个月
        payload.put("ext", date.getTime() + EFFECTIVE_TIME);
        return Jwt.createToken(payload);
    }

    /**
     * 生成token，该方法只在用户登录成功后调用
     *
     * @param payload，可以存储用户id，token生成时间，token过期时间等自定义字段
     * @return token字符串, 若失败则返回null
     */
    private static String createToken(Map<String, Object> payload) {
        String tokenString = null;
        // 创建一个 JWS object
        JWSObject jwsObject = new JWSObject(HEADER, new Payload(new JSONObject(payload)));
        try {
            // 将jwsObject 进行HMAC签名
            jwsObject.sign(new MACSigner(SECRET));
            tokenString = jwsObject.serialize();
        } catch (JOSEException e) {
            System.err.println("签名失败:" + e.getMessage());
            e.printStackTrace();
        }
        return tokenString;
    }

    /**
     * token检验
     *
     * @param token token
     * @return 检验结果
     */
    public static JwtResult checkToken(String token) {

        Map<String, Object> map = validToken(token);

        return new JwtResult(map.get("state").toString(), map.get("data"));

    }

    /**
     * 校验token是否合法，返回Map集合,集合中主要包含 state状态码 data鉴权成功后从token中提取的数据
     * 该方法在过滤器中调用，每次请求API时都校验
     *
     * @param token token
     * @return Map<String, Object>
     */
    private static Map<String, Object> validToken(String token) {
        Map<String, Object> resultMap = new HashMap<>(2);
        try {
            JWSObject jwsObject = JWSObject.parse(token);
            Payload payload = jwsObject.getPayload();
            JWSVerifier verifier = new MACVerifier(SECRET);

            if (jwsObject.verify(verifier)) {
                Map<String, Object> jsonObject = payload.toJSONObject();
                // token校验成功（此时没有校验是否过期）
                resultMap.put("state", TokenStateEnum.VALID.toString());
                // 若payload包含ext字段，则校验是否过期
                if (jsonObject.containsKey(EXT)) {
                    long extTime = Long.parseLong(jsonObject.get(EXT).toString());
                    long curTime = System.currentTimeMillis();
                    // 过期了
                    if (curTime > extTime) {
                        resultMap.clear();
                        resultMap.put("state", TokenStateEnum.EXPIRED.toString());
                    }
                }
                resultMap.put("data", jsonObject);

            } else {
                // 校验失败
                resultMap.put("state", TokenStateEnum.INVALID.toString());
            }

        } catch (Exception e) {
            // token格式不合法导致的异常
            resultMap.clear();
            resultMap.put("state", TokenStateEnum.INVALID.toString());
        }
        return resultMap;
    }
}
