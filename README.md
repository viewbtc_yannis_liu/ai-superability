
**温馨提醒**

1. 本项目仅适用学习交流，并且开源基于Springboot Java Chatgpt后台代码<br/>
2. 目前正在进行基于流推模式会话，增强用户体验，敬请期待<br/>
3. 未来也会陆续推出基于Midjourney技术图片生成，优化项目中社区服务，增强提升用户间互动娱乐性。
4. 本项目不在任何平台出售,如有发现请积极举报<br/>


## 在线体验

- 1. 官方网站：https://www.51lanhu.com/
  
  如需交流或者合作意向，可在官网留言，期待与您的合作。


- 2. AI超能力微信小程序

 <div align=center>
    <td ><img height="" width="" src="https://lanhu-ai.oss-cn-beijing.aliyuncs.com/ai/image/2023-07-07/ai.jpeg"/></td>
 </div>

  3. 扫描微信小程序二维图，可通过微信账号进行注册登录。如图：

 <div align=center>
    <td ><img height="" width="" src="https://lanhu-ai.oss-cn-beijing.aliyuncs.com/ai/image/2023-07-07/3.jpeg"/></td>
    <td ></td>
    <td ></td>
 </div> 
 <div align=center>
    <td ><img height="" width="" src="https://lanhu-ai.oss-cn-beijing.aliyuncs.com/ai/image/2023-07-07/2.jpeg"/></td>
    <td ></td>
    <td ></td>
 </div> 
 <div align=center>
    <td><img height="" width="" src="https://lanhu-ai.oss-cn-beijing.aliyuncs.com/ai/image/2023-07-07/1.jpeg"/></td>
    <td></td>
    <td></td>
 </div>